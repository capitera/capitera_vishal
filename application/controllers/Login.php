<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $this->load->model('customer/m_login', 'objlogin');
    }

    public function index() {

        $login_type = $this->session->userdata('userType');
        if ($login_type == 'customer') {
            redirect('customer/dashboard');
        }
        $this->load->view('login');
    }

    public function authentication() {
        $username = $this->input->post('email');
        $password = $this->input->post('password');

        if (strlen(trim(preg_replace('/\xb2\xa0/', '', $username))) == 0 || strlen(trim(preg_replace('/\xb2\xa0/', '', $password))) == 0) {
            $this->session->set_flashdata('msg', '<lable class="text-red" style="padding: 0 0 10px 0; color:#f50909;">Please enter Username or Password</lable>');
            redirect('login');
        } else {
            // 'password' => md5($password)
            $arr = array(
                'email' => $username
            );
            $data = $this->objlogin->user_login($arr);
            if ($data) {
                if ($data['password'] == md5($password)) {
                    if ($data['two_auth'] == 1) {
                        require APPPATH . 'third_party/class/userClass.php';
                        require APPPATH . 'third_party/googleLib/GoogleAuthenticator.php';
                        $userClass = new userClass();
                        $ga = new GoogleAuthenticator();
                        $checkResult = $ga->verifyCode($data['google_auth_code'], $this->input->post('authenticator'), 2);
                        if ($checkResult) {
                            $session = array(
                                'cid' => $data['cust_id'],
                                'cname' => $data['fullname'],
                                'userType' => 'customer'
                            );
                            $this->session->set_userdata($session);
                            $this->load->library('user_agent');
                            $user_agent = $this->input->ip_address();
                            $login_his_arr = array(
                                'user_id' => $data['cust_id'],
                                'browser' => $this->agent->browser(),
                                'ip_address' => $this->input->ip_address()
                            );
                            $this->db->insert('customerloginhistory', $login_his_arr);
                            redirect('customer/dashboard');
                        } else {
                            $this->session->set_flashdata('msg_2fa', 'Wrong 2AF Code');
                            redirect('login');
                        }
                    } else {
                        $session = array(
                            'cid' => $data['cust_id'],
                            'cname' => $data['fullname'],
                            'userType' => 'customer'
                        );
                        $this->session->set_userdata($session);
                        $this->load->library('user_agent');
                        $user_agent = $this->input->ip_address();
                        $login_his_arr = array(
                            'user_id' => $data['cust_id'],
                            'browser' => $this->agent->browser(),
                            'ip_address' => $this->input->ip_address()
                        );
                        $this->db->insert('customerloginhistory', $login_his_arr);
                        redirect('customer/dashboard');
                    }
                } else {
                    $this->session->set_flashdata('msg_password', 'Wrong password');
                    redirect('login');
                }
            } else {
                $this->session->set_flashdata('msg_email', 'E-mail address not found');
                redirect('login');
            }
        }
    }

    function logout() {
        $this->session->unset_userdata('cid');
        $this->session->unset_userdata('cname');
        $this->session->unset_userdata('userType');
        $this->session->unset_userdata('theme_color');
        header('location:' . base_url() . 'login');
    }

}
