<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class LoginHistory extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('aname');

        if ($login_type != 'admin') {
            header('location:' . base_url() . 'admin/alogin');
            //  exit;
        }
        $this->load->model('madmin/m_loginhistory', 'mloginhistory');
    }

    public function index() {
        $data['loginhistory'] = $this->mloginhistory->getLoginHistory();
        $this->load->view('admin/header');
        $this->load->view('admin/loginhistory', $data);
        $this->load->view('admin/footer');
    }

}
