<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class KycDoc extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('aname');
        if ($login_type != 'admin') {
            header('location:' . base_url() . 'admin/alogin');
            //  exit;
        }
        $this->load->model('madmin/mkycdoc', 'mkyc');
    }

    public function index() {

        $data['user'] = $this->mkyc->getAllKycData();

        $this->load->view('admin/header');
        $this->load->view('admin/kycdata', $data);
        $this->load->view('admin/footer');
    }

    public function showKyc($id) {
        $data['user'] = $this->mkyc->getKycData($id);
        $data['removebutton'] = 'removebutton';
        $this->load->view('admin/header');
        $this->load->view('admin/kycdata', $data);
        $this->load->view('admin/footer');
    }

    public function verify_kyc() {

        foreach ($this->input->post('cat_id') as $id) {
            $set = array(
                'verified_status' => '1'
            );

            $this->db->update("customer_master", $set, array('cust_id' => $id));
        }
        echo TRUE;
    }

    public function reject_kyc() {
        foreach ($this->input->post('cat_id') as $id) {
            $set = array(
                'verified_status' => '2'
            );

            $this->db->update("customer_master", $set, array('cust_id' => $id));
        }
        echo TRUE;
    }

}
