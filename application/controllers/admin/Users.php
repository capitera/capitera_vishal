<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('aname');
        if ($login_type != 'admin') {
            header('location:' . base_url() . 'admin/alogin');
            exit;
        }
        $this->load->model('madmin/muser', 'muser');
    }

    public function index() {
        $data['user'] = $this->muser->getUserData();
        $data['template'] = $this->muser->get_template_data();
        $this->load->view('admin/header');
        $this->load->view('admin/users', $data);
        $this->load->view('admin/footer');
    }

    public function filter_search() {
        $data['user'] = $this->muser->filter_search();
        $this->load->view('admin/header');
        $this->load->view('admin/users', $data);
        $this->load->view('admin/footer');
    }

    public function deleteUser($userid) {
        $result = $this->muser->deleteUser($userid);
        if (!empty($result)) {
            header('location:' . base_url() . 'admin/users?msg=D');
        } else {
            header('location:' . base_url() . 'admin/users?msg=E');
        }
    }

    public function addNewUser() {
        $this->load->view('admin/header');
        $this->load->view('admin/adduser');
        $this->load->view('admin/footer');
    }

    public function add_customer() {
        $result = $this->muser->add_customer();
        if ($result == "0") {
            header('location:' . base_url() . 'admin/users?msg=A'); //email or phone already Exist
        } else if ($result == "2") {
            header('location:' . base_url() . 'admin/users?msg=E'); //Some Error
        } else {
            header('location:' . base_url() . 'admin/users?msg=S'); //Register Success
        }
    }

    public function editUser($userid) {
        $data['user'] = $this->muser->getUserDetail($userid);

        $this->load->view('admin/header');
        $this->load->view('admin/adduser', $data);
        $this->load->view('admin/footer');
    }

    public function updateCustomer() {
        $post = $this->input->post();
        $result = $this->muser->updateCustomer($post);
        if ($result == "0") {
            header('location:' . base_url() . 'admin/users?msg=A'); //email or phone already Exist
        } else if ($result == "2") {
            header('location:' . base_url() . 'admin/users?msg=E'); //Some Error
        } else {
            header('location:' . base_url() . 'admin/users?msg=U'); //Register Success
        }
    }

    public function send_email($id) {
        $ids = (explode(',', $id));
        $message = trim($this->input->post('txtemail'));
        $subject = trim($this->input->post('sel_temp_message'));
        $query = $this->muser->send_email($ids, $message, $subject);
        if ($query) {
            header("location:" . base_url() . "admin/users?msg=M");
        } else {
            header("location:" . base_url() . "admin/users?msg=E");
        }
    }

    public function getArchiveEmails() {
        $data['archive_emails'] = $this->muser->getArchiveEmails();
        $this->load->view('admin/header');
        $this->load->view('admin/archive_emails', $data);
        $this->load->view('admin/footer');
    }

}
