<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Changepassword extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('aname');
        if ($login_type != 'admin') {
            header('location:' . base_url() . 'admin/alogin');
             exit;
        }
    }

    public function index() {
        $this->load->view('admin/header');
        $this->load->view('admin/changepassword');
        $this->load->view('admin/footer');
    }

    public function updatePassword() {
        $post = $this->input->post();
        $oldpassword = $this->getOldPassword();
        if ($oldpassword != '') {
            if ($oldpassword == md5($post['opassword'])) {
                $this->db->update('admin', array('password' => md5($post['cpassword'])), array('admin_id' => 1));
                header('location:' . base_url() . 'admin/changepassword?msg=S');
            } else {
                header('location:' . base_url() . 'admin/changepassword?msg=NM');
            }
        } else {
            header('location:' . base_url() . 'admin/changepassword?msg=E');
        }
    }

    public function getOldPassword() {
        $result = $this->db->get_where('admin', array('admin_id' => '1'));
        return ($result->num_rows() > 0) ? $result->row()->password : '';
    }

}
