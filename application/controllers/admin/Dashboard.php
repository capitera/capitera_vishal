<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('aname');

        if ($login_type != 'admin') {
            header('location:' . base_url() . 'admin/alogin');
            exit;
        }
        $this->load->model('madmin/m_dashboard', 'mdboard');
    }

    public function index() {
        $data['user'] = $this->mdboard->getRegUser();
        $data['portfolios'] = $this->mdboard->getPortfolios();
        $data['bitcoin'] = $this->mdboard->getBitcoins();
        $data['ethereum'] = $this->mdboard->getEthereum();
        $data['litecoin'] = $this->mdboard->getLitecoin();
        $data['totalusd'] = $this->mdboard->getTotalUsd();
        $data['totalcoins'] = $this->mdboard->getTotalCoins();
        $data['pendingKYC'] = $this->mdboard->getPendingKYC();
        $data['branze_package'] = $this->mdboard->getPackages(1);
        $data['silver_package'] = $this->mdboard->getPackages(2);
        $data['gold_package'] = $this->mdboard->getPackages(3);
        $data['platinum_package'] = $this->mdboard->getPackages(4);
        $this->load->view('admin/header');
        $this->load->view('admin/dashboard', $data);
        $this->load->view('admin/footer');
    }

}
