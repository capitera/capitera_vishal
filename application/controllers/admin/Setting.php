<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('aname');
        if ($login_type != 'admin') {
            header('location:' . base_url() . 'admin/alogin');
        }
        $this->load->model('madmin/m_setting', 'setting');
    }

    public function index() {

        require APPPATH . 'third_party/class/userClass.php';
        require APPPATH . 'third_party/googleLib/GoogleAuthenticator.php';

       $data['profileupdate'] = $this->db->get_where('admin',array('admin_id'=>$this->session->userdata('aid')))->row()->two_auth;
       $data['twoAuth'] = $this->setting->checkTwoAuth();
        if ($data['twoAuth']->google_auth_code != '') {
            $userClass = new userClass();
            $ga = new GoogleAuthenticator();
            $data['qrCodeUrl'] = $ga->getQRCodeGoogleUrl($data['twoAuth']->username, $data['twoAuth']->google_auth_code, '2FA to CAPITERA');
            $data['google_auth_code'] = $data['twoAuth']->google_auth_code;
        } else {
            $userClass = new userClass();
            $ga = new GoogleAuthenticator();
            $secret = $ga->createSecret();
            $this->setting->update_googleCode($secret);
            $data['qrCodeUrl'] = $ga->getQRCodeGoogleUrl($data['twoAuth']->username, $secret, '2FA to CAPITERA');
            $data['google_auth_code'] = $secret;
        }


        $this->load->view('admin/header');
        $this->load->view('admin/setting', $data);
        $this->load->view('admin/footer');
    }

    public function updateSetting() {
        $post = $this->input->post();
        
        require APPPATH . 'third_party/class/userClass.php';
        require APPPATH . 'third_party/googleLib/GoogleAuthenticator.php';

        $userClass = new userClass();

        $ga = new GoogleAuthenticator();
       
        $checkResult = $ga->verifyCode(trim($post['adrs']), trim($post['appcode']), 2);    // 2 = 2*30sec clock tolerance
       
        if ($checkResult) {
           // $two_auth = 1;
           $result = $this->setting->updateSetting($post['twoAuthSts']);
            header('location:' . base_url() . 'admin/setting?msg=U');
        } else {
            header('location:' . base_url() . 'admin/setting?msg=E');
        }
    }
}
