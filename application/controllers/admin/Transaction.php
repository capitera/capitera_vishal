<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Transaction extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('aname');
        if ($login_type != 'admin') {
            header('location:' . base_url() . 'admin/alogin');
        }
        $this->load->model('madmin/mtransaction', 'mobject');
    }

    public function index() {
        $data['transaction'] = $this->mobject->getTransactionData();
        $this->load->view('admin/header');
        $this->load->view('admin/transaction', $data);
        $this->load->view('admin/footer');
    }

    public function filter_search() {
        $data['transaction'] = $this->mobject->filter_search();
        $this->load->view('admin/header');
        $this->load->view('admin/transaction', $data);
        $this->load->view('admin/footer');
    }

    public function approveTransaction($txt_id) {
        if ($txt_id != '') {
            $result = $this->mobject->approveTransaction($txt_id);
            if (!empty($result)) {
                $result = $this->mobject->sendCoin($txt_id);
                if (!empty($result)) {
                    // header('location:' . base_url().'admin/transaction?msg=S'); 
                    echo 'success';
                } else {
                    // header('location:' . base_url().'admin/transaction?msg=E');   
                    echo 'error';
                }
            } else {
                // header('location:' . base_url().'admin/transaction?msg=E');  
                echo 'error';
            }
        } else {
            // header('location:' . base_url().'admin/transaction?msg=E');
            echo 'error';
        }
    }

    public function rejectTransaction($txt_id) {
        if ($txt_id != '') {
            $result = $this->mobject->rejectTransaction($txt_id);
            if (!empty($result)) {
                //   header('location:' . base_url().'admin/transaction?msg=S');  
                echo 'success';
            } else {
                //  header('location:' . base_url().'admin/transaction?msg=E');   
                echo 'error';
            }
        } else {
            // header('location:' . base_url().'admin/transaction?msg=E');
            echo 'error';
        }
    }

}
