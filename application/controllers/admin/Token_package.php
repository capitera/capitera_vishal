<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Token_package extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('aname');
        if ($login_type != 'admin') {
            redirect('admin/alogin');
        }
        
        $this->load->model('madmin/m_tokenpackage', 'mpackage');
    }

    public function index() {
        $data['packages'] = $this->mpackage->getPackages();
        $this->load->view('admin/header');
        $this->load->view('admin/token_package', $data);
        $this->load->view('admin/footer');
    }

    public function delete($p_id){
        $this->mpackage->package_delete($p_id);
        header('location:' . base_url().'admin/token_package?msg=S');
    }

    public function getPackage(){
        $p = $this->input->post('package_id');
        $q = $this->db->get_where('token_package', array('package_id' => $p));
        if($q->num_rows() > 0){
            echo json_encode($q->row());
        }else{
            echo json_encode('0');
        }
    }

    public function editPackage(){
        $p = $this->input->post();
        $result = $this->mpackage->editPackage($p);
        if($result == 1){
            echo json_encode($p);
        }else{
            echo json_encode('0');
        }
    }
  
}
