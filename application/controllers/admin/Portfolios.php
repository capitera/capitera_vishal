<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Portfolios extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('aname');
        if ($login_type != 'admin') {
            header('location:' . base_url() . 'admin/alogin');
            exit;
        }
        $this->load->model('madmin/m_portfolio', 'mportfolio');
    }

    public function index() {
        $data['portfolio'] = $this->mportfolio->getPortfolioData();
        $this->load->view('admin/header');
        $this->load->view('admin/portfolio', $data);
        $this->load->view('admin/footer');
    }

    public function filter_search() {
        $data['portfolio'] = $this->mportfolio->filter_search();
        $this->load->view('admin/header');
        $this->load->view('admin/portfolio', $data);
        $this->load->view('admin/footer');
    }

    public function add_portfolio() {
        $data['assets'] = $this->mportfolio->getAssetsCurrencyData();
        
        $this->load->view('admin/header');
        $this->load->view('admin/add_portfolio',$data);
        $this->load->view('admin/footer');
    }

    public function addNewPortfolios() {
        $result = $this->mportfolio->addNewPortfolios();
        if ($result) {
            header('location:' . base_url() . 'admin/portfolios?msg=S'); //Register Success
        } else {
            header('location:' . base_url() . 'admin/portfolios?msg=E'); //Some Error 
        }
    }

    public function editPortfolio($pid) {
        $data['edit_portfolio'] = $this->mportfolio->editPortfolio($pid);
        $data['assets'] = $this->mportfolio->getAssetsCurrencyData();
        $this->load->view('admin/header');
        $this->load->view('admin/add_portfolio', $data);
        $this->load->view('admin/footer');
    }

    public function updatePortfolio() {
        $post = $this->input->post();
        $result = $this->mportfolio->updatePortfolio($post);
        if ($result) {
            header('location:' . base_url() . 'admin/portfolios?msg=U'); //Success
        } else {
            header('location:' . base_url() . 'admin/portfolios?msg=E'); //Error
        }
    }

    public function approvePortfolios($p_id) {
        if ($p_id != '') {
            $result = $this->mportfolio->approvePortfolios($p_id);
            if (!empty($result)) {
                echo 'success';
            } else {
                echo 'error';
            }
        } else {
            echo 'error';
        }
    }

    public function rejectPortfolios($p_id) {
        if ($p_id != '') {
            $result = $this->mportfolio->rejectPortfolios($p_id);
            if (!empty($result)) {
                echo 'success';
            } else {
                echo 'error';
            }
        } else {
            echo 'error';
        }
    }

    public function deletePortfolios($p_id) {
        if ($p_id != '') {
            $result = $this->mportfolio->deletePortfolios($p_id);
            if (!empty($result)) {
                echo 'success';
            } else {
                echo 'error';
            }
        } else {
            echo 'error';
        }
    }

}
