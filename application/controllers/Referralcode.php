<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Referralcode extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $this->load->helper('cookie');
    }

    function _remap($refferalcode) {
        $this->index($refferalcode);
    }

    public function index($refferalcode) {
        $result = $this->db->select('*')->get_where('customer_master', array('referralcode' => trim($refferalcode)));
        if ($result->num_rows() > 0) {
            if ($result->row()->referralcode != '') {
                $cookie = array(
                    'name' => 'referralcode',
                    'value' => trim($result->row()->referralcode),
                    'expire' => '86500'
                );
                $this->input->set_cookie($cookie);
                header('location:' . base_url());
            } else {
                header('location:' . base_url());
            }
        } else {
            header('location:' . base_url());
        }
    }

}
