<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Theme_setting extends CI_Controller {

    public function index() {
        $post = $this->input->post();
        $session = array(
            'theme_color' => $post['color']
        );
        $this->session->set_userdata($session);
        echo json_encode(array("result" => 1));
    }

}
