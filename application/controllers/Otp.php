<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Otp extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $this->load->model('customer/m_otp', 'motp');
    }

    public function index() {
        $auth = $this->session->userdata('google_auth');
        if ($auth != 1) {
            redierct('login');
        }
        $this->load->view('otp_page');
    }

    public function check_otp() {

        $p = $this->input->post();
        $data = $this->motp->get_otp($p);

        require APPPATH . 'third_party/class/userClass.php';
        require APPPATH . 'third_party/googleLib/GoogleAuthenticator.php';

        $userClass = new userClass();

        $ga = new GoogleAuthenticator();
        $checkResult = $ga->verifyCode($data->google_auth_code, $p['otp'], 2);    // 2 = 2*30sec clock tolerance

        if ($checkResult) {
            $session = array(
                'cid' => $data->cust_id,
                'cname' => $data->fullname,
                'userType' => 'customer'
            );
            $this->session->set_userdata($session);
            $this->load->library('user_agent');
            $user_agent = $this->input->ip_address();
            $login_his_arr = array(
                'user_id' => $data->cust_id,
                'browser' => $this->agent->browser(),
                'ip_address' => $this->input->ip_address()
            );
            $this->db->insert('customerloginhistory', $login_his_arr);
            redirect('customer/dashboard');
        } else {
            $this->session->set_flashdata('msg', '<div class="col-md-12 text-red" style="padding: 0 0 10px 0; color: red;">Google Authenticator Code is Wrong, Please try again.!!</div><br>');
            redirect('otp/index?id=' . base64_encode($data->cust_id));
        }
    }

}
