<?php

class Issuetoken extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('userType');
        if ($login_type != 'customer') {
            redirect('login');
        }
        $this->load->model('customer/m_issuetoken', 'missuetoken');
    }

    public function index() {
        $data['issuetoken'] = $this->missuetoken->getHistoryIssueToken();
        $data['wallate_blc']= $this->common->getCustTotalFanscoin($this->session->userdata('cid'));
        $this->load->view('customer/header');
        $this->load->view('customer/issuetoken', $data);
        $this->load->view('customer/footer');
    }

    public function get_userName() {
        $data['userName'] = $this->missuetoken->getUserName();
        $data['custid'] = $this->missuetoken->getCustId();
        echo json_encode($data);
    }

    public function send_balance() {
        $post = $this->input->post();
        if ($post['custid'] != '' && ($post['txtamount'] > 0)) {
            $result = $this->missuetoken->creditedWallet($post, $post['custid']);
            if ($result) {
                $this->session->set_flashdata('msg', '<div class="alert alert-success margin-top-15">Tokens Sent Successfully..!</div>');
                redirect('customer/issuetoken');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger margin-top-15">Something went wrong, plz try again..!</div>');
                redirect('customer/issuetoken');
            }
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger margin-top-15">Enter Valid Email Id..!</div>');
            redirect('customer/issuetoken');
        }
    }

}
