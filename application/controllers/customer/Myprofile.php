<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Myprofile extends CI_Controller {

    public function __construct() {
        parent::__construct();
       $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('userType');
        if($login_type != 'customer'){
            redirect('login');
        }
        $this->load->model('customer/m_myprofile','myprofile');
    }

   
    public function index(){
        $data['myprofile'] = $this->myprofile->getUserDetail();
        $this->load->view('customer/header');
        $this->load->view('customer/myprofile',$data);
        $this->load->view('customer/footer'); 
    }
    
    public function updateMyprofile(){
        $post = $this->input->post();
        $result = $this->myprofile->updateCustomer($post);
        if($result == "0")
        {
            header('location:' . base_url() . 'customer/myprofile?msg=A'); //email or phone already Exist
        } else if ($result == "2") {
            header('location:' . base_url() . 'customer/myprofile?msg=E'); //Some Error
        } else {
            header('location:' . base_url() . 'customer/myprofile?msg=U'); //Register Success
        }
    }
}
