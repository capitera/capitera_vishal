<?php

class Myitera extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('userType');
        if ($login_type != 'customer') {
            redirect('login');
        }
        $this->load->model('customer/m_myitera', 'mmyitera');
    }

    public function index() {
        $data['frozenCoins'] = $this->mmyitera->getFrozenCoins();
        $this->load->view('customer/header');
        $this->load->view('customer/myitera', $data);
        $this->load->view('customer/footer');
    }

}
