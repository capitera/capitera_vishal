<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('userType');
        if ($login_type != 'customer') {
            redirect('login');
        }
        $this->load->model('customer/m_dashboard', 'mdboard');
    }

    public function index() {
        $this->load->model('customer/m_portfolios', 'mportfolios');
        $data['portfolio'] = $this->mdboard->getPortfolioData();
        $this->load->view('customer/header');
        $this->load->view('customer/dashboard', $data);
        $this->load->view('customer/footer');
    }

    function setPortfoliyoSection() {
        $this->load->model('customer/m_portfolios', 'mportfolios');
        $data['portfolio'] = $this->mportfolios->getPortfolioData();
        $this->load->view('customer/portfolios', $data);
    }

    function setBuyHoldSection() {
        $this->load->model('customer/m_buyandhold', 'mbuyandhold');
        $data['buy_hold'] = $this->mbuyandhold->getBuyHoldData();
        $this->load->view('customer/buyandhold', $data);
    }

    function setMyIteraSection() {
        $this->load->model('customer/m_myitera', 'mmyitera');
        $data['frozenCoins'] = $this->mmyitera->getFrozenCoins();
        $this->load->view('customer/myitera', $data);
    }

    function set_my_wallet_section() {
        $this->load->model('customer/m_wallet', 'mwallet');
        $data['wallet'] = $this->mwallet->getWalletData();
        $this->load->view('customer/wallet', $data);
    }

}
