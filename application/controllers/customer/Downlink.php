<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Downlink extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $login_type = $this->session->userdata('userType');
        if($login_type != 'customer'){
            redirect('login');
        }
        $this->load->model('customer/m_downlink', 'mlink');
    }
    
    public function index()
    {
        $this->load->view('customer/header');
        $this->load->view('customer/downlink');
        $this->load->view('customer/footer');
    }

   
}