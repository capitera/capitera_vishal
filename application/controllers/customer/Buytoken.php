<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Buytoken extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('userType');
        if ($login_type != 'customer') {
            redirect('login');
        }
        $this->load->model('customer/m_buytoken', 'mbuytoken');
    }

    public function index() {

        $data['coin_rate'] = $this->mbuytoken->getCoinRate();

        $data['bonus'] = $this->mbuytoken->getBonus();

        $data['icoround'] = $this->mbuytoken->getIcoRound();

        $data['DisplaySetting'] = $this->mbuytoken->getBonusDisplaySetting();

        $data['DisplayICOSetting'] = $this->mbuytoken->getICODisplaySetting();

        $data['transationtotal'] = $this->mbuytoken->getTransactionTotalToken();
        $data['transactionpending'] = $this->mbuytoken->getTransactionPending();
        $data['referralcode'] = $this->mbuytoken->getReferralCode();
        $data['publickey'] = $this->mbuytoken->getPublicKey();
        $data['custdataverifystatus'] = $this->mbuytoken->getCustmerData();
        $data['addtioncharge'] = $this->mbuytoken->getAddtionalCharge();
        $data['token_packages'] = $this->mbuytoken->getTokenPackage();

        $this->load->view('customer/header');
        $this->load->view('customer/buytoken', $data);
        $this->load->view('customer/footer');
    }

    public function transaction() {
        $post = $this->input->post();

        $id = $this->mbuytoken->addTransaction($post);
        if ($id) {
            $result_data['txn_id'] = $id;
            if ($post['method'] != 'stripe') {
                $address = $this->mbuytoken->getCoinAddressData($id);
                $result_data['coin_address'] = $address;
                if ($post['method'] == 'bitcoin') {
                    // $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=USD'));
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=USD'));
                    $result_data['price_data'] = new stdClass();
                    $result_data['price_data']->name = 'Bitcoin';
                    $result_data['price_data']->symbol = 'BTC';
                    $result_data['price_data']->price_usd = $result_data['bitcoin_rate']->bitcoin->usd;
                } elseif ($post['method'] == 'litecoin') {
                    // $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/litecoin/?convert=USD'));
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coingecko.com/api/v3/simple/price?ids=litecoin&vs_currencies=USD'));
                    $result_data['price_data'] = new stdClass();
                    $result_data['price_data']->name = 'Litecoin';
                    $result_data['price_data']->symbol = 'LTC';
                    $result_data['price_data']->price_usd = $result_data['bitcoin_rate']->litecoin->usd;
                } elseif ($post['method'] == 'ethereum') {
                    // $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=USD'));
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=usd'));
                    $result_data['price_data'] = new stdClass();
                    $result_data['price_data']->name = 'Ethereum';
                    $result_data['price_data']->symbol = 'ETH';
                    $result_data['price_data']->price_usd = $result_data['bitcoin_rate']->ethereum->usd;
                } elseif ($post['method'] == 'XMR') {
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/monero/?convert=USD'));
                } elseif ($post['method'] == 'Dash') {
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/dash/?convert=USD'));

                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coingecko.com/api/v3/simple/price?ids=dash&vs_currencies=USD'));
                    $result_data['price_data'] = new stdClass();
                    $result_data['price_data']->name = 'Dash';
                    $result_data['price_data']->symbol = 'Dash';
                    $result_data['price_data']->price_usd = $result_data['bitcoin_rate']->dash->usd;
                } elseif ($post['method'] == 'Bitcoincash') {
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin-cash/?convert=USD'));
                } elseif ($post['method'] == 'B2BX') {
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/B2BX/?convert=USD'));
                } elseif ($post['method'] == 'EthereumClassic') {
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/ethereum-classic/?convert=USD'));
                } elseif ($post['method'] == 'NEM') {
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/NEM/?convert=USD'));
                } elseif ($post['method'] == 'NEO') {
                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/NEO/?convert=USD'));
                } elseif ($post['method'] == 'XRP') {
                    // $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coinmarketcap.com/v1/ticker/ripple/?convert=USD'));

                    $result_data['bitcoin_rate'] = json_decode(file_get_contents('https://api.coingecko.com/api/v3/simple/price?ids=ripple&vs_currencies=USD'));
                    $result_data['price_data'] = new stdClass();
                    $result_data['price_data']->name = 'Ripple';
                    $result_data['price_data']->symbol = 'XRP';
                    $result_data['price_data']->price_usd = $result_data['bitcoin_rate']->ripple->usd;
                }
                echo json_encode($result_data);
            } else {
                echo json_encode('');
            }
        } else {
            echo json_decode('');
        }
    }

    public function transaction_paid() {
        $post = $this->input->post();
        if (!empty($post)) {
            $icoround = $this->mbuytoken->getIcoRound();
            if (!empty($icoround)) {
                $transctionData = $this->common->getTransction($post['c_txnid']);
                if ($transctionData->total <= $icoround->total_token) {
                    $transaction = $this->mbuytoken->transaction_paid($post);
                    if (!empty($transaction)) {
                        header('location:' . base_url() . 'customer/buytoken?msg=S');
                    } else {
                        header('location:' . base_url() . 'customer/buytoken?msg=E');
                    }
                } else {
                    header('location:' . base_url() . 'customer/buytoken?msg=E');
                }
            } else {
                header('location:' . base_url() . 'customer/buytoken?msg=E');
            }
        } else {
            header('location:' . base_url() . 'customer/buytoken?msg=E');
        }
    }

    public function transactionStrip() {
        $post = $this->input->post();
        if (!empty($post)) {
            $icoround = $this->mbuytoken->getIcoRound();
            if (!empty($icoround)) {
                if ($post['total_bonus'] <= $icoround->total_token) {
                    $t_id = $this->mbuytoken->transactionStrip($post);
                    if (!empty($t_id)) {
                        $result_data['result'] = 'success';
                        echo json_encode($result_data);
                    } else {
                        $result_data['result'] = 'error';
                        echo json_encode($result_data);
                    }
                } else {
                    $result_data['result'] = 'error';
                    echo json_encode($result_data);
                }
            } else {
                $result_data['result'] = 'error';
                echo json_encode($result_data);
            }
        } else {
            $result_data['result'] = 'error';
            echo json_encode($result_data);
        }
    }

    public function transaction_offline() {
        $post = $this->input->post();
        if (!empty($post)) {
            $this->mbuytoken->add_transaction_offline($post);
            header('location:' . base_url() . 'customer/buytoken?msg=S');
        } else {
            header('location:' . base_url() . 'customer/buytoken?msg=E');
        }
    }

    public function transactionPaypal() {
        $post = $this->input->post();
        $id = $this->mbuytoken->addPaypalTransaction($post);
        if (!empty($post) && $id != '') {
            $data['post'] = $post;
            $data['id'] = $id;
            $this->session->set_userdata('txn_id', $id);

            $this->load->view('customer/paypal_payment', $data);
        } else {
            header('location:' . base_url() . 'customer/buytoken');
        }
    }

    public function success() {
        $id = $this->session->userdata('txn_id');
        if ($id != '') {
            $id = $this->mbuytoken->paypalSuccessPayment($id);
            $this->db->set('status', 0)->where('txn_id', $id)->update('transaction_mast');
        }
        $this->session->unset_userdata('txn_id');
        header('location:' . base_url() . 'customer/buytoken?msg=S');
    }

    public function cancel() {
        $id = $this->session->userdata('txn_id');
        $this->session->unset_userdata('txn_id');
        header('location:' . base_url() . 'customer/buytoken?msg=E');
    }

    public function notify_paypal() {
        if ($_POST['payment_status'] == "Completed") {
            $id = $_POST['custom'];
            $this->db->set('status', 0)->where('txn_id', $id)->update('transaction_mast');
        }
    }

    public function getPackageCoins() {
        $p = $this->input->post('package_id');

        $q = $this->db->get_where('token_package', array('package_id' => $p));
        if ($q->num_rows() > 0) {
            echo json_encode($q->row());
        } else {
            echo json_encode("0");
        }
    }

    public function getToken() {
        $data = array(
            "email" => 'vishaltesting16@gmail.com',
            "password" => "vishal789"
        );
        $headers = array(
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.bestrate.org/api/auth/login');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_exec($ch);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);

        echo "<pre>";
        print_r($result);
        die;
    }

    public function getPatner() {

        $data = array(
            "serviceId" => 'changelly',
            "depositTicker" => "BTC",
            "withdrawalTicker" => "eth",
            "partnerId" => "",
            "depositValue" => "1",
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bestrate.org/api/select-service",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $data = json_decode($response);
        }

        echo "<pre>";
        print_r($data);
        die;
    }

}
