<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kyc_document extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header('cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header("cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        $login_type = $this->session->userdata('userType');
        if ($login_type != 'customer') {
            redirect('login');
        }
        $this->load->model('customer/m_kycdoc', 'mkyc');
    }

    public function index() {
        $data['myprofile'] = $this->mkyc->getUserDetail();

        $this->load->view('customer/header');
        $this->load->view('customer/kyc_doc', $data);
        $this->load->view('customer/footer');
    }

    public function save_kycdoc() {
        
        $config['upload_path'] = './assets/KycDoc/';
        $config['allowed_types'] = 'jpg|png|pdf|doc|docx';
        $config['file_name'] = random_string('alnum', 16);
        $this->load->library('upload', $config);

        $error_messages = "";
        $error_messages1 = "";
        $error_messages2 = "";
        $dt = array();
        
      
        
        if (!$this->upload->do_upload('phone_bill')) {
            $error = array('error' => $this->upload->display_errors());
            $error_messages = "<br>Phone bill: " . $this->upload->display_errors();
        } else {

            $dt = array('upload_data' => $this->upload->data());
            $data = array(
                "fullname" => $this->input->post('full_name'),
                "phone" => $this->input->post('phone'),
                "email" => $this->input->post('email'),
                "address" => $this->input->post('address'),
                'country' => $this->input->post('country'),
                "phone_bill" => $dt['upload_data']['file_name'],
                "verified_status" => 0
            );

            $dt = $this->mkyc->save_docs($data);
        }


        if (!$this->upload->do_upload('proof_of_address_file')) {
            $error = array('error' => $this->upload->display_errors());
            $error_messages = "<br>Proof of Address: " . $this->upload->display_errors();
        } else {

            $dt = array('upload_data' => $this->upload->data());
            $data = array(
                "fullname" => $this->input->post('full_name'),
                "phone" => $this->input->post('phone'),
                "email" => $this->input->post('email'),
                "address" => $this->input->post('address'),
                'country' => $this->input->post('country'),
                "proof_of_address" => $dt['upload_data']['file_name'],
                "verified_status" => 0
            );

            $dt = $this->mkyc->save_docs4($data);
        }


        if (!$this->upload->do_upload('passport')) {
            $error = array('error' => $this->upload->display_errors());
            $error_messages1 = "<br>Passport: " . $this->upload->display_errors();
        } else {
            $dt = array('upload_data' => $this->upload->data());
            $data = array(
                "fullname" => $this->input->post('full_name'),
                "phone" => $this->input->post('phone'),
                "passport" => $dt['upload_data']['file_name'],
                "address" => $this->input->post('address'),
                'country' => $this->input->post('country'),
                "verified_status" => 0
            );
            $dt = $this->mkyc->save_docs1($data);
        }

        if (!$this->upload->do_upload('national_id')) {
            $error = array('error' => $this->upload->display_errors());
            $error_messages2 = "<br>National Id: " . $this->upload->display_errors();
        } else {
            $dt = array('upload_data' => $this->upload->data());
            $data = array(
                "fullname" => $this->input->post('full_name'),
                "phone" => $this->input->post('phone'),
                "national_id" => $dt['upload_data']['file_name'],
                "address" => $this->input->post('address'),
                'country' => $this->input->post('country'),
                "verified_status" => 0
            );
            $dt = $this->mkyc->save_docs2($data);
        }
        if ($error_messages != "") {
            $this->session->set_flashdata('msg', '<div class="col-md-12 text-red text-center" style="padding: 0 0 10px 0;">' . $this->upload->display_errors() . '</div><br>');
        }
        $this->session->set_flashdata('msg', '<div class="col-md-12 text-center" style="padding: 0 0 10px 0;color:#5cb85c;">Your KYC Document is updated...!</div><br>');
        redirect('customer/kyc_document');
    }

}
