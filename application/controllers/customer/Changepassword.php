<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Changepassword extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $login_type = $this->session->userdata('userType');
        if ($login_type != 'customer') {
            redirect('login');
        }
    }

    public function index() {
        $this->load->view('customer/header');
        $this->load->view('customer/changepassword');
        $this->load->view('customer/footer');
    }

    public function updatePassword() {
        $post = $this->input->post();

        $oldpassword = $this->getOldPassword();

        if ($oldpassword != '') {
            if ($oldpassword == md5($post['opassword'])) {
                /* $this->db->update('customer_master',array('password'=> base64_encode($post['cpassword'])),array('cust_id'=>$this->session->userdata('cid'))); */

                $sql = 'UPDATE customer_master SET password = ? WHERE cust_id = ?';
                $this->db->query($sql, array(md5($post['cpassword']), $this->session->userdata("cid")));

                header('location:' . base_url() . 'customer/changepassword?msg=S');
            } else {
                header('location:' . base_url() . 'customer/changepassword?msg=NM');
            }
        } else {
            header('location:' . base_url() . 'customer/changepassword?msg=E');
        }
    }

    public function getOldPassword() {
        /* $result = $this->db->get_where('customer_master',array('cust_id'=>$this->session->userdata('cid'))); */

        $sql = "SELECT * FROM customer_master WHERE cust_id = ?";
        $result = $this->db->query($sql, $this->session->userdata("cid"));

        return ($result->num_rows() > 0) ? $result->row()->password : '';
    }

}
