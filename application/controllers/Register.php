<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $this->load->helper('cookie');
        $this->load->helper('captcha');
        $this->load->helper('string');
        $this->load->model('customer/m_register', 'objregister');
        $this->rand = random_string('alnum', 4);
    }

    public function index() {
//        $captcha = $this->__captcha();
//        $data['captchaImg'] = $captcha;
        $this->load->view('register');
    }

    public function add_customer() {
        $result = $this->objregister->add_customer();
        if ($result == "0") {
            header('location:' . base_url() . 'register?msg=A');
        } else if ($result == "2") {
            header('location:' . base_url() . 'register?msg=E');
        } else {
            $this->session->set_flashdata('msg', '<div class="col-md-12" style="padding: 0 0 10px 0; color:green;">You have successfully Registered</div><br>');
            header('location:' . base_url() . 'login');
        }
    }

    function __captcha() {
        $vals = array(
            'word' => $this->rand,
            'img_path' => 'captcha_images/',
            'img_url' => base_url() . 'captcha_images/',
            'font_path' => FCPATH . 'assets/fonts/ROBOTO-BLACK.TTF'
        );
        $captcha = create_captcha($vals);
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode', $captcha['word']);
        return $captcha['image'];
    }

}
