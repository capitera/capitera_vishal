<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Forgotpassword extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Strict-Transport-Security:max-age=3600");
        $this->output->set_header("X-XSS-Protection: 1; mode=block");
        $this->output->set_header("X-Frame-Options: deny");
        $this->output->set_header("X-Content-Type-Options: nosniff");
        $this->output->set_header("Referrer-Policy: strict-origin");
        $this->load->model('customer/m_forgotpassword', 'objforgotpassword');
    }

    public function index() {
        $this->load->view('forgotpassword');
    }

    public function checkEmail() {
        $email = $this->input->post('email');
        $this->db->where('email', trim($email));
        $customer = $this->db->get('customer_master');
        if ($customer->num_rows() > 0) { //Check Email or Phone exist with new Use
            $result['msg'] = 'exist';
            echo json_encode($result);
        } else {
            $result['msg'] = 'notexist';
            echo json_encode($result);
        }
    }

    public function sendEmail() {
        $email = $this->input->post('email');
        $this->db->where('email', trim($email));
        $customer = $this->db->get('customer_master');
        if ($customer->num_rows() > 0) { //Check Email or Phone exist with new Use
            $cust_data = $customer->row();
            $from = 'info@capitera.io';
            $subject = 'Forgot Password';
            $link = base_url() . 'forgotpassword/changePassword?id=' . base64_encode($cust_data->cust_id);
            $message = "<p>Hello,<br><br>Please copy-paste following link on the browser </p><br>" . $link . "<br><br> or <a href=" . $link . ">Click Here</a> to reset your password <br><br> Best Regards,<br>Capitera Token Team";
            $this->common->sendEmail($from, trim($cust_data->email), $subject, $message);
            $result['msg'] = 'sendemail';
            echo json_encode($result);
        } else {
            $result['msg'] = 'error';
            echo json_encode($result);
        }
    }

    public function changePassword() {
        $data['customer_id'] = $this->input->get('id');
        $this->load->view('changeforgotpassword', $data);
    }

    public function passwordChange() {
        $post = $this->input->post();
        if (!empty($post)) {
            $this->objforgotpassword->setnewpassword($post);
            header('location:' . base_url() . 'login');
        } else {
            header('location:' . base_url() . 'login');
        }
    }

}
