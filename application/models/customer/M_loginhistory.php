<?php
class M_loginhistory extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function getLoginHistory()
    {
    	$this->db->select('*');
    	$this->db->from('customerloginhistory');
        $this->db->where('user_id',$this->session->userdata("cid"));
        $this->db->order_by("customerloginhistory_id", "desc");
        $result = $this->db->get();
        return ($result->num_rows() > 0) ? $result->result() : '';
    }
}