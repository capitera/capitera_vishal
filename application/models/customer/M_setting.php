<?php
class M_setting extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    public function checkTwoAuth(){
        $id = $this->session->userdata('cid');
        $result = $this->db->select('email, two_auth, google_auth_code')->where('cust_id', $id)->get('customer_master');
        return $result->row();
    }
    
    public function updateSetting($two_auth){
        $id = $this->session->userdata('cid');
        $this->db->set('two_auth', $two_auth)->where('cust_id', $id)->update('customer_master');
        return $this->db->affected_rows();
    }
    
    public function update_googleCode($secret){
        $id = $this->session->userdata('cid');
        $this->db->set('google_auth_code', $secret)->where('cust_id', $id)->update('customer_master');
        return $this->db->affected_rows();
    }
}