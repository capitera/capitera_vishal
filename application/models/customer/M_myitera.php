<?php

class M_myitera extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getFrozenCoins() {
        $whr = array('user_id' => $this->session->userdata("cid"), 'status' => '1');
        $this->db->select('SUM(remaining_coin) as total_coins');
        $this->db->from('transaction_mast');
        $this->db->where($whr);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row()->total_coins;
        } else {
            return '0';
        }
    }

}
