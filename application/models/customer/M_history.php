<?php

class M_history extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getSendTokenData() {
        $this->db->select('*');
        $this->db->from('history_issuetoken');
        $this->db->where('issue_id', $this->session->userdata('cid'));
        $this->db->order_by("history_issuetoken_id", "desc");
        $result = $this->db->get();
        return ($result->num_rows() > 0) ? $result->result() : '';
    }

    function getReferralData() {
        $referral_code = $this->db->get_where("customer_master",array("cust_id"=>$this->session->userdata('cid')))->row()->referralcode;
        $this->db->select('*');
        $this->db->from('customer_master');
        $this->db->where('referralcodefrom',$referral_code);
        $result = $this->db->get();
        return ($result->num_rows() > 0) ? $result->result() : '';
    }

}
