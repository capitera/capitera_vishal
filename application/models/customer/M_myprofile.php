<?php

class M_myprofile extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getUserDetail() {
        /* $this->db->select('*');
          $this->db->from('customer_master');
          $this->db->where('cust_id',$this->session->userdata("cid"));
          $user = $this->db->get(); */

        $sql = "SELECT * FROM customer_master WHERE cust_id = ?";
        $user = $this->db->query($sql, array($this->session->userdata("cid")));

        if ($user->num_rows() > 0) {
            return $user->row();
        } else {
            return '';
        }
    }

    function updateCustomer($post) {
        $this->load->helper('string');
        $this->updateProfile($post);


        $config['upload_path'] = './assets/KycDoc/';
        $config['allowed_types'] = 'jpg|png|pdf|doc|docx';
        $config['file_name'] = random_string('alnum', 16);
        $this->load->library('upload', $config);

        $error_messages = "";
        $error_messages1 = "";
        $error_messages2 = "";
        $dt = array();



        if (!$this->upload->do_upload('phone_bill')) {
            $error = array('error' => $this->upload->display_errors());
            $error_messages = "<br>Phone bill: " . $this->upload->display_errors();
        } else {

            $dt = array('upload_data' => $this->upload->data());
            $data = array(
                "phone_bill" => $dt['upload_data']['file_name'],
                "verified_status" => 0
            );

            $this->db->update("customer_master", $data, array('cust_id' => $this->session->userdata("cid")));
        }


        if (!$this->upload->do_upload('proof_of_address_file')) {
            $error = array('error' => $this->upload->display_errors());
            $error_messages = "<br>Proof of Address: " . $this->upload->display_errors();
        } else {

            $dt = array('upload_data' => $this->upload->data());
            $data = array(
                "proof_of_address" => $dt['upload_data']['file_name'],
                "verified_status" => 0
            );

            $this->db->update("customer_master", $data, array('cust_id' => $this->session->userdata("cid")));
        }


        if (!$this->upload->do_upload('passport')) {
            $error = array('error' => $this->upload->display_errors());
            $error_messages1 = "<br>Passport: " . $this->upload->display_errors();
        } else {
            $dt = array('upload_data' => $this->upload->data());
            $data = array(
                "passport" => $dt['upload_data']['file_name'],
                "verified_status" => 0
            );
            $this->db->update("customer_master", $data, array('cust_id' => $this->session->userdata("cid")));
        }

        if (!$this->upload->do_upload('national_id')) {
            $error = array('error' => $this->upload->display_errors());
            $error_messages2 = "<br>National Id: " . $this->upload->display_errors();
        } else {
            $dt = array('upload_data' => $this->upload->data());
            $data = array(
                "national_id" => $dt['upload_data']['file_name'],
                "verified_status" => 0
            );
            $this->db->update("customer_master", $data, array('cust_id' => $this->session->userdata("cid")));
        }


        return "1";
    }

    function updateProfile($post) {
        $config['upload_path'] = './assets/profiles/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['file_name'] = random_string('alnum', 16);
        $this->load->library('upload', $config,'new_upload');

        $set = array(
            'fullname' => trim($post['full_name']),
            'address' => $post['address'],
            'postal_code' => $post['postal_code'],
            'city' => trim($post['city']),
            'country' => trim($post['country']),
            'date_of_birth' => date("Y-m-d", strtotime($post['date_of_birth'])),
            'phone' => trim($post['phone']),
        );

        if ($_FILES["profile_pic"]["name"] != '') {
            $this->new_upload->do_upload('profile_pic');
            $dt = array('upload_data' => $this->new_upload->data());
            $set['profile_pic'] = $dt['upload_data']['file_name'];
        }


        $this->db->update("customer_master", $set, array('cust_id' => $this->session->userdata("cid")));
    }

}
