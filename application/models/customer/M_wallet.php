<?php

class M_wallet extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getWalletData() {
        $this->db->select('*');
        $this->db->from('customer_wallet w');
        $this->db->join('assets_currency a', 'w.currency_wallet_id=a.currency_wallet_id');
        $this->db->where('w.user_id', $this->session->userdata('cid'));
        $buy_hold = $this->db->get();
        if ($buy_hold->num_rows() > 0) {
            return $buy_hold->result();
        } else {
            return '';
        }
    }

}
