<?php
class M_login extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    public function user_login($login_data){
        /* $result = $this->db->select('cust_id, fullname, two_auth')->get_where('customer_master', $login_data); */
         $sql = "SELECT * FROM customer_master WHERE email = ?";
	     $result = $this->db->query($sql, array($login_data['email']));
        
        if($result->num_rows() > 0){
            return $result->row_array();
        }else{
            return '';
        }
    }
}