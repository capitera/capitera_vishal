<?php

class M_dashboard extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getPortfolioData() {
        $this->db->select('*');
        $this->db->from('portfolio p');
        $this->db->join('assets_currency a', 'p.assets_base=a.currency_wallet_id');
        $this->db->order_by("portfolio_id", "desc");
        $portfolio = $this->db->get();
        if ($portfolio->num_rows() > 0) {
            return $portfolio->result();
        } else {
            return '';
        }
    }

}
