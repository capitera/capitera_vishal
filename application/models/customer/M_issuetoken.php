<?php
class M_issuetoken extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getUserName() {
        $email = $this->input->post('email');
        $result = $this->db->where('email', $email)->get('customer_master');
        return ($result->num_rows() > 0) ? $result->row('fullname') : '';
    }

    function getCustId() {
        $email = $this->input->post('email');
        $result = $this->db->where('email', $email)->get('customer_master');
        return ($result->num_rows() > 0) ? $result->row('cust_id') : '';
    }

    function getWalletBalance() {
        $email = $this->input->post('email');
        $result = $this->db->where('email', $email)->get('customer_master');
        if ($result->num_rows() > 0) {
            $custdata = $result->row();
            $result_wallet = $this->db->where('cust_id', $custdata->cust_id)->get('wallet');
            return ($result_wallet->num_rows() > 0) ? $result_wallet->row('total_coins') : 0;
        } else {
            return 0;
        }
    }

    function getHistoryIssueToken() {
        $this->db->select('*');
        $this->db->from('history_issuetoken');
        $this->db->where('issue_id', $this->session->userdata('cid'));
        $this->db->order_by("history_issuetoken_id", "desc");
        $result = $this->db->get();
        return ($result->num_rows() > 0) ? $result->result() : '';
    }

    //  Credit Amount in Receiver Wallet    
    function creditedWallet($post, $receiver_id) {

        $q = $this->db->get_where('wallet', array('cust_id' => $this->session->userdata('cid')));
        if ($q->num_rows() > 0) {

            $mywallet = $q->row()->total_coins;

            if ($mywallet >= $post['txtamount']) {
                $cust_info = $this->common->getCustData($this->session->userdata('cid'));
                $history_arr = array(
                    'email' => $cust_info->email,
                    'token' => $post['txtamount'],
                    'wallet_type' => 'Debite by ' . $post['txtemail'],
                    'issue_by' => 'user',
                    'issue_id' => $this->session->userdata('cid')
                );
                $this->db->insert('history_issuetoken', $history_arr);

                $remain_wallet = $mywallet - $post['txtamount'];
                $this->db->update('wallet', array('total_coins' => $remain_wallet), array('cust_id' => $this->session->userdata('cid')));


                $wallate_bal = $this->db->get_where('wallet', array('cust_id' => $receiver_id))->row();

                $wallet_balance_total = $wallate_bal->total_coins + $post['txtamount'];

                $wallet_set = array(
                    'total_coins' => $wallet_balance_total
                );
                $this->db->where('cust_id', $receiver_id)->update('wallet', $wallet_set);

                $history_arr = array(
                    'email' => $post['txtemail'],
                    'token' => $post['txtamount'],
                    'wallet_type' => 'Credit By : ' . $cust_info->email,
                    'issue_by' => 'user',
                    'issue_id' => $receiver_id
                );
                $this->db->insert('history_issuetoken', $history_arr);


                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

}
