<?php

class M_kycdoc extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getUserDetail() {
        $sql = "SELECT * FROM customer_master WHERE cust_id = ?";
        $user = $this->db->query($sql, $this->session->userdata("cid"));
        if ($user->num_rows() > 0) {
            return $user->row();
        } else {
            return '';
        }
    }

    function save_docs($data) {
        $sql = 'UPDATE customer_master SET fullname = ?, phone = ?, email = ?, address = ?, country = ?, phone_bill = ?, verified_status = ? WHERE cust_id = ?';
        $this->db->query($sql, array(trim($data['fullname']), trim($data['phone']), trim($data['email']), trim($data['address']), trim($data['country']), trim($data['phone_bill']), trim($data['verified_status']), $this->session->userdata("cid")));
    }

    function save_docs1($data) {
        $sql = 'UPDATE customer_master SET fullname = ?, phone = ?, passport = ?, address = ?, country = ?, verified_status = ? WHERE cust_id = ?';
        $this->db->query($sql, array(trim($data['fullname']), trim($data['phone']), trim($data['passport']), trim($data['address']), trim($data['country']), trim($data['verified_status']), $this->session->userdata("cid")));
    }

    function save_docs2($data) {
        $sql = 'UPDATE customer_master SET fullname = ?, phone = ?, national_id = ?, address = ?, country = ?, verified_status = ? WHERE cust_id = ?';
        $this->db->query($sql, array(trim($data['fullname']), trim($data['phone']), trim($data['national_id']), trim($data['address']), trim($data['country']), trim($data['verified_status']), $this->session->userdata("cid")));
    }

    function save_docs4($data) {
        $sql = 'UPDATE customer_master SET fullname = ?, phone = ?, proof_of_address = ?, address = ?, country = ?, verified_status = ? WHERE cust_id = ?';
        $this->db->query($sql, array(trim($data['fullname']), trim($data['phone']), trim($data['proof_of_address']), trim($data['address']), trim($data['country']), trim($data['verified_status']), $this->session->userdata("cid")));
    }

    function is_kyc_confirmed_by_admin() {
        $customer_id = $this->session->userdata("cid");
        $this->db->where("cust_id", $customer_id);
        $this->db->where("verified_status", 1);
        $query = $this->db->get("customer_master");
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
