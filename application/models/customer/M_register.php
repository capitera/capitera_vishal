<?php

class M_register extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function add_customer() {
        $post = $this->input->post();
        $or_where = '(email = "' . trim($post['email']) . '")';
        $this->db->where($or_where);
        $customer = $this->db->get('customer_master');
        if ($customer->num_rows() > 0) { //Check Email or Phone exist with new User 
            return "0";
        } else {
            if ($post['email'] != '' && $post['password'] != '') {
                $referralcode = $post['referral_id'];
                $password = md5($post['password']);
                $sql = 'INSERT INTO customer_master (fullname, email, password, country, ethereum_address, status, referralcode, referralcodefrom) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
                $this->db->query($sql, array("", trim($post['email']), $password, "", '', 1, random_string('alnum', 8), $referralcode));
                $cust_id = $this->db->insert_id();
                if ($cust_id > 0) {
                    $this->createCustomerWallet($cust_id);
                    $from = 'info@capitera.io';
                    $subject = 'Capitera : Your Registration is successful';
                    $message = '<p>Congratulation, Your registration is successful. You can login and purchase Capitera Token now ! <br><br><br> Best Regards,<br>Capitera Token Team<p>';
                    $this->common->sendEmail($from, trim($post['email']), $subject, $message);
                    return "1";
                } else {
                    return "2";
                }
            } else {
                return "2";
            }
        }
    }

    function createCustomerWallet($cust_id) {
        $this->db->select('*');
        $this->db->from('assets_currency');
        $currency_wallet = $this->db->get();
        if ($currency_wallet->num_rows() > 0) {
            foreach ($currency_wallet->result() as $val) {
                $setarray = array(
                    'currency_wallet_id' => trim($val->currency_wallet_id),
                    'total_balance' => 0,
                    'address' => '',
                    'method' => trim($val->currency_name),
                    'user_id' => $cust_id,
                    'reg_date' => date('Y-m-d')
                );
                $this->db->insert("customer_wallet", $setarray);
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
