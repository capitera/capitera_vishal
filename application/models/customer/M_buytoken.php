<?php

class M_buytoken extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getCoinRate() {
        $this->db->select('*');
        $this->db->from('ico_round');
        $this->db->where('STR_TO_DATE(start_date, "%Y-%m-%d") <=', date('Y-m-d'));
        $this->db->where('STR_TO_DATE(end_date, "%Y-%m-%d") >=', date('Y-m-d'));
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row('round_rate');
        } else {
            return 0;
        }
    }

    function getIcoRound() {
        $this->db->select('*');
        $this->db->from('ico_round');
        $this->db->where('STR_TO_DATE(start_date, "%Y-%m-%d") <=', date('Y-m-d'));
        $this->db->where('STR_TO_DATE(end_date, "%Y-%m-%d") >=', date('Y-m-d'));
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

    function getBonus() {
        $this->db->select('*');
        $this->db->from('ico_round');
        $this->db->order_by("ico_round_id", "asc");
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }
    }

    function getICODisplaySetting() {
        // $result = $this->db->select('*')->get('ico_display_setting')->row();

        $sqldisplaysetting = 'SELECT * FROM ico_display_setting';
        $result = $this->db->query($sqldisplaysetting)->row();
        return $result;
    }

    function getBonusDisplaySetting() {
        // $result = $this->db->select('status')->get('bonus_display_setting')->row();
        $sqlBonus = 'SELECT `status` FROM bonus_display_setting';
        $result = $this->db->query($sqlBonus)->row();
        return $result;
    }

    function addTransaction($post) {

        if ($post['method'] != 'Credit-Card') {
            $result = $this->getCoinAddress($post['method']);

            $num = sizeof($result);
            $address_num = rand(0, $num - 1);
            $rate = '';
            if ($post['method'] == 'bitcoin') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=USD');
            } else if ($post['method'] == 'litecoin') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/litecoin/?convert=USD');
            } else if ($post['method'] == 'ethereum') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=USD');
            } else if ($post['method'] == 'XMR') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/monero/?convert=USD');
            } else if ($post['method'] == 'Dash') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/dash/?convert=USD');
            } else if ($post['method'] == 'Bitcoincash') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/bitcoin-cash/?convert=USD');
            } else if ($post['method'] == 'B2BX') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/B2BX/?convert=USD');
            } else if ($post['method'] == 'EthereumClassic') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/ethereum-classic/?convert=USD');
            } else if ($post['method'] == 'NEM') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/NEM/?convert=USD');
            } else if ($post['method'] == 'NEO') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/NEO/?convert=USD');
            } elseif ($post['method'] == 'XRP') {
                $rate = file_get_contents('https://api.coinmarketcap.com/v1/ticker/ripple/?convert=USD');
            }

            $crt_address = $result[$address_num]->address;
        } else {
            $crt_address = '';
        }


        /* $rate = json_decode($rate);
          $extracharge = $this->getAddtionalCharge();
          if ($extracharge > 0) {
          $chargerate = $rate[0]->price_usd + ($rate[0]->price_usd * $extracharge) / 100;
          $crypto = $post['amount'] / $chargerate;
          } else {
          $crypto = $post['amount'] / $rate[0]->price_usd;
          }
          $icoround = $this->getIcoRound(); */

        /* $arr = array(
          'address' => $address_num,
          'method' => $post['method'],
          'user_id' => $this->session->userdata('cid'),
          'user_name' => $this->session->userdata('cname'),
          'amount' => $post['amount'],
          'crypto' => number_format($crypto, 8),
          'coins' => $post['coin'],
          'bonus' => $post['bonus'],
          'total' => $post['total_bonus'],
          'status' => 0,
          'action' => 1,
          'transaction_date' => date('Y-m-d H:i')
          );
          $this->db->insert('transaction_mast', $arr); */
        $this->load->library('user_agent');
        $browser = $this->agent->browser();
        $ip_address = $this->input->ip_address();
        $token_package = $this->db->get_where('token_package', array('package_id' => $post['package_id']))->row();
        if (!empty($token_package)) {
            $post['amount'] = $token_package->price * $post['quantity'];
            $post['coin'] = $token_package->no_of_token * $post['quantity'];
            $post['bonus'] = $token_package->no_of_token * $post['quantity'];
        }
        $sql = 'INSERT INTO transaction_mast (address, method, package_id, user_id, user_name, quantity, amount, crypto, coins, bonus, total, status, action, remaining_coin,transaction_date,browser,ip_address,round_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?)';
        $this->db->query($sql, array($crt_address, $post['method'], $post['package_id'], $this->session->userdata('cid'), $this->session->userdata('cname'), $post['quantity'], $post['amount'], 0, $post['coin'], $post['bonus'], $post['coin'], 0, 1, $post['coin'], date('Y-m-d H:i'), $browser, $ip_address, ''));
        // $this->db->query($sql, array($result[$address_num]->address, $post['method'], $post['package_id'], $this->session->userdata('cid'), $this->session->userdata('cname'), $post['quantity'], $post['amount'], number_format($crypto, 8), $post['coin'], $post['bonus'], $post['coin'], 0, 1, $post['coin'],date('Y-m-d H:i'), ''));
        return $this->db->insert_id();
    }

    function getCoinAddress($method) {
        $result = $this->db->where('coin_name', $method)->get('coin_address');
        return ($result->num_rows() > 0) ? $result->result() : '';
    }

    function getCoinAddressData($id) {
        $sqlAddress = 'SELECT * FROM transaction_mast WHERE txn_id = ?';
        $query = $this->db->query($sqlAddress, array($id));
        return $query->row()->address;
    }

    function transaction_paid($post) {
        /* $arr = array(
          'address' => $post['c_address'],
          'crypto' => number_format($post['c_crypto'], 8)
          );
          $this->db->update('transaction_mast', $arr, array('txn_id' => $post['c_txnid'])); */

        $sqlTransactionUpdate = 'UPDATE transaction_mast SET address = ?, crypto = ? WHERE txn_id = ?';
        $this->db->query($sqlTransactionUpdate, array($post['c_address'], number_format($post['c_crypto'], 8), $post['c_txnid']));

        $transctionData = $this->common->getTransction($post['c_txnid']);
        $packageName = $this->common->getPackageName($transctionData->package_id);
        if (!empty($transctionData)) {
            $from = 'info@capitera.io';
            $to = trim($this->common->getCustEmail($this->session->userdata("cid")));
            $subject = 'Capitera Token : Your transaction is pending';
            $message = '<p>Hi ' . $this->session->userdata('cname') . ' <br><br> Thank you for your purchase of ' . $transctionData->total . ' Capitera Token via ' . $transctionData->method . ' as the payment method. <br><br> <b>Transaction Details :- </b> <br> Package : ' . $packageName->package_name . ' $' . $packageName->price . '<br>Quantity : ' . $transctionData->quantity . '<br>Paid via : ' . $transctionData->method . ' <br> You will get : ' . $transctionData->total . ' Capitera Token <br><br> We need at least 12-24 hours to confirm your transaction, Once it approved you can see ' . $transctionData->total . ' Capitera Token in your wallet. <br><br><br>Best Regards,<br>Capitera Token Team <p>';
            $this->common->sendEmail($from, $to, $subject, $message);
            return $post['c_txnid'];
        } else {
            return '';
        }
    }

    public function getPublicKey() {
        // $query = $this->db->get_where('stripe_key_setting', array('stripe_key_setting_id' => '1'));

        $sqlPublicKey = 'SELECT * FROM stripe_key_setting WHERE stripe_key_setting_id = ?';
        $query = $this->db->query($sqlPublicKey, array('1'));

        return $query->row();
    }

    public function transactionStrip($post) {
        $post = $this->input->post();
        if (!empty($post)) {
            $SecretKey = $this->getSecretKey();
            if ($SecretKey->secret_key != '') {
                require_once(FCPATH . 'assets/stripe/lib/Stripe.php');
                Stripe::setApiKey(trim($SecretKey->secret_key));

                if ($post['stripeToken'] != "") {
                    $charge = Stripe_Charge::create(array(
                                "source" => $post['stripeToken'],
                                "amount" => $post['amount'] * 100,
                                "currency" => 'USD',
                                "metadata" => array(
                                    'Customer_ID' => $this->session->userdata("cid")
                                ),
                                "description" => $this->session->userdata("fullname")
                    ));

                    if ($charge->id != '') {

                        $from = 'info@capitera.io';
                        $to = trim($this->common->getCustEmail($this->session->userdata("cid")));
                        $subject = 'Capitera Token : Your transaction is pending';
                        $message = '<p>Hi ' . $this->session->userdata('cname') . ' <br><br> Thank you for your purchase of ' . $post['total_bonus'] . ' Capitera Token via ' . $post['method'] . ' as the payment method. <br><br> <b>Transaction Details :- </b> <br> Amount in USD : ' . $post['amount'] . '<br> Paid via : ' . $post['method'] . ' <br> You will get : ' . $post['total_bonus'] . ' Capitera Token <br><br> We need at least 12-24 hours to confirm your transaction, Once it approved you can see ' . $post['total_bonus'] . ' Capitera Token in your wallet. <br><br><br> Best Regards,<br>Capitera Token Team <p>';
                        $this->common->sendEmail($from, $to, $subject, $message);

                        $wallet = $this->common->getCustWallet($this->session->userdata("cid"));
                        $this->load->library('user_agent');
                        $browser = $this->agent->browser();
                        $ip_address = $this->input->ip_address();
                        if (!empty($wallet)) {
                            $totalamount = $wallet->amount + $post['amount'];
                            // $wllarray = array('amount'=>$totalamount);
                            // $this->db->update('wallet',$wllarray,array('cust_id'=>$this->session->userdata("cid")));

                            $sqlWalletUpdate = 'UPDATE wallet SET amount = ? WHERE cust_id = ?';
                            $this->db->query($sqlWalletUpdate, array($totalamount, $this->session->userdata("cid")));
                        } else {
                            // $wllarray = array('cust_id'=>$this->session->userdata("cid"),'total_coins'=>0,'amount'=>$post['amount']);
                            // $this->db->insert('wallet', $wllarray);

                            $sqlWallet = 'INSERT INTO wallet (cust_id, total_coins, amount) VALUES (?, ?, ?)';
                            $this->db->query($sqlWallet, array($this->session->userdata("cid"), 0, $post['amount']));
                        }
                        $icoround = $this->getIcoRound();
                        /* $arr = array(
                          'method' => $post['method'],
                          'transaction_id'=>$charge->id,
                          'user_id' => $this->session->userdata('cid'),
                          'user_name' => $this->session->userdata('cname'),
                          'amount' => $post['amount'],
                          'crypto'=> $post['amount'],
                          'coins' => $post['coin'],
                          'bonus' => $post['bonus'],
                          'total' => $post['total_bonus'],
                          'status' => 1,
                          'action' => 1,
                          'transaction_date' => date('Y-m-d H:i')
                          );
                          $this->db->insert('transaction_mast', $arr); */

                        $sqlTransaction = 'INSERT INTO transaction_mast (method, transaction_id, user_id, user_name, amount, crypto, coins, bonus, total, status, action, transaction_date,browser,ip_address, round_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?)';
                        $this->db->query($sqlTransaction, array($post['method'], $charge->id, $this->session->userdata('cid'), $this->session->userdata('cname'), $post['amount'], $post['amount'], $post['coin'], $post['bonus'], $post['total_bonus'], 1, 1, date('Y-m-d H:i'), $browser, $ip_address, $icoround->ico_round_id));

                        $transctionid = $this->db->insert_id();
                        $icodata = $this->getIcoRound();
                        if (!empty($icodata)) {
                            $total_token = $icodata->total_token - $post['total_bonus'];
                            $total_sold = $icodata->total_sold + $post['total_bonus'];
                            $icoarray = array('total_token' => $total_token, 'total_sold' => $total_sold);
                            $this->db->update('ico_round', $icoarray, array('ico_round_id' => $icodata->ico_round_id));
                        }

                        $this->sendCoin($transctionid);
                        return $transctionid;
                    } else {
                        return '';
                    }
                } else {
                    return '';
                }
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function getSecretKey() {
        // $query = $this->db->get_where('stripe_key_setting', array('stripe_key_setting_id' => '1'));
        $sqlSecretKey = 'SELECT * FROM stripe_key_setting WHERE stripe_key_setting_id = ?';
        $query = $this->db->query($sqlSecretKey, array(1));
        return $query->row();
    }

    public function getReferralCode() {
        //   $query = $this->db->get_where('customer_master', array('cust_id' =>$this->session->userdata('cid')));

        $sqlReferralCode = 'SELECT * FROM customer_master WHERE cust_id = ?';
        $query = $this->db->query($sqlReferralCode, array($this->session->userdata('cid')));

        return $query->row()->referralcode;
    }

    function getWithdrawalCoins() {
        /* $whr = array("user_id"=>$this->session->userdata('cid'));
          $q = $this->db->get_where('withdraw',$whr); */

        $sqlWithdrawalCoins = 'SELECT * FROM withdraw WHERE user_id = ?';
        $q = $this->db->query($sqlWithdrawalCoins, array($this->session->userdata('cid')));

        return $q->result_array();
    }

    function getWithdrawalCoinsTotal() {
        /* $this->db->select('SUM(tokens) as total_coins');
          $this->db->from('withdraw');
          $this->db->where(array('withdraw_status'=>'1','user_id' =>$this->session->userdata('cid')));
          $totalcoin = $this->db->get(); */

        $sqlWithdrawalCoinsTotal = 'SELECT SUM(tokens) as total_coins FROM withdraw WHERE withdraw_status = ? and user_id = ?';
        $totalcoin = $this->db->query($sqlWithdrawalCoinsTotal, array(1, $this->session->userdata('cid')));

        if ($totalcoin->num_rows() > 0) {
            return $totalcoin->row()->total_coins;
        } else {
            return '0';
        }
    }

    function getTransactionPending() {
        /* $this->db->select('SUM(total) as total_coins');
          $this->db->from('transaction_mast');
          $this->db->where(array('status'=>'0','user_id' =>$this->session->userdata('cid')));
          $totalcoin = $this->db->get(); */

        $sqlTransactionPending = 'SELECT SUM(total) as total_coins FROM transaction_mast WHERE status = ? AND user_id = ?';
        $totalcoin = $this->db->query($sqlTransactionPending, array(0, $this->session->userdata('cid')));

        if ($totalcoin->num_rows() > 0) {
            return $totalcoin->row()->total_coins;
        } else {
            return '0';
        }
    }

    function getTransactionTotalToken() {
        /* $this->db->select('SUM(total) as total_coins');
          $this->db->from('transaction_mast');
          $this->db->where(array('status'=>'1','user_id' =>$this->session->userdata('cid')));
          $totalcoin = $this->db->get(); */

        $sqlWithdrawalCoinsTotal = 'SELECT SUM(total) as total_coins FROM transaction_mast WHERE status = ? AND user_id = ?';
        $totalcoin = $this->db->query($sqlWithdrawalCoinsTotal, array('1', $this->session->userdata('cid')));

        if ($totalcoin->num_rows() > 0) {
            return $totalcoin->row()->total_coins;
        } else {
            return '0';
        }
    }

    function sendCoin($txt_id) {
        $transaction_mast = $this->common->getTransactionMast($txt_id);
        if (!empty($transaction_mast)) {
            $customer_data = $this->common->getCustData($transaction_mast->user_id);
            if ($customer_data->referralcodefrom != '') {
                $refferal_customer = $this->getRefferalCustomer($customer_data->referralcodefrom);
                if (!empty($refferal_customer)) {
                    $refferal_customerwallet = $this->common->getCustWallet($refferal_customer->cust_id);
                    if (!empty($refferal_customerwallet)) {
                        if ($refferal_customer->affiliateusercommission != '') {
                            $commisstion = ($transaction_mast->total * $refferal_customer->affiliateusercommission) / 100;
                        } else {
                            $commisstion = ($transaction_mast->total * 5) / 100;
                        }
                        $totalcoinrefferal = $refferal_customerwallet->total_coins + $commisstion;
                        $refferalwllarray = array('total_coins' => $totalcoinrefferal);
                        $this->db->update('wallet', $refferalwllarray, array('wallet_id' => $refferal_customerwallet->wallet_id));
                        $referearn = array(
                            'reg_date' => date('Y-m-d H:i'),
                            'user' => $customer_data->fullname,
                            'email' => $customer_data->email,
                            'purchase_coins' => $transaction_mast->total,
                            'commission' => $commisstion,
                            'cust_id' => $refferal_customer->cust_id
                        );
                        $this->db->insert('referearn', $referearn);
                    } else {
                        if ($refferal_customer->affiliateusercommission != '') {
                            $commisstion = ($transaction_mast->total * $refferal_customer->affiliateusercommission) / 100;
                        } else {
                            $commisstion = ($transaction_mast->total * 5) / 100;
                        }
                        $totalcoinrefferal = $commisstion;
                        $refferalwllarray = array('cust_id' => $refferal_customer->cust_id, 'total_coins' => $totalcoinrefferal);
                        $this->db->insert('wallet', $refferalwllarray);
                        $referearn = array(
                            'reg_date' => date('Y-m-d H:i'),
                            'user' => $customer_data->fullname,
                            'email' => $customer_data->email,
                            'purchase_coins' => $transaction_mast->total,
                            'commission' => $commisstion,
                            'cust_id' => $refferal_customer->cust_id
                        );
                        $this->db->insert('referearn', $referearn);
                    }

                    $wallet = $this->common->getCustWallet($transaction_mast->user_id);
                    if (!empty($wallet)) {
                        $totalcoin = $wallet->total_coins + $transaction_mast->total;
                        $wllarray = array('total_coins' => $totalcoin);
                        $this->db->update('wallet', $wllarray, array('wallet_id' => $wallet->wallet_id));
                    }
                    $arr = array(
                        'wallet_status' => 1
                    );
                    $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
                    $updated = $this->db->affected_rows();
                    if ($updated) {
                        $from = 'info@capitera.io';
                        $to = trim($this->common->getCustEmail($transaction_mast->user_id));
                        $subject = 'Capitera Token : Your transaction has been approved';
                        $message = '<p>Hi ' . $customer_data->fullname . ' <br><br>  Congratulation, Your transaction has been approved by Capitera Token team. We have credited ' . $transaction_mast->total . ' Capitera Token  in your wallet <br><br> Best Regards, <br> Capitera Token Team,';
                        $this->common->sendEmail($from, $to, $subject, $message);
                        return $txt_id;
                    } else {
                        return '';
                    }
                } else {
                    $wallet = $this->common->getCustWallet($transaction_mast->user_id);
                    if (!empty($wallet)) {
                        $totalcoin = $wallet->total_coins + $transaction_mast->total;
                        $wllarray = array('total_coins' => $totalcoin);
                        $this->db->update('wallet', $wllarray, array('wallet_id' => $wallet->wallet_id));
                    }
                    $arr = array(
                        'wallet_status' => 1
                    );
                    $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
                    $updated = $this->db->affected_rows();
                    if ($updated) {
                        $from = 'info@capitera.io';
                        $to = trim($this->common->getCustEmail($transaction_mast->user_id));
                        $subject = 'Capitera Token : Your transaction has been approved';
                        $message = '<p>Hi ' . $customer_data->fullname . ' <br><br>  Congratulation, Your transaction has been approved by Capitera Token team. We have credited ' . $transaction_mast->total . ' Capitera Token  in your wallet <br><br> Best Regards,<br> Capitera Token Team,';
                        $this->common->sendEmail($from, $to, $subject, $message);
                        return $txt_id;
                    } else {
                        return '';
                    }
                }
            } else {
                $wallet = $this->common->getCustWallet($transaction_mast->user_id);
                if (!empty($wallet)) {
                    $totalcoin = $wallet->total_coins + $transaction_mast->total;
                    $wllarray = array('total_coins' => $totalcoin);
                    $this->db->update('wallet', $wllarray, array('wallet_id' => $wallet->wallet_id));
                }
                $arr = array(
                    'wallet_status' => 1
                );
                $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
                $updated = $this->db->affected_rows();
                if ($updated) {
                    $from = 'info@capitera.io';
                    $to = trim($this->common->getCustEmail($transaction_mast->user_id));
                    $subject = 'Capitera Token : Your transaction has been approved';
                    $message = '<p>Hi ' . $customer_data->fullname . ' <br><br>  Congratulation, Your transaction has been approved by Capitera Token team. We have credited ' . $transaction_mast->total . ' Capitera Token  in your wallet <br><br> Best Regards,<br> Capitera Token Team,';
                    $this->common->sendEmail($from, $to, $subject, $message);
                    return $txt_id;
                } else {
                    return '';
                }
            }
        } else {
            return '';
        }
    }

    function getRefferalCustomer($referralcodefrom) {
        $this->db->select('*');
        $this->db->from('customer_master');
        $this->db->where('referralcode', $referralcodefrom);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

    function getCustmerData() {
        $result = $this->db->select('*')->get_where('customer_master', array('cust_id' => $this->session->userdata('cid')))->row()->verified_status;
        return $result;
    }

    function getAddtionalCharge() {
        $this->db->select('*');
        $this->db->from('additional_charge');
        $this->db->where('additional_charge_id', 1);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row()->charge;
        } else {
            return '0';
        }
    }

    function getICORoundData($ico_round) {
        $this->db->select('*');
        $this->db->from('ico_round');
        $this->db->where('ico_round_id', $ico_round);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

    function addPaypalTransaction($post) {
        if (!empty($post)) {
            $icoround = $this->getIcoRound();
            $arr = array(
                'method' => $post['method_1'],
                'user_id' => $this->session->userdata('cid'),
                'user_name' => $this->session->userdata('cname'),
                'amount' => $post['amount_1'],
                'crypto' => $post['amount_1'],
                'coins' => $post['coin_1'],
                'bonus' => $post['bonus_1'],
                'total' => $post['total_bonus_1'],
                'status' => 0,
                'action' => 1,
                'transaction_date' => date('Y-m-d H:i'),
                'round_id' => $icoround->ico_round_id
            );
            $this->db->insert('transaction_mast', $arr);
            return $this->db->insert_id();
        } else {
            return '';
        }
    }

    function paypalSuccessPayment($transctionid) {
        $transctionDATA = $this->getTransactionData($transctionid);
        $from = 'info@capitera.io';
        $to = trim($this->common->getCustEmail($this->session->userdata("cid")));
        $subject = 'Capitera Token : Your transaction is pending';
        $message = '<p>Hi ' . $this->session->userdata('cname') . ' <br><br> Thank you for your purchase of ' . $transctionDATA->total . ' Capitera Token via ' . $transctionDATA->method . ' as the payment method. <br><br> <b>Transaction Details :- </b> <br> Amount in USD : ' . $transctionDATA->amount . '<br> Paid via : ' . $transctionDATA->method . ' <br> You will get : ' . $transctionDATA->total . ' Capitera Token <br><br> We need at least 12-24 hours to confirm your transaction, Once it approved you can see ' . $transctionDATA->total . ' Capitera Token in your wallet. <br><br><br> Best Regards,<br>Capitera Token Team.<p>';
        $this->common->sendEmail($from, $to, $subject, $message);
        return $transctionid;
    }

    function getTransactionData($transctionid) {
        $this->db->select('*');
        $this->db->from('transaction_mast');
        $this->db->where('txn_id', $transctionid);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

    function add_transaction_offline($post) {
        if (!empty($post)) {
            $icoround = $this->getIcoRound();
            $arr = array(
                'method' => $post['method_offline'],
                'user_id' => $this->session->userdata('cid'),
                'user_name' => $this->session->userdata('cname'),
                'amount' => $post['amount_offline'],
                'crypto' => $post['amount_offline'],
                'coins' => $post['coin_offline'],
                'bonus' => $post['bonus_offline'],
                'total' => $post['total_bonus_offline'],
                'status' => 0,
                'action' => 1,
                'transaction_date' => date('Y-m-d H:i'),
                'round_id' => $icoround->ico_round_id
            );
            $this->db->insert('transaction_mast', $arr);
            $transctionid = $this->db->insert_id();
            $transctionDATA = $this->getTransactionData($transctionid);
            $from = 'info@capitera.io';
            $to = trim($this->common->getCustEmail($this->session->userdata("cid")));
            $subject = 'Capitera Token : Your transaction is pending';
            $message = '<p>Hi ' . $this->session->userdata('cname') . ' <br><br> Thank you for your purchase of ' . $transctionDATA->total . ' Capitera Token via ' . $transctionDATA->method . ' as the payment method. <br><br> <b>Transaction Details :- </b> <br> Amount in USD : ' . $transctionDATA->amount . '<br> Paid via : ' . $transctionDATA->method . ' <br> You will get : ' . $transctionDATA->total . ' Capitera Token <br><br> We need at least 12-24 hours to confirm your transaction, Once it approved you can see ' . $transctionDATA->total . ' Capitera Token in your wallet. <br><br><br> Best Regards,<br>Capitera Token Team.<p>';
            $this->common->sendEmail($from, $to, $subject, $message);
            return $transctionid;
        } else {
            return '';
        }
    }

    public function getTokenPackage() {
        $this->db->select('*');
        $this->db->from('token_package');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }
    }

}
