<?php
class M_setting extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    public function checkTwoAuth(){
        $id = $this->session->userdata('aid');
        $result = $this->db->select('username, two_auth, google_auth_code')->where('admin_id', $id)->get('admin');
        return $result->row();
    }
    
    public function updateSetting($two_auth) {
        $id = $this->session->userdata('aid');
        $this->db->set(array('two_auth'=>$two_auth))->where('admin_id', $id)->update('admin');
        return $this->db->affected_rows();
    }
    
    public function update_googleCode($secret){
        $id = $this->session->userdata('aid');
        $this->db->set('google_auth_code', $secret)->where('admin_id', $id)->update('admin');
        return $this->db->affected_rows();
    }
}