<?php
class Muser extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    function getUserData(){
        $this->db->select('c.cust_id,c.fullname,c.email,c.phone,c.country,c.status,c.register_date,c.ethereum_address,c.affiliateusercommission,w.total_coins,w.amount,verified_status');
	    $this->db->from('customer_master c');
	    $this->db->join('wallet w','w.cust_id = c.cust_id','left');
	    $this->db->where(array('status'=>1));
	    $this->db->order_by("c.cust_id","DESC");
	    $user = $this->db->get();
	    if($user->num_rows() > 0)
	    {
	         return $user->result();
	    } else {
	         return '';
	    }    
    }
    
    function filter_search(){
        $post = $this->input->post();
      
        $this->db->select('*');
        $this->db->from('customer_master');
        
        ($post['start_date'] != "") ? $where['DATE(register_date) >='] = date('Y-m-d',strtotime($post['start_date'])) : '';
        ($post['end_date'] != "") ? $where['DATE(register_date) <='] = date('Y-m-d',strtotime($post['end_date'])) : ''; 
        
        ($post['country'] != "") ? $where['country'] = trim($post['country']) : '';
        
        ($post['cname'] != "") ? $where['fullname LIKE'] = '%'.trim($post['cname']).'%' : ''; 
        
        ($post['phone_number'] != "") ? $where['phone LIKE'] = '%'.trim($post['phone_number']).'%' : ''; 
         
        if(!empty($where))
        {
            $this->db->where($where);
        } else { 
            return "";
        }
        
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {           
            return $query->result();
        } else {
            return "";

        }
    }
    
    function deleteUser($userid){
        // delete transaction //
        $this->deleteTransction($userid);
         // delete wallete //
        $this->deleteWallet($userid);
        
        $this->db->where('cust_id',$userid);
        $this->db->delete('customer_master');
        return $userid;
    }
    
    function deleteTransction($userid){
       $this->db->where('user_id',$userid);
       $this->db->delete('transaction_mast'); 
    }
    
    function deleteWallet($userid){
       $this->db->where('cust_id',$userid);
       $this->db->delete('wallet'); 
    }
    
    function add_customer(){
        $post = $this->input->post();
        
        $or_where = '(email = "' . trim($post['email']) . '" or phone = "' . trim($post['phone']) . '")';
        $this->db->where($or_where);
        $customer = $this->db->get('customer_master');
        if($customer->num_rows() > 0) //Check Email or Phone exist with new User 
        {
            return "0"; 
        } else {
            $set = array(
                'fullname' => trim($post['full_name']),
                'email' => trim($post['email']),
                'password' => base64_encode($post['password']),
                'country' => trim($post['country']),
                'status' => 1
            );
            $this->db->insert("customer_master", $set);
            if($this->db->affected_rows() > 0)
            {
                $from = 'info@capitera.io';
                $subject = 'Capitera Token : Your Registration is successful';
                $message = '<p>Congratulation, Your registration is successful. You can login and purchase Capitera Token using bitcoin, litecoin, ethereum and credit/debit cards.<br>Capitera Token Team<p>';
                $this->common->sendEmail($from,trim($post['email']), $subject, $message);
                return "1";
            } else {
                return "2";
            }
        }
    }
    
    function getUserDetail($userid){
        $this->db->select('*');
	    $this->db->from('customer_master');
	    $this->db->where('cust_id',$userid);
	    $user = $this->db->get();
	    if($user->num_rows() > 0)
	    {
	         return $user->row();
	    } else {
	         return '';
	    }    
    }
    
    function updateCustomer($post){
        $set = array(
                'fullname' => trim($post['full_name']),
                'country' => trim($post['country']),
                'affiliateusercommission' =>''
        );
        
        $this->db->update("customer_master",$set,array('cust_id'=>$post['cid']));
        return "1";
    }
     public function get_template_data() 
    {        
        $query = $this->db->get("email_template");
        if($query->num_rows() > 0)
        {
            return $query->result();
        } else {
            return "";
        }
    }
    
    public function send_email($ids, $message, $sub_id) 
    {    
        $sub = $this->common->select_subject($sub_id);

        $data = array(
            'email_subject' => $sub,
            'email_message' => $message,
            'sent_by' => $this->session->userdata('aid'),
            'user_ids' => implode(',',$ids),
            'sent_date' => date('Y-m-d H:i:s')
        );

        $this->db->insert('archive_emails', $data);
        
        foreach ($ids as $id)
        {    
            $customer = $this->db->get_where('customer_master', array('cust_id' => $id))->row(); 
            if(!empty($customer))
            {    
                $to =  trim($customer->email);
                $body = preg_replace('/{NAME}/', $customer->fullname , $message);
                $from = 'info@capitera.io';
                $subject = 'Capitera';
                $this->common->sendEmail($from,$to, $sub, $body); 
            } else {
                continue;
            }
        }
        return TRUE;
    }

    public function getArchiveEmails(){
        $this->db->select('*');
        $this->db->from('archive_emails');
        $this->db->order_by('archive_id', 'DESC');
        $q = $this->db->get();
        if($q->num_rows() > 0){
            return $q->result();
        }else{
            return '';
        }
    }
}