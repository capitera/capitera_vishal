<?php

class M_portfolio extends CI_Model {

    function __construct() {
        parent::__construct();
         $this->load->helper('string');
    }

    function getPortfolioData() {
        $this->db->select('*');
        $this->db->from('portfolio p');
        $this->db->join('assets_currency a','p.assets_base=a.currency_wallet_id');
        $this->db->order_by("portfolio_id", "desc");
        $portfolio = $this->db->get();
        if ($portfolio->num_rows() > 0) {
            return $portfolio->result();
        } else {
            return '';
        }
    }

    function editPortfolio($p_id) {
        $this->db->select('*');
        $this->db->from('portfolio');
        $this->db->where("portfolio_id", $p_id);
        $portfolio = $this->db->get();
        if ($portfolio->num_rows() > 0) {
            return $portfolio->row();
        } else {
            return '';
        }
    }

    function getAssetsCurrencyData() {
        $this->db->select('*');
        $this->db->from('assets_currency');
        $this->db->where("currency_status", 1);
        $assets_currency = $this->db->get();
        if ($assets_currency->num_rows() > 0) {
            return $assets_currency->result();
        } else {
            return '';
        }
    }

    function addNewPortfolios() {
        $post = $this->input->post();
        $insert_array = array(
            'name' => trim($post['name']),
            'manager' => trim($post['manager']),
            'amount_invest' => trim($post['amount_invest']),
            'profit_target' => 0,
            'loss' => 0,
            'start_date' => date("Y-m-d H:i:s", strtotime($post['start_date'])),
            'end_date' => date("Y-m-d H:i:s", strtotime($post['end_date'])),
            'assets_base' => trim($post['assets']),
            'risk_level' => trim($post['risk_level']),
            'reg_date' => date('Y-m-d H:i:s')
        );
        $this->db->insert("portfolio", $insert_array);
        $last_id = $this->db->insert_id();
        
        if ($_FILES['portfolio_image']['size'] != 0) {
            $config = array(
                'upload_path' => './uploads/portfolio_image/',
                'allowed_types' => "jpg|png|jpeg",
                'overwrite' => TRUE
            );
            $this->load->library('upload', $config);
            $_FILES["portfolio_image"]['name'] = $_FILES['portfolio_image']['name'];
            $_FILES["portfolio_image"]['type'] = $_FILES['portfolio_image']['type'];
            $_FILES["portfolio_image"]['tmp_name'] = $_FILES['portfolio_image']['tmp_name'];
            $_FILES["portfolio_image"]['error'] = $_FILES['portfolio_image']['error'];
            $_FILES["portfolio_image"]['size'] = $_FILES['portfolio_image']['size'];

            $file_name = random_string('alnum', 16);
            $config['file_name'] = $file_name;
            $this->upload->initialize($config);
            $this->upload->do_upload("portfolio_image");
            $imageDetailArray = $this->upload->data();
            $this->db->set('portfolio_image', $imageDetailArray['file_name'])->where('portfolio_id', $last_id)->update('portfolio');
        }
        return TRUE;
    }

    function approvePortfolios($p_id) {
        $arr = array(
            'status' => 1
        );
        $this->db->update('portfolio', $arr, array('portfolio_id' => $p_id));
        $updated = $this->db->affected_rows();
        if ($updated) {
            return $p_id;
        } else {
            return '';
        }
    }

    function rejectPortfolios($p_id) {
        $arr = array(
            'status' => 2
        );
        $this->db->update('portfolio', $arr, array('portfolio_id' => $p_id));
        $updated = $this->db->affected_rows();
        if ($updated) {
            return $p_id;
        } else {
            return '';
        }
    }

    function deletePortfolios($p_id) {
        $this->db->delete('portfolio', array('portfolio_id' => $p_id));
        return $p_id;
    }

    function updatePortfolio($post) {
        $update_array = array(
            'name' => trim($post['name']),
            'manager' => trim($post['manager']),
            'amount_invest' => trim($post['amount_invest']),
            'start_date' => date("Y-m-d H:i:s", strtotime($post['start_date'])),
            'end_date' => date("Y-m-d H:i:s", strtotime($post['end_date'])),
            'assets_base' => trim($post['assets']),
            'risk_level' => trim($post['risk_level'])
        );
        $this->db->update("portfolio", $update_array, array("portfolio_id" => $post['portfolio_id']));
        $last_id = $post['portfolio_id'];
        if ($_FILES['portfolio_image']['size'] != 0) {
            $config = array(
                'upload_path' => './uploads/portfolio_image/',
                'allowed_types' => "jpg|png|jpeg",
                'overwrite' => TRUE
            );
            $this->load->library('upload', $config);
            $_FILES["portfolio_image"]['name'] = $_FILES['portfolio_image']['name'];
            $_FILES["portfolio_image"]['type'] = $_FILES['portfolio_image']['type'];
            $_FILES["portfolio_image"]['tmp_name'] = $_FILES['portfolio_image']['tmp_name'];
            $_FILES["portfolio_image"]['error'] = $_FILES['portfolio_image']['error'];
            $_FILES["portfolio_image"]['size'] = $_FILES['portfolio_image']['size'];

            $file_name = random_string('alnum', 16);
            $config['file_name'] = $file_name;
            $this->upload->initialize($config);
            $this->upload->do_upload("portfolio_image");
            $imageDetailArray = $this->upload->data();
            $this->db->set('portfolio_image', $imageDetailArray['file_name'])->where('portfolio_id', $last_id)->update('portfolio');
        }
        return TRUE;
    }

}
