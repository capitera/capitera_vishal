<?php
class M_tokenpackage extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    function getPackages(){
        $this->db->select('*');
        $this->db->from('token_package');
        $q=$this->db->get();
        if($q->num_rows() > 0){
            return $q->result();
        }
    }
   
    function package_delete($id){
        $this->db->where('package_id', $id)->delete('token_package');
        return $this->db->affected_rows();
    }

    function editPackage($p){

        $config['upload_path'] = './assets/package/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['file_name'] = random_string('alnum', 10);   
        $this->load->library('upload', $config);

        $arr = array(
            "package_name" => $p['p_name'],
            "price" => $p['p_price'],
            "no_of_token" => $p['no_token']
        );

        if(!empty($_FILES["file"])){
            if($_FILES["file"]["name"] != '') 
            {
                $this->upload->do_upload('file');
                $dt = array('upload_data'=>$this->upload->data());
                $arr['image'] = $dt['upload_data']['file_name'];
            }
        }

        $this->db->update('token_package', $arr, array('package_id'=>$p['package_id']));
        if($this->db->affected_rows()){
            return 1;
        }else{
            return '';
        }
    }

}