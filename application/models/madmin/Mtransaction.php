<?php

class Mtransaction extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getTransactionData() {
        /* $this->db->order_by("txn_id", "desc");
          $result = $this->db->get('transaction_mast'); */
        $this->db->select('t.*, p.package_name, p.price');
        $this->db->from('transaction_mast t');
        $this->db->join('token_package p', 't.package_id = p.package_id');
        $this->db->where("recycle_bin", "0");
        $this->db->order_by("txn_id", "desc");
        $result = $this->db->get();
        return ($result->num_rows() > 0) ? $result->result() : '';
    }

    function getReferralTransactions() {
        $this->db->select('*');
        $this->db->from('referral_master');
        $this->db->order_by("referral_id", "desc");
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return '';
        }
    }

    function getWithdrawData() {
        $this->db->select('*');
        $this->db->from('withdraw w');
        $this->db->join('customer_master c', 'c.cust_id = w.user_id');
        $this->db->order_by("w_id", "desc");
        $result = $this->db->get();
        return ($result->num_rows() > 0) ? $result->result() : '';
    }

    function getHistoryIssueToken() {
        $this->db->select('*');
        $this->db->from('history_issuetoken');
        $this->db->order_by("history_issuetoken_id", "desc");
        $result = $this->db->get();
        return ($result->num_rows() > 0) ? $result->result() : '';
    }

    function sendReferralCoins($txn_id) {

        $q = $this->db->get_where('transaction_mast', array('txn_id' => $txn_id));
        if ($q->num_rows() > 0) {

            $current_transaction = $q->row();
            $coins = $current_transaction->coins;
            $currentUserId = $current_transaction->user_id;
            $currentUserEmail = $this->common->getCustEmail($currentUserId);
            $members = array();
            $userSql = $this->db->get_where('customer_master', array('cust_id' => $currentUserId));
            if ($userSql->num_rows() > 0) {

                $curentUserCodeFrom = $userSql->row()->referralcodefrom;

                if ($curentUserCodeFrom != '') {
                    /* ---- Level 1 Code Starts Here ---- */

                    $one_sql = $this->db->get_where('customer_master', array('referralcode' => $curentUserCodeFrom));

                    if ($one_sql->num_rows() > 0) {
                        $one_result = $one_sql->row();
                        $one_result->cust_id;
                        $one_percentage = $this->common->getCommissionPercentage(1);
                        $one_received_coins = $coins * $one_percentage / 100;

                        $one_data = array(
                            "method" => 'referral',
                            "user_id" => $one_result->cust_id,
                            "user_name" => $one_result->fullname,
                            "coins" => $one_received_coins,
                            "remaining_coin" => $one_received_coins,
                            "status" => 1,
                            "action" => 1,
                            "transaction_date" => date('Y-m-d H:i'),
                        );
                        $this->db->insert('transaction_mast', $one_data);

                        $oneReferralData = array(
                            "reg_date" => date('Y-m-d H:i'),
                            "user" => $current_transaction->user_name,
                            "email" => $currentUserEmail,
                            "purchase_coins" => $coins,
                            "commission" => $one_received_coins,
                            "cust_id" => $one_result->cust_id,
                            "txn_id" => $txn_id,
                            "package_id" => $current_transaction->package_id,
                        );
                        $this->db->insert('referral_master', $oneReferralData);

                        /* ---- Level 1 Code Ends Here ---- */

                        /* ---- Level 2 Code Starts Here ---- */

                        $second_sql = $this->db->get_where('customer_master', array('referralcode' => $one_result->referralcodefrom));

                        if ($second_sql->num_rows() > 0) {
                            $second_result = $second_sql->row();
                            $second_result->cust_id;
                            $two_percentage = $this->common->getCommissionPercentage(2);
                            $two_received_coins = $coins * $two_percentage / 100;

                            $two_data = array(
                                "method" => 'referral',
                                "user_id" => $second_result->cust_id,
                                "user_name" => $second_result->fullname,
                                "coins" => $two_received_coins,
                                "remaining_coin" => $two_received_coins,
                                "status" => 1,
                                "action" => 1,
                                "transaction_date" => date('Y-m-d H:i'),
                            );
                            $this->db->insert('transaction_mast', $two_data);

                            $secondReferralData = array(
                                "reg_date" => date('Y-m-d H:i'),
                                "user" => $current_transaction->user_name,
                                "email" => $currentUserEmail,
                                "purchase_coins" => $coins,
                                "commission" => $two_received_coins,
                                "cust_id" => $second_result->cust_id,
                                "txn_id" => $txn_id,
                                "package_id" => $current_transaction->package_id,
                            );
                            $this->db->insert('referral_master', $secondReferralData);

                            /* ---- Level 2 Code Ends Here ---- */

                            /* ---- Level 3 Code Starts Here ---- */

                            $three_sql = $this->db->get_where('customer_master', array('referralcode' => $second_result->referralcodefrom));

                            if ($three_sql->num_rows() > 0) {
                                $three_result = $three_sql->row();
                                $three_result->cust_id;
                                $three_percentage = $this->common->getCommissionPercentage(3);
                                $three_received_coins = $coins * $three_percentage / 100;

                                $three_data = array(
                                    "method" => 'referral',
                                    "user_id" => $three_result->cust_id,
                                    "user_name" => $three_result->fullname,
                                    "coins" => $three_received_coins,
                                    "remaining_coin" => $three_received_coins,
                                    "status" => 1,
                                    "action" => 1,
                                    "transaction_date" => date('Y-m-d H:i'),
                                );
                                $this->db->insert('transaction_mast', $three_data);

                                $thirdReferralData = array(
                                    "reg_date" => date('Y-m-d H:i'),
                                    // "user" =>  $three_result->user_name,
                                    "user" => $current_transaction->user_name,
                                    "email" => $currentUserEmail,
                                    "purchase_coins" => $coins,
                                    "commission" => $three_received_coins,
                                    "cust_id" => $three_result->cust_id,
                                    "txn_id" => $txn_id,
                                    "package_id" => $current_transaction->package_id,
                                );
                                $this->db->insert('referral_master', $thirdReferralData);
                            } else {
                                return '';
                            }
                            /* ---- Level 3 Code Ends Here ---- */
                        } else {
                            return '';
                        }
                    } else {
                        return '';
                    }
                } else {
                    return '';
                }
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    function approveTransaction($txt_id) {
        $transaction_mast = $this->common->getTransactionMast($txt_id);
        if (!empty($transaction_mast)) {
            $wallet = $this->common->getCustWallet($transaction_mast->user_id);

            if (!empty($wallet)) {
                $totalamount = $wallet->amount + $transaction_mast->amount;
                $wllarray = array('amount' => $totalamount);
                $this->db->update('wallet', $wllarray, array('cust_id' => $transaction_mast->user_id));
            } else {
                $wllarray = array('cust_id' => $transaction_mast->user_id, 'total_coins' => 0, 'amount' => $transaction_mast->amount);
                $this->db->insert('wallet', $wllarray);
            }
            $arr = array(
                'status' => 1
            );
            $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
            $updated = $this->db->affected_rows();
            if ($updated) {
                return $txt_id;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    function rejectTransaction($txt_id) {
        $arr = array(
            'status' => 2
        );
        $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
        $updated = $this->db->affected_rows();
        if ($updated) {
            return $txt_id;
        } else {
            return '';
        }
    }

    function filter_search() {
        $post = $this->input->post();

        $this->db->select('*');
        $this->db->from('transaction_mast');

        ($post['start_date'] != "") ? $where['DATE(transaction_date) >='] = date('Y-m-d', strtotime($post['start_date'])) : '';
        ($post['end_date'] != "") ? $where['DATE(transaction_date) <='] = date('Y-m-d', strtotime($post['end_date'])) : '';

        ($post['method'] != "") ? $where['method'] = trim($post['method']) : '';

        if ($post['usd'] == '1') {
            ($post['usd'] != "") ? $where['amount <'] = '50' : '';
        } elseif ($post['usd'] == '2') {
            ($post['usd'] != "") ? $where['amount >'] = '50' : '';
            ($post['usd'] != "") ? $where['amount <'] = '100' : '';
        } elseif ($post['usd'] == '3') {
            ($post['usd'] != "") ? $where['amount >'] = '100' : '';
            ($post['usd'] != "") ? $where['amount <'] = '500' : '';
        } elseif ($post['usd'] == '4') {
            ($post['usd'] != "") ? $where['amount >'] = '500' : '';
            ($post['usd'] != "") ? $where['amount <'] = '1000' : '';
        } elseif ($post['usd'] == '5') {
            ($post['usd'] != "") ? $where['amount >'] = '1000' : '';
            ($post['usd'] != "") ? $where['amount <'] = '2000' : '';
        } elseif ($post['usd'] == '6') {
            ($post['usd'] != "") ? $where['amount >'] = '2000' : '';
        }

        ($post['cname'] != "") ? $where['user_name LIKE'] = '%' . trim($post['cname']) . '%' : '';

        if (!empty($where)) {
            $this->db->where($where);
        } else {
            return "";
        }
        $this->db->order_by("txn_id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {

            return "";
        }
    }

    public function delete($ids) {
        foreach ($ids as $id) {
            $did = intval($id);
            //$this->db->where('txn_id', $did);
            //$this->db->delete("transaction_mast");
            $this->db->update('transaction_mast', array('recycle_bin' => 1), array('txn_id' => $did));
        }
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function sendCoin($txt_id) {
        $transaction_mast = $this->common->getTransactionMast($txt_id);
        if (!empty($transaction_mast)) {
            $customer_data = $this->common->getCustData($transaction_mast->user_id);
            $wallet = $this->common->getCustWallet($transaction_mast->user_id);
            if (!empty($wallet)) {
                $totalcoin = $wallet->total_coins + $transaction_mast->total;
                $wllarray = array('total_coins' => $totalcoin);
                $this->db->update('wallet', $wllarray, array('wallet_id' => $wallet->wallet_id));
            }
            $arr = array(
                'wallet_status' => 1
            );
            $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
            $updated = $this->db->affected_rows();
            if ($updated) {
                $from = 'info@capitera.io';
                $to = trim($this->common->getCustEmail($transaction_mast->user_id));
                $subject = 'Capitera Token : Your transaction has been approved';
                $message = '<p>Hi ' . $customer_data->fullname . ' <br><br> Congratulation, Your transaction has been approved by Capitera Token team. We have credited ' . $transaction_mast->total . ' Capitera Token  in your wallet <br><br> Best Regards,<br> Capitera Token Team </p>';
                $this->common->sendEmail($from, $to, $subject, $message);
                return $txt_id;
            } else {
                return '';
            }

            /* if ($customer_data->referralcodefrom != '') {
              $refferal_customer = $this->getRefferalCustomer($customer_data->referralcodefrom);
              if (!empty($refferal_customer)) {
              $refferal_customerwallet = $this->common->getCustWallet($refferal_customer->cust_id);
              if (!empty($refferal_customerwallet)) {
              if ($refferal_customer->affiliateusercommission != '') {
              $commisstion = ($transaction_mast->total * $refferal_customer->affiliateusercommission) / 100;
              } else {
              $commisstion = ($transaction_mast->total * 5) / 100;
              }
              $totalcoinrefferal = $refferal_customerwallet->total_coins + $commisstion;
              $refferalwllarray = array('total_coins' => $totalcoinrefferal);
              $this->db->update('wallet', $refferalwllarray, array('wallet_id' => $refferal_customerwallet->wallet_id));
              $referearn = array(
              'reg_date' => date('Y-m-d H:i'),
              'user' => $customer_data->fullname,
              'email' => $customer_data->email,
              'purchase_coins' => $transaction_mast->total,
              'commission' => $commisstion,
              'cust_id' => $refferal_customer->cust_id
              );
              $this->db->insert('referearn', $referearn);
              } else {
              if ($refferal_customer->affiliateusercommission != '') {
              $commisstion = ($transaction_mast->total * $refferal_customer->affiliateusercommission) / 100;
              } else {
              $commisstion = ($transaction_mast->total * 5) / 100;
              }
              $totalcoinrefferal = $commisstion;
              $refferalwllarray = array('cust_id' => $refferal_customer->cust_id, 'total_coins' => $totalcoinrefferal);
              $this->db->insert('wallet', $refferalwllarray);
              $referearn = array(
              'reg_date' => date('Y-m-d H:i'),
              'user' => $customer_data->fullname,
              'email' => $customer_data->email,
              'purchase_coins' => $transaction_mast->total,
              'commission' => $commisstion,
              'cust_id' => $refferal_customer->cust_id
              );
              $this->db->insert('referearn', $referearn);
              }

              $wallet = $this->common->getCustWallet($transaction_mast->user_id);
              if (!empty($wallet)) {
              $totalcoin = $wallet->total_coins + $transaction_mast->total;
              $wllarray = array('total_coins' => $totalcoin);
              $this->db->update('wallet', $wllarray, array('wallet_id' => $wallet->wallet_id));
              }
              $arr = array(
              'wallet_status' => 1
              );
              $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
              $updated = $this->db->affected_rows();
              if ($updated) {
              $from = 'info@capitera.io';
              $to = trim($this->common->getCustEmail($transaction_mast->user_id));
              $subject = 'ERA Coin : Your transaction has been approved';
              $message = '<p>Hi ' . $customer_data->fullname . ' <br><br> Congratulation, Your transaction has been approved by ERA Coin team. We have credited ' . $transaction_mast->total . ' ERA Coin  in your wallet <br><br> Best Regards,<br>ERA Coin Team</p>';
              $this->common->sendEmail($from, $to, $subject, $message);
              return $txt_id;
              } else {
              return '';
              }
              } else {
              $wallet = $this->common->getCustWallet($transaction_mast->user_id);
              if (!empty($wallet)) {
              $totalcoin = $wallet->total_coins + $transaction_mast->total;
              $wllarray = array('total_coins' => $totalcoin);
              $this->db->update('wallet', $wllarray, array('wallet_id' => $wallet->wallet_id));
              }
              $arr = array(
              'wallet_status' => 1
              );
              $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
              $updated = $this->db->affected_rows();
              if ($updated) {
              $from = 'info@capitera.io';
              $to = trim($this->common->getCustEmail($transaction_mast->user_id));
              $subject = 'ERA Coin : Your transaction has been approved';
              $message = '<p>Hi ' . $customer_data->fullname . ' <br><br> Congratulation, Your transaction has been approved by ERA Coin team. We have credited ' . $transaction_mast->total . ' ERA Coin  in your wallet <br><br> Best Regards,<br>ERA Coin Team</p>';
              $this->common->sendEmail($from, $to, $subject, $message);
              return $txt_id;
              } else {
              return '';
              }
              }
              } else {
              $wallet = $this->common->getCustWallet($transaction_mast->user_id);
              if (!empty($wallet)) {
              $totalcoin = $wallet->total_coins + $transaction_mast->total;
              $wllarray = array('total_coins' => $totalcoin);
              $this->db->update('wallet', $wllarray, array('wallet_id' => $wallet->wallet_id));
              }
              $arr = array(
              'wallet_status' => 1
              );
              $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
              $updated = $this->db->affected_rows();
              if ($updated) {
              $from = 'info@capitera.io';
              $to = trim($this->common->getCustEmail($transaction_mast->user_id));
              $subject = 'ERA Coin : Your transaction has been approved';
              $message = '<p>Hi ' . $customer_data->fullname . ' <br><br> Congratulation, Your transaction has been approved by ERA Coin team. We have credited ' . $transaction_mast->total . ' ERA Coin  in your wallet <br><br> Best Regards,<br> ERA Coin Team </p>';
              $this->common->sendEmail($from, $to, $subject, $message);
              return $txt_id;
              } else {
              return '';
              }
              } */
        } else {
            return '';
        }
    }

    function getICORoundData($ico_round) {
        $this->db->select('*');
        $this->db->from('ico_round');
        $this->db->where('ico_round_id', $ico_round);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

    function getRefferalCustomer($referralcodefrom) {
        $this->db->select('*');
        $this->db->from('customer_master');
        $this->db->where('referralcode', $referralcodefrom);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

    function view_trash() {
        $this->db->select('t.*, p.package_name, p.price');
        $this->db->from('transaction_mast t');
        $this->db->join('token_package p', 't.package_id = p.package_id');
        $this->db->where("recycle_bin", "1");
        $this->db->order_by("txn_id", "desc");
        $result = $this->db->get();
        return ($result->num_rows() > 0) ? $result->result() : '';
    }

    function restoreTransaction($txt_id) {
        $arr = array(
            'recycle_bin' => 0
        );
        $this->db->update('transaction_mast', $arr, array('txn_id' => $txt_id));
        $updated = $this->db->affected_rows();
        if ($updated) {
            return $txt_id;
        } else {
            return '';
        }
    }

}
