<?php

class M_otp extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
	function get_otp($p)
	{
		$whr = array('admin_id'=>base64_decode($p['cid']));
		$q = $this->db->get_where('admin',$whr);
		return $q->row();
	}
}