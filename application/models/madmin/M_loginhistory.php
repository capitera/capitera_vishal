<?php
class M_loginhistory extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function getLoginHistory()
    {
    	$this->db->select('*');
    	$this->db->from('adminloginhistory');
    	$this->db->where('user_id',$this->session->userdata('aid'));
    	$this->db->order_by("adminloginhistory_id","desc");
        $result = $this->db->get();
        return ($result->num_rows() > 0) ? $result->result() : '';
    }
}