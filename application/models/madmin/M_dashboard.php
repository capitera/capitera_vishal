<?php

class M_dashboard extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getRegUser() {
        $result = $this->db->get_where('customer_master', array('status' => '1'));
        return ($result->num_rows() > 0) ? $result->num_rows() : '0';
    }

    function getPortfolios() {
        $result = $this->db->get_where('portfolio');
        return ($result->num_rows() > 0) ? $result->num_rows() : '0';
    }

    function getBitcoins() {
        $this->db->select('SUM(crypto) as totalcrypto');
        $this->db->from('transaction_mast');
        $this->db->where(array('method' => 'bitcoin', 'status' => 1));
        $bitcoins = $this->db->get();
        if ($bitcoins->num_rows() > 0) {
            return $bitcoins->row();
        } else {
            return '0';
        }
    }

    function getPendingKYC() {
        $this->db->select('count(cust_id) as ptotal');
        $this->db->from('customer_master');
        $this->db->where(array('verified_status' => 0));
        $pending = $this->db->get();
        if ($pending->num_rows() > 0) {
            return $pending->row()->ptotal;
        } else {
            return '0';
        }
    }

    function getEthereum() {
        $this->db->select('SUM(crypto) as totalcrypto');
        $this->db->from('transaction_mast');
        $this->db->where(array('method' => 'ethereum', 'status' => 1));
        $bitcoins = $this->db->get();
        if ($bitcoins->num_rows() > 0) {
            return $bitcoins->row();
        } else {
            return '0';
        }
    }

    function getLitecoin() {
        $this->db->select('SUM(crypto) as totalcrypto');
        $this->db->from('transaction_mast');
        $this->db->where(array('method' => 'litecoin', 'status' => 1));
        $bitcoins = $this->db->get();
        if ($bitcoins->num_rows() > 0) {
            return $bitcoins->row();
        } else {
            return '0';
        }
    }

    function getTotalUsd() {
        $this->db->select('SUM(amount) as totalusd');
        $this->db->from('transaction_mast');
        $this->db->where(array('status' => 1));
        $bitcoins = $this->db->get();
        if ($bitcoins->num_rows() > 0) {
            return $bitcoins->row();
        } else {
            return '0';
        }
    }

    function getTotalCoins() {
        $this->db->select('SUM(coins) as totalcoin');
        $this->db->from('transaction_mast');
        $this->db->where(array('status' => 1));
        $this->db->where('total is NOT NULL', NULL, FALSE);
        $bitcoins = $this->db->get();
        if ($bitcoins->num_rows() > 0) {
            return $bitcoins->row();
        } else {
            return '0';
        }
    }

    function getPackages($pid) {
        $this->db->select('SUM(quantity) as totalPackage');
        $this->db->from('transaction_mast');
        $this->db->where(array('status' => 1, 'package_id' => $pid));
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->row()->totalPackage;
        } else {
            return '0';
        }
    }

}
