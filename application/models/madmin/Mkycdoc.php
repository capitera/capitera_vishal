<?php

class Mkycdoc extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getKycData($id) {
        $whr = array("cust_id" => $id);
        $q = $this->db->get_where('customer_master', $whr);
        return $q->result();
    }

    function getAllKycData() {
        $this->db->select('*');
        $this->db->from('customer_master');
        // $this->db->where_in('verified_status', array('0', '2'));
        $q = $this->db->get();
        return $q->result();
    }

}
