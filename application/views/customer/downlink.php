<div class="row">
    <div class="col-md-5 custom-padding-column">
        <div class="card">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="col-12 col-md-12">
                        <div class="card-header-transparent">
                            <h2 class="downlink-head">
                                <img src="<?= base_url() ?>newassets/app-assets/images/icon1.png" width="50px" height="50px"> 
                                <span>TRIANGLE</span>
                            </h2>
                        </div>
                    </div>
                    <div class="col-12 col-md-12">
                        <div class="row mt-2">
                            <div class="col-md-4 col-4">
                                <ul class="p-0">
                                    <li>Base</li>
                                    <li>Risk Level</li>
                                    <li>Manager</li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-4">
                                <ul class="p-0">
                                    <li>BTC</li>
                                    <li>High</li>
                                    <li>Martin Reyes</li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-4">
                                <p class="rating-num m-0">4.0</p>
                                <div class="rating">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                </div>
                                <div>
                                    <span>1,256 reviews</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <div class="col-md-7 custom-padding-column">
        <div class="card">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 col-md-4">
                            <div class="right-downlink-box mb-5">
                                <p class="downlink-head">Profit Target</p>
                                <h1>450%</h1>
                            </div>
                            <div class="mb-5">
                                <p class="downlink-head">Time Limit</p>
                                <h4 class="downlink-head">24 hours</h4>
                            </div>
                            <div>
                                <a href="#" class="btn-sm btn btn-outline-primary bnt-sm">BUY</a>
                                <a href="#" class="btn-sm btn btn-outline-warning bnt-sm">RENT</a>
                            </div>
                        </div>
                        <div class="col-8 col-md-8">
                            <div class="row mb-2">
                                <div class="col-6 col-md-4">
                                    <div class="right-downlink-box">
                                        <p class="downlink-head">Max Amount</p>
                                        <h5 right-downlink-box>$ 50,000</h5>
                                    </div>
                                </div>
                                <div class="col-6 col-md-4">
                                    <div class="right-downlink-box">
                                        <p class="downlink-head">Available</p>
                                        <h5 right-downlink-box>$ 50,421</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-1">    
                                <div class="col-6 col-md-4">
                                    <div class="right-downlink-box">
                                        <p class="downlink-head">Required</p>
                                        <h5 right-downlink-box>$ 50,000 ERA</h5>
                                    </div>
                                </div>
                                <div class="col-6 col-md-4">
                                    <div class="right-downlink-box">
                                        <p class="downlink-head">Available</p>
                                        <h5 right-downlink-box>$ 80,750 ERA</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="row">    
                                <div class="col-12 col-md-12">
                                    <div class="right-downlink-box">
                                        <form name="invest-form" method="post">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <div class="form-group">
                                                <label class="form-label">Invest</label>
                                                <input type="text" name="invest-value" class="form-control" placeholder="Enter Value">
                                                <button class="btn btn-secondary btn-sm mt-1" type="button">INVEST NOW</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-5 custom-padding-column">
        <div class="card">
            <div class="card-content collapse show">
                <div class="card-header card-header-transparent">
                    <h6 class="card-title">IMPORTANT INFORMATION</h6>
                </div>
                <div class="card-body">
                    <div class="col-12 col-md-12">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <div class="col-md-7 custom-padding-column">
        <div class="card">
            <div class="card-content">
                <div class="card-header card-header-transparent">
                    <h6 class="card-title">ASSETS & DIRECTION</h6>
                </div>

                <div class=" table-responsive">
                    <table class="table table-bordered table-striped text-left" id="tbl_transaction">
                        <thead>
                            <tr>
                                <th>Asset Symbol</th> 
                                <th>Asset Name</th>                                
                                <th>Value</th>
                                <th>Trade</th>
                                <th>Share</th>
                                <th>Direction</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>ETH</td>
                                <td>Ethereum</td>
                                <td>$220</td>                                                     
                                <td>Short</td> 
                                <td>60%</td> 
                                <td>4,7%</td> 
                            </tr>
                            <tr>
                                <td>BTC</td>
                                <td>Bitcoin</td>
                                <td>$9000</td>                                                     
                                <td>Long</td> 
                                <td>20%</td> 
                                <td>2,7%</td> 
                            </tr>
                            <tr>
                                <td>LTC</td>
                                <td>Litecoin</td>
                                <td>$170</td>                                                     
                                <td>Long</td> 
                                <td>5%</td> 
                                <td>20,7%</td> 
                            </tr>
                        </tbody>
                    </table>
                </div>  

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Reviews</h4>
            </div>
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-md-2 pt-2">
                        <div class="rating">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div>
                            <span>1,256 reviews</span>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <h6 class="text-right downlink-head">20 October 2019</h6>
                        <h4 class="text-left downlink-head">The Most Reliable</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 pt-2">
                        <div class="rating">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-o"></span>
                        </div>
                        <div>
                            <span>1,256 reviews</span>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <h6 class="text-right downlink-head">20 October 2019</h6>
                        <h4 class="text-left downlink-head">The Most Reliable</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
$msg = $this->input->get("msg");
switch ($msg) {
    case "S":
        $m = "Invitation successfully sent...";
        $t = "success";
        break;
    case "E":
        $m = "Something went wrong please try again...!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl_transaction").dataTable({
            "ordering": true
        });


<?php if ($msg): ?>
            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>


        $(".clickBtn").click(function () {
            $('#wallet-address').select();
            document.execCommand('copy');
            $('#copymsg').text('Copied');
        });
    });
</script>