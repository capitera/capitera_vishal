<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">MY Card</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content">
                <div class="row" style="margin-left:0px; margin-right: 0px;">
                    <div class="col-md-12 table-responsive pl-0 pr-0">
                        <table class="table table-bordered table-striped" id="tbl_transaction">
                            <thead>
                                <tr>
                                    <th>Date</th> 
                                    <th>Method</th>             
                                    <th>USD</th>
                                    <th>Quantity</th>
                                    <th>Package</th>
                                    <th>Token</th>
                                    <th>Status</th>                                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2019-5-20</td>
                                    <td>BTC</td>                            
                                    <td>100$</td>
                                    <td>0.255</td>
                                    <td>ET</td>
                                    <td>100</td>
                                    <td>SS</td>
                                   
                                </tr>
                            </tbody>
                        </table>
                    </div>                                    
                </div>         
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl_transaction").dataTable({
             "ordering": true,
        });
    });
</script>
