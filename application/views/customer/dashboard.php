<style>
    .btn-gradient-secondary {
        background-image: linear-gradient(to right, #4CAF50 0%, #8BC34A 51%, #4CAF50 100%);
    }

    .social_icons li{
        margin: 5px;
    }

    .ct-labels .ct-label{
        -webkit-transform: rotate(-35deg);
        -moz-transform: rotate(-35deg);
        -ms-transform: rotate(-35deg);
        -o-transform: rotate(-35deg);
        transform: rotate(-35deg);
    }
    #my_portfoliyo{
        margin-top: 25px;   
    }
    #buy_hold{
        margin-top: 25px; 
    }
    #my_itera{
        margin-top: 25px;  
    }
    #my_wallet{
        margin-top: 25px; 
    }

    .ct-label{
        font-size: 10px;
    }
    .ct-label {
        fill: rgba(255, 255, 255);
        color: rgb(255, 255, 255);
        font-size: 0.75rem;
        line-height: 1;
    }
    /* START: Custom css */
    ul.token-distribution-list li.power::before {
        background: #ec7c68;
    }

    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px; 
        max-width: 500px;
        margin: 1em auto;
    }

    #container {
        height: 400px;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    ul.token-distribution-list li.crowd-sale::before {
        background: #2372a8;
    }
    ul.token-distribution-list li.team::before {
        background: #3bc1e6;
    }
    ul.token-distribution-list li.advisors::before {
        background: #2c3384;
    }
    ul.token-distribution-list li.power::before {
        background: #c6fff8;
    }


    ul.token-distribution-list li.graph1::before {
        background: #092e38;
    }
    ul.token-distribution-list li.graph2::before {
        background: #1c8691;
    }
    ul.token-distribution-list li.graph3::before {
        background: #35ffb8;
    }

    ul.token-distribution-list li.graph4::before {
        background: #72b0b3;
    }
    .header_padding_css{
        padding: 14px 15px 14px 15px;
    }
    .card_title_css{
        padding-top: 5px  
    }
    .btn_padding{
        padding: 0px 14px 0px 0px;
        text-align: right;
    }
    .btn_stye_in_btn{
        padding: 7px 11px 7px 11px;
        width: auto;
    }
    .box_cust_css {
        background-color: #f7f7f7 !important;
    }
    .text-muted {
        color: #272727 !important;
    }
    /* END: Custom css */
</style>
<div class="row">
    <div class="col-xl-3 col-lg-12 box-shadow" style="padding-left: 5px; padding-right: 5px;">
        <div class="card profile-card-with-cover box_cust_css">
            <div class="card-content card-deck" style="margin-bottom: 0rem;">
                <div class="card box-shadow box_cust_css">
                    <div class="card-header card-header-transparent header_padding_css">
                        <div class="row">
                            <div class="col-8">
                                <h6 class="card-title card_title_css">My PORTFOLIO</h6>
                            </div>
                            <div class="col-4 btn_padding">
                                <a href="<?= base_url() ?>customer/invest" class="btn-secondary btn-gradient-css btn-sm btn_stye_in_btn">Invest</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-content" id="click_to_portfoliyo">
                        <div id="my_portfoliyo_pie" style="margin: 0; height: 225px;"></div>
                        <div class="card-body">
                            <div class="row mx-0">
                                <ul class="token-distribution-list col-md-12 mb-0"  style="font-size: 12px; color: #272727;">
                                    <li class="crowd-sale">PRISM <span class="float-right text-muted">$12000 (15.7%)</span></li>
                                    <li class="team">HARMONY<span class="float-right text-muted">$7501.1 (9.85%)</span></li>
                                    <li class="advisors">SUNRISE <span class="float-right text-muted">$17900 (23.44%)</span></li>
                                    <li class="power">POWER <span class="float-right text-muted">$18,328.2 (24.31%)</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-12" style="padding-left: 5px; padding-right: 5px;">
        <div class="card  profile-card-with-cover box_cust_css">
            <div class="card-content card-deck" style="margin-bottom: 0rem;">
                <div class="card box-shadow box_cust_css">
                    <div class="card-header card-header-transparent header_padding_css">
                        <div class="row">
                            <div class="col-8">
                                <h6 class="card-title card_title_css">BUY & HOLD</h6>
                            </div>
                            <div class="col-4 btn_padding">
                                <a href="<?= base_url() ?>customer/buyandhold" class="btn-secondary btn-gradient-css btn-sm btn_stye_in_btn">Buy/Hold</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-content" id="click_to_buy_hold">
                        <div id="buy_hold_pie" style="margin: 0; height: 225px;"></div>
                        <div class="card-body">
                            <div class="row mx-0">
                                <ul class="token-distribution-list col-md-12 mb-0"  style="font-size: 12px; color: #272727;">
                                    <li class="crowd-sale">BTC <span class="float-right text-muted">$1200 (21%)</span></li>
                                    <li class="team">ETC<span class="float-right text-muted">$7501.1 (12%)</span></li>
                                    <li class="advisors">LTC <span class="float-right text-muted">$17900 (35%)</span></li>
                                    <li class="power">XRP <span class="float-right text-muted">$3294 (20%)</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-12" style="padding-left: 5px; padding-right: 5px;">
        <div class="card  profile-card-with-cover">
            <div class="card-content card-deck" style="margin-bottom: 0rem;">
                <div class="card box-shadow box_cust_css">
                    <div class="card-header card-header-transparent header_padding_css">
                        <div class="row">
                            <div class="col-8">
                                <h6 class="card-title card_title_css">MY ITERA</h6>
                            </div>
                            <div class="col-4 btn_padding">
                                <a href="<?= base_url() ?>customer/myitera" class="btn-secondary btn-gradient-css btn-sm btn_stye_in_btn">My Itera</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-content" id="click_to_my_itera">
                        <div id="my_itera_pie" style="margin: 0; height: 225px;"></div>
                        <div class="card-body">
                            <div class="row mx-0">
                                <ul class="token-distribution-list col-md-12 mb-0"  style="font-size: 12px; color: #272727;">
                                    <li class="graph1">Available <span class="float-right text-muted">$1200 (20%)</span></li>
                                    <li class="graph2">Frozen Portfolios<span class="float-right text-muted">$7500 (15%)</span></li>
                                    <li class="graph3">Low Risk <span class="float-right text-muted">$17900 (32%)</span></li>
                                    <li class="graph4">High Risk <span class="float-right text-muted">$16500 (33%)</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-12" style="padding-left: 5px; padding-right: 5px;">
        <div class="card  profile-card-with-cover box_cust_css">
            <div class="card-content card-deck" style="margin-bottom: 0rem;">
                <div class="card box-shadow box_cust_css">
                    <div class="card-header card-header-transparent header_padding_css">
                        <div class="row">
                            <div class="col-8">
                                <h6 class="card-title card_title_css" >MY WALLET</h6>
                            </div>
                            <div class="col-4 btn_padding">
                                <a href="<?= base_url() ?>customer/wallet" class="btn-secondary btn-gradient-css btn-sm btn_stye_in_btn" style="padding: 7px 9px 7px 9px">My Wallet</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-content" id="click_to_my_wallet">
                        <div id="my_wallet_pie" style="margin: 0; height: 225px;"></div>
                        <div class="card-body">
                            <div class="row mx-0">
                                <ul class="token-distribution-list col-md-12 mb-0" style="font-size: 12px; color: #272727;">
                                    <li class="graph1">USD <span class="float-right text-muted">$1200 (11%)</span></li>
                                    <li class="graph2">EUR<span class="float-right text-muted">$7500 (39%)</span></li>
                                    <li class="graph3">BTC <span class="float-right text-muted">$17900 (25%)</span></li>
                                    <li class="graph4">ETH <span class="float-right text-muted">$4800 (25%)</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="set_new_section">

</div>
<?php
$msg = $this->input->get("msg");
switch ($msg) {
    case "S":
        $m = "Transaction was Successful...!!!";
        $t = "success";
        break;
    case "E":
        $m = "Transaction was Declined!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl_transaction").dataTable({
            "ordering": false,
        });
        var set_color = "#f7f7f7";
        my_portfoliyo_pie(set_color);
        mybuy_hold_pie(set_color);
        my_itera_pie(set_color);
        my_wallet_pie(set_color);
        $("#click_to_portfoliyo").on("click", function () {
            $("#click_to_portfoliyo").parent().removeClass('box_cust_css');
            $("#click_to_buy_hold").parent().addClass('box_cust_css');
            $("#click_to_my_itera").parent().addClass('box_cust_css');
            $("#click_to_my_wallet").parent().addClass('box_cust_css');
            var set_color = "#ffffff"
            var old_set_color = "#f7f7f7"
            my_portfoliyo_pie(set_color);
            mybuy_hold_pie(old_set_color);
            my_itera_pie(old_set_color);
            my_wallet_pie(old_set_color);
            $.ajax({
                url: "<?= base_url() ?>customer/dashboard/setPortfoliyoSection",
                type: "POST",
                data: {},
                success: function (data, textStatus, jqXHR) {
                    $('#set_new_section').html(data);
                }
            });
        });

        $("#click_to_buy_hold").on("click", function () {
            $("#click_to_buy_hold").parent().removeClass('box_cust_css');
            $("#click_to_portfoliyo").parent().addClass('box_cust_css');
            $("#click_to_my_itera").parent().addClass('box_cust_css');
            $("#click_to_my_wallet").parent().addClass('box_cust_css');
            var set_color = "#ffffff"
            var old_set_color = "#f7f7f7"
            my_portfoliyo_pie(old_set_color);
            mybuy_hold_pie(set_color);
            my_itera_pie(old_set_color);
            my_wallet_pie(old_set_color);
            $.ajax({
                url: "<?= base_url() ?>customer/dashboard/setBuyHoldSection",
                type: "POST",
                data: {},
                success: function (data, textStatus, jqXHR) {
                    $('#set_new_section').html(data);
                }
            });
        });

        $("#click_to_my_itera").on("click", function () {
            $("#click_to_my_itera").parent().removeClass('box_cust_css');
            $("#click_to_portfoliyo").parent().addClass('box_cust_css');
            $("#click_to_buy_hold").parent().addClass('box_cust_css');
            $("#click_to_my_wallet").parent().addClass('box_cust_css');
            var set_color = "#ffffff"
            var old_set_color = "#f7f7f7"
            my_portfoliyo_pie(old_set_color);
            mybuy_hold_pie(old_set_color);
            my_itera_pie(set_color);
            my_wallet_pie(old_set_color);

            $.ajax({
                url: "<?= base_url() ?>customer/dashboard/setMyIteraSection",
                type: "POST",
                data: {},
                success: function (data, textStatus, jqXHR) {
                    $('#set_new_section').html(data);
                }
            });
        });

        $("#click_to_my_wallet").on("click", function () {
            $("#click_to_my_wallet").parent().removeClass('box_cust_css');
            $("#click_to_portfoliyo").parent().addClass('box_cust_css');
            $("#click_to_buy_hold").parent().addClass('box_cust_css');
            $("#click_to_my_itera").parent().addClass('box_cust_css');
            var set_color = "#ffffff"
            var old_set_color = "#f7f7f7"
            my_portfoliyo_pie(old_set_color);
            mybuy_hold_pie(old_set_color);
            my_itera_pie(old_set_color);
            my_wallet_pie(set_color);
            $.ajax({
                url: "<?= base_url() ?>customer/dashboard/set_my_wallet_section",
                type: "POST",
                data: {},
                success: function (data, textStatus, jqXHR) {
                    $('#set_new_section').html(data);
                }
            });
        });
    });
</script>
<!-- ============================================================= Start my portfoliyo pie ==============================================================-->
<script type="text/javascript">
    function my_portfoliyo_pie(set_color) {
        Highcharts.chart('my_portfoliyo_pie', {
            credits: {
                enabled: false
            },
            chart: {
                marginTop: 20,
                marginBottom: 0,
                marginLeft: 0,
                marginRight: 0,
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0,
                backgroundColor: set_color
            },
            colors: [
                '#2372a8',
                '#3bc1e6',
                '#2c3384',
                '#c6fff8'
            ],
            exporting: {
                enabled: false
            },
            title: {
                text: '<b style="color:#407e6a">15.7%</b><br>$76367.3',
                align: 'center',
                verticalAlign: 'middle',
                y: 30
            },
            legend: {
                enabled: false,
                align: 'left',
                layout: 'vertical',
                verticalAlign: 'mididle',
                borderWidth: 0,
                margin: 0,
                labelFormatter: function () {
                    return '<b>' + this.name + ' - ' + this.y + '%</b><br/> $' + this.value + '';
                },
                itemMarginTop: 5,
                itemMarginBottom: 5,
                itemStyle: {
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSize: '12px'
                }
            },
            plotOptions: {
                stacking: 'percent',
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y:.1f}%'
                    }
                }
            },
            series: [{
                    marginTop: 0,
                    type: 'pie',
                    name: 'My Portfoliyo',
                    colorByPoint: true,
                    innerSize: '85%',
                    data: [15.7, 9.85, 23.44, 24.31],
                    dataLabels: {
                        enabled: false
                    }
                }]
        });
    }
</script>    
<!-- ============================================================= End my portfoliyo pie ==============================================================-->

<!-- ============================================================= Start buy_hold_pie pie ==============================================================-->
<script type="text/javascript">
    function mybuy_hold_pie(set_color) {
        Highcharts.chart('buy_hold_pie', {
            credits: {
                enabled: false
            },
            chart: {
                marginTop: 20,
                marginBottom: 0,
                marginLeft: 0,
                marginRight: 0,
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0,
                backgroundColor: set_color,
            },
            colors: [
                '#2372a8',
                '#3bc1e6',
                '#2c3384',
                '#c6fff8'
            ],
            exporting: {
                enabled: false
            },
            title: {
                text: '$16470',
                align: 'center',
                verticalAlign: 'middle',
                y: 30
            },
            legend: {
                enabled: false,
                align: 'left',
                layout: 'vertical',
                verticalAlign: 'mididle',
                borderWidth: 0,
                margin: 0,
                labelFormatter: function () {
                    return '<b>' + this.name + ' - ' + this.y + '%</b><br/> $' + this.value + '';
                },
                itemMarginTop: 5,
                itemMarginBottom: 5,
                itemStyle: {
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSize: '12px'
                }
            },
            plotOptions: {
                stacking: 'percent',
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y:.1f}%'
                    }
                }
            },
            series: [{
                    marginTop: 0,
                    type: 'pie',
                    name: 'Buy Hold',
                    colorByPoint: true,
                    innerSize: '85%',
                    data: [21, 12, 35, 20],
                    dataLabels: {
                        enabled: false
                    }
                }]
        });
    }

</script>    
<!-- ============================================================= End buy_hold_pie ==============================================================-->



<!-- ============================================================= Start my_itera_pie ==============================================================-->
<script type="text/javascript">
    function my_itera_pie(set_color) {
        Highcharts.chart('my_itera_pie', {
            credits: {
                enabled: false
            },
            chart: {
                marginTop: 20,
                marginBottom: 0,
                marginLeft: 0,
                marginRight: 0,
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0,
                backgroundColor: set_color,
            },
            colors: [
                '#092e38',
                '#1c8691',
                '#35ffb8',
                '#72b0b3'
            ],
            exporting: {
                enabled: false
            },
            title: {
                text: '50,000 ITERA <br> $5,000',
                align: 'center',
                verticalAlign: 'middle',
                y: 30
            },
            legend: {
                enabled: false,
                align: 'left',
                layout: 'vertical',
                verticalAlign: 'mididle',
                borderWidth: 0,
                margin: 0,
                labelFormatter: function () {
                    return '<b>' + this.name + ' - ' + this.y + '%</b><br/> $' + this.value + '';
                },
                itemMarginTop: 5,
                itemMarginBottom: 5,
                itemStyle: {
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSize: '12px'
                }
            },
            plotOptions: {
                stacking: 'percent',
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y:.1f}%'
                    }
                }
            },
            series: [{
                    marginTop: 0,
                    type: 'pie',
                    name: 'Buy Hold',
                    colorByPoint: true,
                    innerSize: '85%',
                    data: [20, 15, 32, 33],
                    dataLabels: {
                        enabled: false
                    }
                }]
        });
    }

</script>   
<!-- ============================================================= End my_itera_pie ==============================================================-->



<!-- ============================================================= Start my_wallet_pie ==============================================================-->
<script type="text/javascript">
    function my_wallet_pie(set_color) {
        Highcharts.chart('my_wallet_pie', {
            credits: {
                enabled: false
            },
            chart: {
                marginTop: 20,
                marginBottom: 0,
                marginLeft: 0,
                marginRight: 0,
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0,
                backgroundColor: set_color,
            },
            colors: [
                '#092e38',
                '#1c8691',
                '#35ffb8',
                '#72b0b3'
            ],
            exporting: {
                enabled: false
            },
            title: {
                text: '$19,200',
                align: 'center',
                verticalAlign: 'middle',
                y: 30
            },
            legend: {
                enabled: false,
                align: 'left',
                layout: 'vertical',
                verticalAlign: 'mididle',
                borderWidth: 0,
                margin: 0,
                labelFormatter: function () {
                    return '<b>' + this.name + ' - ' + this.y + '%</b><br/> $' + this.value + '';
                },
                itemMarginTop: 5,
                itemMarginBottom: 5,
                itemStyle: {
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSize: '12px'
                }
            },
            plotOptions: {
                stacking: 'percent',
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y:.1f}%'
                    }
                }
            },
            series: [{
                    marginTop: 0,
                    type: 'pie',
                    name: 'Buy Hold',
                    colorByPoint: true,
                    innerSize: '85%',
                    data: [11, 39, 25, 25],
                    dataLabels: {
                        enabled: false
                    }
                }]
        });
    }
</script>    
<!-- ============================================================= End my_wallet_pie ==============================================================-->