<style>
    .btn-gradient-secondary {
        background-image: linear-gradient(to right, #4CAF50 0%, #8BC34A 51%, #4CAF50 100%);
    }

    .social_icons li{
        margin: 5px;
    }

    .ct-labels .ct-label{
        -webkit-transform: rotate(-35deg);
        -moz-transform: rotate(-35deg);
        -ms-transform: rotate(-35deg);
        -o-transform: rotate(-35deg);
        transform: rotate(-35deg);
    }
    #my_portfoliyo{
        margin-top: 25px;   
    }
    #buy_hold{
        margin-top: 25px; 
    }
    #my_itera{
        margin-top: 25px;  
    }
    #my_wallet{
        margin-top: 25px; 
    }

    .ct-label{
        font-size: 10px;
    }
    .ct-label {
        fill: rgba(255, 255, 255);
        color: rgb(255, 255, 255);
        font-size: 0.75rem;
        line-height: 1;
    }
    /* START: Custom css */
    ul.token-distribution-list li.power::before {
        background: #ec7c68;
    }

    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px; 
        max-width: 500px;
        margin: 1em auto;
    }

    #container {
        height: 400px;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    ul.token-distribution-list li.crowd-sale::before {
        background: #2372a8;
    }
    ul.token-distribution-list li.team::before {
        background: #3bc1e6;
    }
    ul.token-distribution-list li.advisors::before {
        background: #2c3384;
    }
    ul.token-distribution-list li.power::before {
        background: #c6fff8;
    }


    ul.token-distribution-list li.graph1::before {
        background: #092e38;
    }
    ul.token-distribution-list li.graph2::before {
        background: #1c8691;
    }
    ul.token-distribution-list li.graph3::before {
        background: #35ffb8;
    }

    ul.token-distribution-list li.graph4::before {
        background: #72b0b3;
    }
    .header_padding_css{
        padding: 14px 15px 14px 15px;
    }
    .card_title_css{
        padding-top: 5px  
    }
    .btn_padding{
        padding: 0px 14px 0px 0px;
        text-align: right;
    }
    .btn_stye_in_btn{
        padding: 7px 11px 7px 11px;
        width: auto;
    }
    .nav.nav-tabs .nav-item .nav-link.active {
        background-color: #f1f1f1 !important;
    }
    .nav-tabs .nav-link {
        padding: 0.4rem 2.7rem !important;
        border-top-left-radius: 0rem;
        border-top-right-radius: 0.75rem;
    }

    .nav-tabs .nav-item {
        margin-bottom: 0px !important;
    }

    .nav.nav-tabs .nav-item .nav-link.active {
        background-color: #f1f1f1 !important;
        border-radius: 0rem 0.75rem 0 0 !important;
    }
    .progress {
        height: 2.75rem;
    }
     .progress-bar {
       background-image: linear-gradient(to right, #3e4a49 , #29b4ac);
    }
    /* END: Custom css */
</style>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">PORTFOLIO : <?= isset($portfolio) ? $portfolio->name : "" ?></h4>
            </div>
        </div>   
        <ul class="nav nav-tabs" style="background-color: #e8e8e8">
            <li class="nav-item">
                <a class="nav-link active" id="base-tab1" style="color: #000; background-color: #d8d8d8;" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">INVEST NOW</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab2" style="color: #000; background-color: #d8d8d8; margin-left: 5px; margin-right: 3px;" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">ASSETS & DIRECTION</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab3" style="color: #000; background-color: #d8d8d8; margin-left: 3px; margin-right: 3px;" data-toggle="tab" aria-controls="tab3" href="#tab3" aria-expanded="false">REVIEWS</a>
            </li>
        </ul>
        <div class="card">

            <div class="card-body" style="padding-left: 0px; padding-top: 10px;">

                <div class="tab-content px-1 pt-1" style="padding-top: 10px !important;">
                    <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                        <div class="row" style="margin-left:1px; margin-right: 3px;">
                            <div class="col-md-4" style="padding-left: 0px; padding-right: 5px;">
                                <div class="form-body" style="padding-top: 10px;">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <div class="card-header-transparent">
                                                <h2 class="downlink-head">
                                                    <img src="<?= base_url() ?>uploads/portfolio_image/<?= isset($portfolio) ? $portfolio->portfolio_image : "" ?>" width="150px" height="150px"> 
                                                    <span><?= isset($portfolio) ? $portfolio->name : "" ?></span>
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-12">
                                            <div class="row">
                                                <div class="col-md-5 col-4">
                                                    <ul class="p-0" style="    margin-left: 20px;">
                                                        <li><b>Base</b></li>
                                                        <li><b>Risk Level</b></li>
                                                        <li><b>Manager</b></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6 col-6">
                                                    <ul class="p-0">
                                                        <li><?= isset($portfolio) ? $portfolio->symbol : "" ?></li>
                                                        <li><?= isset($portfolio) ? $portfolio->risk_level : "" ?></li>
                                                        <li><?= isset($portfolio) ? $portfolio->manager : "" ?></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-left: 5px; padding-right: 0px;">

                                <div class="form-body" style="padding-top: 10px;">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <fieldset class="form-group">
                                                <label for="postal-code">Base</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="" value="3" name="amount">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><?= isset($portfolio) ? $portfolio->symbol : "" ?></span>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-3">
                                            <fieldset class="form-group">
                                                <label for="postal-code">Available</label>
                                                <input type="text" class="form-control" placeholder="" value="<?= isset($portfolio) ? $portfolio->amount_invest : "" ?>" aria-label="" name="amount">
                                            </fieldset>
                                        </div>
                                        <div class="col-md-3">
                                            <fieldset class="form-group">
                                                <label for="postal-code">Invest</label>
                                                <input type="text" class="form-control" placeholder="" aria-label="" name="amount">
                                            </fieldset>
                                        </div>
                                        <div class="col-md-3">
                                            <fieldset class="form-group">
                                                <label for="postal-code">Limit </label>
                                                <input type="text" class="form-control" placeholder="" aria-label="" name="amount">
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <fieldset class="form-group">
                                                <label for="postal-code">Risk Level</label>
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="41"
                                                         aria-valuemin="0" aria-valuemax="100" style="width:41%;">
                                                        <span class="sr-only">41% Complete</span>
                                                    </div>
                                                    <span class="btn-gradient-css" style="width: 30px; border: 1px solid #dadada; color: #dadada !important;"></span>
                                                    <span style="font-size: 18px; margin-left: 5px; margin-top: 7px;"> 41%</span>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-4">
                                            <fieldset class="form-group">
                                                <label for="postal-code">Profit Target</label><label style="float: right; padding-right: 2px;"><span id="errorpostal_code" style="color:red; font-weight: 500;"></span></label>
                                                <input type="text" class="form-control" placeholder="" value="237.52" name="amount">
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="submit" class="btn-secondary btn-gradient-css btn-sm my-1" id="btn_register" style="margin-right: 10px; ">Invest</button>
                                        <button type="reset" class="btn-secondary btn-gradient-css btn-sm my-1">Cancel</button>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                                <div class="form-body" style="padding-top: 10px;">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <div class="card-header-transparent">
                                                <h6>
                                                    <p class="rating-num m-0">4.0</p>
                                                    <div class="rating">
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    </div>
                                                    <div>
                                                        <span>1,256 reviews</span>
                                                    </div>
                                                </h6>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div> 
                    </div>
                    <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                        <div class="row" style="margin-left:1px; margin-right: 3px;">
                            <div class="col-md-12 table-responsive pl-0 pr-0">
                                <table class="table table-bordered table-striped text-left"  id="tbl_transaction">
                                    <thead>
                                        <tr>
                                            <th>Coin</th> 
                                            <th>Value</th>             
                                            <th>Share</th>
                                            <th>Trade</th>
                                            <th>Direction</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td>
                                                <p class="m-0">ETH</p>
                                                <small>Ethereum</small>
                                            </td>
                                            <td class="text-right">$ 220.17</td>                            
                                            <td class="text-right">60%</td>
                                            <td>Short</td>
                                            <td class="text-right">-4.6%</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p class="m-0">BTC</p>
                                                <small>Bitcoin</small>
                                            </td>
                                            <td class="text-right">$ 9455.17</td>                            
                                            <td class="text-right">20%</td>
                                            <td>Long</td>
                                            <td class="text-right">2.5%</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p class="m-0">ETH</p>
                                                <small>Ethereum</small>
                                            </td>
                                            <td class="text-right">$ 220.17</td>                            
                                            <td class="text-right">5%</td>
                                            <td>Long</td>
                                            <td class="text-right">20%</td>
                                        </tr>



                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>
                    <div class="tab-pane" id="tab3" aria-labelledby="base-tab3">
                        <div class="row" style="margin-left:1px; margin-right: 3px;">

                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl_transaction").dataTable({
            "ordering": true
        });
    });
</script>


