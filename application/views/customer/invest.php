<style>
    .btn-gradient-secondary {
        background-image: linear-gradient(to right, #4CAF50 0%, #8BC34A 51%, #4CAF50 100%);
    }

    .social_icons li{
        margin: 5px;
    }

    .ct-labels .ct-label{
        -webkit-transform: rotate(-35deg);
        -moz-transform: rotate(-35deg);
        -ms-transform: rotate(-35deg);
        -o-transform: rotate(-35deg);
        transform: rotate(-35deg);
    }
    #my_portfoliyo{
        margin-top: 25px;   
    }
    #buy_hold{
        margin-top: 25px; 
    }
    #my_itera{
        margin-top: 25px;  
    }
    #my_wallet{
        margin-top: 25px; 
    }

    .ct-label{
        font-size: 10px;
    }
    .ct-label {
        fill: rgba(255, 255, 255);
        color: rgb(255, 255, 255);
        font-size: 0.75rem;
        line-height: 1;
    }
    /* START: Custom css */
    ul.token-distribution-list li.power::before {
        background: #ec7c68;
    }

    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px; 
        max-width: 500px;
        margin: 1em auto;
    }

    #container {
        height: 400px;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    ul.token-distribution-list li.crowd-sale::before {
        background: #2372a8;
    }
    ul.token-distribution-list li.team::before {
        background: #3bc1e6;
    }
    ul.token-distribution-list li.advisors::before {
        background: #2c3384;
    }
    ul.token-distribution-list li.power::before {
        background: #c6fff8;
    }


    ul.token-distribution-list li.graph1::before {
        background: #092e38;
    }
    ul.token-distribution-list li.graph2::before {
        background: #1c8691;
    }
    ul.token-distribution-list li.graph3::before {
        background: #35ffb8;
    }

    ul.token-distribution-list li.graph4::before {
        background: #72b0b3;
    }
    .header_padding_css{
        padding: 14px 15px 14px 15px;
    }
    .card_title_css{
        padding-top: 5px  
    }
    .btn_padding{
        padding: 0px 14px 0px 0px;
        text-align: right;
    }
    .btn_stye_in_btn{
        padding: 7px 11px 7px 11px;
        width: auto;
    }
    .nav.nav-tabs .nav-item .nav-link.active {
        background-color: #f1f1f1 !important;
    }
    .nav-tabs .nav-link {
        padding: 0.4rem 2.7rem !important;
        border-top-left-radius: 0rem;
        border-top-right-radius: 0.75rem;
    }

    .nav-tabs .nav-item {
        margin-bottom: 0px !important;
    }

    .nav.nav-tabs .nav-item .nav-link.active {
        background-color: #f1f1f1 !important;
        border-radius: 0rem 0.75rem 0 0 !important;
    }

    .nav-tabs {
        border-bottom: 10px solid #f0f0f0;
    }

    .nav-tabs .nav-link.active {
        border-color: #BABFC7 #BABFC7 #f0f0f0;
    }
    .progress {
        height: 2.75rem;
    }
    .progress-bar {
       background-image: linear-gradient(to right, #3e4a49 , #29b4ac);
    }
    /* END: Custom css */
</style>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <ul class="nav nav-tabs" style="background-color: #e8e8e8">
            <li class="nav-item">
                <a class="nav-link active" id="base-tab1" style="color: #000; background-color: #d8d8d8;" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">EASY</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab2" style="color: #000; background-color: #d8d8d8; margin-left: 5px; margin-right: 3px;" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">ADVANCED</a>
            </li>
        </ul>
        <div class="tab-content px-1 pt-1" style="padding-top: 10px !important;">
            <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                <div class="row">
                    <div class="col-md-6" style="padding-left: 0px; padding-right: 5px;">
                        <div class="form-box">
                            <div class="form-heading">
                                <h4 class="h3 text-capitalize">All Portfolios</h4>
                            </div>
                            <div class="form-body" style="padding-top: 10px;">
                                <div class="row">
                                    <div class="col-md-3">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Base</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="" aria-label="" name="amount">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">USD</span>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-3">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Available</label>
                                            <input type="text" class="form-control" placeholder="" value="1000" name="amount">
                                        </fieldset>
                                    </div>
                                    <div class="col-md-3">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Invest</label>
                                            <input type="text" class="form-control" placeholder="" value="1000" name="amount">
                                        </fieldset>
                                    </div>
                                    <div class="col-md-3">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Max Period</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="" value="3" name="amount">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">days</span>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Risk Level</label>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="74"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:74%;">
                                                    <span class="sr-only">74% Complete</span>
                                                </div>
                                                <span class="btn-gradient-css" style="width: 30px; border: 1px solid #dadada; color: #dadada !important;"></span>
                                                <span style="font-size: 18px; margin-left: 5px; margin-top: 7px;"> 74%</span>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Profit Target</label><label style="float: right; padding-right: 2px;"><span id="errorpostal_code" style="color:red; font-weight: 500;"></span></label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="" value="237.52" name="amount">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">USD</span>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 82px"> 
                                    <div class="col-md-12">
                                        <p>The invested amount is split in ten diferent portfolios for diversification</p>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn-secondary btn-gradient-css btn-sm my-1" id="btn_register" style="margin-right: 10px; ">Invest</button>
                                    <button type="reset" class="btn-secondary btn-gradient-css btn-sm my-1">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-left: 5px; padding-right: 0px;">
                        <div class="form-box">
                            <div class="form-heading">
                                <h4 class="h3 text-capitalize">ITERA Portfolios</h4>
                            </div>
                            <div class="form-body" style="padding-top: 10px;">
                                <div class="row">
                                    <div class="col-md-4">
                                        <fieldset class="form-group">
                                            <label for="postal-code">ITERA Available</label>
                                            <input type="text" class="form-control" placeholder="" aria-label="" name="amount">
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Amount to Invest</label>
                                            <input type="text" class="form-control" placeholder="" aria-label="" name="amount">
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Max Period</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="" value="30" name="amount">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">days</span>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Risk Level</label>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="41"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:41%;">
                                                    <span class="sr-only">41% Complete</span>
                                                </div>
                                               <span class="btn-gradient-css" style="width: 30px; border: 1px solid #dadada; color: #dadada !important;"></span>
                                                <span style="font-size: 18px; margin-left: 5px; margin-top: 7px;"> 41%</span>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Profit Target</label><label style="float: right; padding-right: 2px;"><span id="errorpostal_code" style="color:red; font-weight: 500;"></span></label>
                                            <input type="text" class="form-control" placeholder="" value="237.52" name="amount">
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Portfolios</label>
                                            <input type="text" class="form-control" placeholder="" value="32100" name="amount">
                                        </fieldset>
                                    </div>
                                    <div class="col-md-3">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Low Risk</label>
                                            <input type="text" class="form-control" placeholder="" value="25420" name="amount">
                                        </fieldset>
                                    </div>
                                    <div class="col-md-3">
                                        <fieldset class="form-group">
                                            <label for="postal-code">High Risk</label>
                                            <input type="text" class="form-control" placeholder="" value="24360" name="amount">
                                        </fieldset>
                                    </div>
                                    <div class="col-md-3">
                                        <fieldset class="form-group">
                                            <label for="postal-code">Rental</label>
                                            <input type="text" class="form-control" placeholder="" value="8120" name="amount">
                                        </fieldset>
                                    </div>
                                </div>  
                                <div class="row" > 
                                    <div class="col-md-12">
                                        <p>The invested amount is split in ten diferent portfolios for diversification</p>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn-secondary btn-gradient-css btn-sm my-1" id="btn_register" style="margin-right: 10px; ">Invest</button>
                                    <button type="reset" class="btn-secondary btn-gradient-css btn-sm my-1">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                <div class="row">
                    <div class="col-lg-12 custom-padding-column" style="padding-left: 0px; padding-right:0px;">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">PORTFOLIOs</h4>
                            </div>
                            <div class="card-content">
                                <div class="row" style="margin-left:0px; margin-right: 0px;">
                                    <div class="col-md-12 table-responsive pl-0 pr-0">
                                        <table class="table table-bordered table-striped text-left" id="tbl_transaction">
                                            <thead>
                                                <tr>
                                                    <th>Name/Manager</th> 
                                                    <th>Base</th>
                                                    <th>Time Frame</th>
                                                    <th>Available</th>
                                                    <th>Max Profit</th>
                                                    <th>Ratings</th>
                                                    <th>Favourite</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (isset($portfolio) && !empty($portfolio)) {
                                                    foreach ($portfolio as $val) {
                                                        $date1 = $val->start_date;
                                                        $date2 = $val->end_date;
                                                        $diff = abs(strtotime($date2) - strtotime($date1));
                                                        $years = floor($diff / (365 * 60 * 60 * 24));
                                                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                                        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                                                        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <div class="name-tddata">
                                                                    <img src="<?= base_url() ?>uploads/portfolio_image/<?= $val->portfolio_image ?>" width="40px" height="40px"> 
                                                                    <span><?= strtoupper($val->name); ?></span>
                                                                    <small><?= $val->manager ?></small>
                                                                </div>
                                                            </td>
                                                            <td ><?= $val->currency_name; ?></td>
                                                            <td class="text-right"><?= $days . "d, " . $hours . "h, " . $minutes . "m" ?></td>
                                                            <td class="text-right">$<?= $val->amount_invest; ?></td>
                                                            <td class="text-right">
                                                                <small class="text-success text-bold-700">97.3%</small>
                                                            </td>
                                                            <td>
                                                                <p class="rating-num m-0">4.0</p>
                                                                <div class="rating">
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="fa fa-star-o"></span>
                                                                </div>
                                                                <div>
                                                                    <span>1,256 reviews</span>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <span class="fa fa-star-o" style="font-size: 24px;"></span>  <a href="<?= base_url() ?>customer/portfolios/portfolios_view/<?= $val->portfolio_id; ?>"><i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>                                    
                                </div>         
                            </div>
                        </div>
                    </div>
                </div> 
            </div>

        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl_transaction").dataTable({
            "ordering": true,
        });
    });
</script>

