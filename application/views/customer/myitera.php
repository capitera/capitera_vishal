<style>
    *:not(.gem-table):not(.cart_totals) > table:not(.shop_table):not(.group_table):not(.variations) td {
        border-top: 0px solid #dfe5e8;
        border-bottom: 1px solid #dfe5e8;
        /* background-color: rgb(247, 247, 247); */
        font-weight: 500;
        font-size: 15px;
        color: #1c1c1c;
    }
</style>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header" style="margin-bottom: 0px;">
                <h4 class="card-title">MY ITERA</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content">
                <!--<div class="row ml-1 mr-1 my-itera">
                    <div class="col-md-3">
                        <div class="card ">
                            <div class="card-header">
                                <h6>AVAILABLE</h6>
                                <p class="percent-background">52%</p>
                            </div>
                            <div class="card-body text-center">
                                <h1 class="text-center pt-5 pb-2">
                <?php
                $CustTotalFanscoin = $this->common->getCustTotalFanscoin($this->session->userdata("cid"))
                ?>
                <?= ($CustTotalFanscoin) ? number_format($CustTotalFanscoin, 2) : 0 ?>
                                </h1>
                                <div class="row mb-1">
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">Value</p>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">$ 720</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-12">
                                        <a href="<?= base_url() ?>customer/buytoken" class="btn-sm btn btn-outline-primary mb-1">BUY</a>
                                        <a href="<?= base_url() ?>customer/issuetoken" class="btn-sm btn btn-outline-secondary mb-1">SEND</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card ">
                            <div class="card-header">
                                <h6>FROZEN</h6>
                                <p class="percent-background">72%</p>
                            </div>
                            <div class="card-body text-center">
                                <h1 class="text-center pt-5 pb-2"><?= ($frozenCoins) ? number_format($frozenCoins, 2) : '0' ?> </h1>
                                <div class="row mb-1">
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">Value</p>
                                        <p class="font-weight-bold">Current Profit</p>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">$ 750</p>
                                        <p class="font-weight-bold">14.5%</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-12">
                                        <a href="#" class="btn-sm btn btn-outline-primary mb-1">ADD</a>
                                        <a href="#" class="btn-sm btn btn-outline-danger mb-1">WITHDRAW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card ">
                            <div class="card-header">
                                <h6>LIQUIDITY POOL, LOW RISK</h6>
                                <p class="percent-background">15%</p>
                            </div>
                            <div class="card-body text-center">
                                <h1 class="text-center pt-5 pb-2">11,000</h1>
                                <div class="row mb-1">
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">Value</p>
                                        <p class="font-weight-bold">Current Profit</p>
                                        <p class="font-weight-bold">Return time</p>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">$ 720</p>
                                        <p class="font-weight-bold">5%</p>
                                        <p class="font-weight-bold">3 days</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-12">
                                        <a href="#" class="btn-sm btn btn-outline-primary mb-1">ADD</a>
                                        <a href="#" class="btn-sm btn btn-outline-danger mb-1">WITHDRAW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card ">
                            <div class="card-header">
                                <h6>LIQUIDITY POOL, height RISK</h6>
                                <p class="percent-background">52%</p>
                            </div>
                            <div class="card-body text-center">
                                <h1 class="text-center pt-5 pb-2">4500</h1>
                                <div class="row mb-1">
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">Value</p>
                                        <p class="font-weight-bold">Current Profit</p>
                                        <p class="font-weight-bold">Return time</p>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">$ 450</p>
                                        <p class="font-weight-bold">30%</p>
                                        <p class="font-weight-bold">22 h</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-12">
                                        <a href="#" class="btn-sm btn btn-outline-primary mb-1">ADD</a>
                                        <a href="#" class="btn-sm btn btn-outline-secondary mb-1">WITHDRAW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row ml-1 mr-1 my-itera">
                    <div class="col-md-3">
                        <div class="card ">
                            <div class="card-header">
                                <h6>RENTED</h6>
                                <p class="percent-background">53%</p>
                            </div>
                            <div class="card-body text-center">
                                <h1 class="text-center pt-5 pb-2">4500</h1>
                                <div class="row mb-1">
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">Value</p>
                                    </div>
                                    <div class="col-md-6 col-6">
                                        <p class="font-weight-bold">$ 720</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-12">
                                        <a href="#" class="btn-sm btn btn-outline-primary mb-1">ADD</a>
                                        <a href="#" class="btn-sm btn btn-outline-danger mb-1">WITHDRAW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card ">
                            <div class="card-header">
                                <h6>BUY BACK</h6>
                                <p class="percent-background">48%</p>
                            </div>
                            <div class="card-body text-center">
                                <h1 class="text-center pt-5 pb-2">6600</h1>
                <!-- <div class="row mb-1">
                    <div class="col-md-6 col-6">
                        <p class="font-weight-bold">Value</p>
                        <p class="font-weight-bold">Current Profit</p>
                    </div>
                    <div class="col-md-6 col-6">
                        <p class="font-weight-bold">$ 750</p>
                        <p class="font-weight-bold">14.5%</p>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-md-12 col-12">
                        <a href="#" class="btn-sm btn btn-outline-primary mb-1">ADD</a>
                        <a href="#" class="btn-sm btn btn-outline-danger mb-1">WITHDRAW</a>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card ">
            <div class="card-header">
                <h6>LIQUIDITY POOL, LOW RISK</h6>
                <p class="percent-background">22%</p>
            </div>
            <div class="card-body text-center">
                <h1 class="text-center pt-5 pb-2">11,000</h1>
                <div class="row mb-1">
                    <div class="col-md-6 col-6">
                        <p class="font-weight-bold">Value</p>
                        <p class="font-weight-bold">Current Profit</p>
                        <p class="font-weight-bold">Return time</p>
                    </div>
                    <div class="col-md-6 col-6">
                        <p class="font-weight-bold">$ 720</p>
                        <p class="font-weight-bold">5%</p>
                        <p class="font-weight-bold">3 days</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-12">
                        <a href="#" class="btn-sm btn btn-outline-primary mb-1">ADD</a>
                        <a href="#" class="btn-sm btn btn-outline-danger mb-1">WITHDRAW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card ">
            <div class="card-header">
                <h6>LIQUIDITY POOL, height RISK</h6>
                <p class="percent-background">9%</p>
            </div>
            <div class="card-body text-center">
                <h1 class="text-center pt-5 pb-2">4500</h1>
                <div class="row mb-1">
                    <div class="col-md-6 col-6">
                        <p class="font-weight-bold">Value</p>
                        <p class="font-weight-bold">Current Profit</p>
                        <p class="font-weight-bold">Return time</p>
                    </div>
                    <div class="col-md-6 col-6">
                        <p class="font-weight-bold">$ 450</p>
                        <p class="font-weight-bold">30%</p>
                        <p class="font-weight-bold">22 h</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-12">
                        <a href="<?= base_url() ?>customer/buytoken" class="btn-sm btn btn-outline-primary mb-1">BUY</a>
                        <a href="<?= base_url() ?>customer/issuetoken" class="btn-sm btn btn-outline-secondary mb-1">SEND</a>
                    </div>
                <!--                            <div class="col-md-12 col-12">
                                                <a href="#" class="btn-sm btn btn-outline-danger mb-1">SELL</a>
                                                <a href="#" class="btn-sm btn btn-outline-success mb-1">DEPOSIT</a>
                                            </div>-->
                <!--                                </div>
                                            </div>
                                        </div>
                                    </div>-->
            </div>

            <div class="row" style="margin-left:0px; margin-right: 0px;">
                <div class="col-md-12 table-responsive pl-0 pr-0">
                    <table class="table table-bordered table-striped text-left" id="tbl_transaction">
                        <thead>
                            <tr>
                                <th>Status</th> 
                                <th>Amount</th>             
                                <th>Percent</th>
                                <th>Value, $</th>
                                <th>Profit %</th>
                                <th>Action</th>                                         
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><i class="icon icon-like"></i>&nbsp; AVAILABLE</td>
                                <td class="text-right"> <?php
                                    $CustTotalFanscoin = $this->common->getCustTotalFanscoin($this->session->userdata("cid"))
                                    ?>
                                    <?= ($CustTotalFanscoin) ? number_format($CustTotalFanscoin, 2) : 0 ?></td>                            
                                <td class="text-right">32%</td>
                                <td class="text-right">$3000</td>
                                <td class="text-right">n/a</td>
                                <td> 
                                    <a href="<?= base_url() ?>customer/buytoken" class="btn-secondary btn-gradient-css btn-sm" style="text-align: center; margin-right: 5px; padding: 8px 7px 8px 7px;">Buy</a>
                                    <a href="" class="btn-secondary btn-gradient-css btn-sm" style="text-align: center; margin-right: 5px; padding: 8px 7px 8px 7px;">Sell</a>
                                    <a href="<?= base_url() ?>customer/issuetoken" class="btn-secondary btn-gradient-css btn-sm" style="text-align: center; margin-right: 5px; padding: 8px 7px 8px 7px;">Send</a>
                                    <a href="" class="btn-secondary btn-gradient-css btn-sm" style="text-align: center; padding: 8px 7px 8px 7px;">Deposit</a>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="icon icon-lock"></i>&nbsp; FROZEN</td>
                                <td class="text-right"><?= ($frozenCoins) ? number_format($frozenCoins, 2) : '0' ?> </td>                            
                                <td class="text-right">17%</td>
                                <td class="text-right">$2250</td>
                                <td class="text-right">n/a</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><i class="icon icon-support"></i>&nbsp; LIQUIDITY POOL, LOW RISK</td>
                                <td class="text-right">16000</td>
                                <td class="text-right">22%</td>
                                <td class="text-right">$4800</td>
                                <td class="text-right">5%</td>
                                <td> 
                                    <a href="" class="btn-secondary btn-gradient-css btn-sm" style="text-align: center; margin-right: 5px; padding: 8px 7px 8px 7px;">
                                        Add
                                    </a>
                                    <a href="" class="btn-secondary btn-gradient-css btn-sm" style="text-align: center; padding: 8px 7px 8px 7px;">
                                        Withdraw
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="icon icon-support"></i>&nbsp; LIQUIDITY POOL, HIGH RISK</td>
                                <td class="text-right">7500</td>
                                <td class="text-right">17%</td>
                                <td class="text-right">$2200</td>
                                <td class="text-right">n/a</td>
                                <td> 
                                    <a href="" class="btn-secondary btn-gradient-css btn-sm" style="text-align: center; margin-right: 5px; padding: 8px 7px 8px 7px;">
                                        Add
                                    </a>
                                    <a href="" class="btn-secondary btn-gradient-css btn-sm" style="text-align: center; padding: 8px 7px 8px 7px;">
                                        Withdraw
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="icon icon-lock"></i> RENTED</td>
                                <td class="text-right">16000</td>
                                <td class="text-right">22%</td>
                                <td class="text-right">$4800</td>
                                <td class="text-right">5%</td>
                                <td> 

                                </td>
                            </tr>
                            <tr>
                                <td><i class="icon icon-support"></i> BUY BACK</td>
                                <td class="text-right">7500</td>
                                <td class="text-right">17%</td>
                                <td class="text-right">$2500</td>
                                <td class="text-right">n/a</td>
                                <td> 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>                                    
            </div>         
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl_transaction").dataTable({
             "ordering": true,
        });
        var max_height = 0;
        $(".my-itera .card").each(function (key, val) {
            var block_height = $(val).height();
            if (block_height >= max_height)
            {
                max_height = block_height;
            }
        });
        $(".my-itera .card").css("height", max_height);
    });
</script>