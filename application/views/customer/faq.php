<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header col-md-8 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">FAQ</h3>
            <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>customer/dashboard">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active"><a href="<?= base_url() ?>customer/faq">FAQ</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-12  order-2">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade active show" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <div id="accordion" class="collapse-icon accordion-icon-rotate left">
                    <div class="card">
                        <div class="card-header" id="heading11">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11" href="#">What is an ICO?</a>
                        </div>
                        <div id="collapse11" class="collapse show" aria-labelledby="heading11" data-parent="#accordion">
                            <div class="card-body">
                                ICO - initial coin offering is a strategic model to raise the funds, for a project based on the Blockchain through a token sale.
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card">
                        <div class="card-header" id="heading12">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12" href="#">When is the start of the ICO and what is launch date?</a>
                        </div>
                        <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
                            <div class="card-body">
                                The ICO launches on March 1 2018 at 12:00 AM CST it will end April 15 2018 at 12:00 PM CST.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading13">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13" href="#">Where can i learn more about the Project?</a>
                        </div>
                        <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordion">
                            <div class="card-body">
                                To learn more about project, you can read the provided prospectus or email us directly on at

                                To learn more about ERA in general, please watch the video on
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading14">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14" href="#">How to deposit?</a>
                        </div>
                        <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordion">
                            <div class="card-body">
                                Initially, the Pre-sale ICO begins on 3rd December 2017. The purchase of will be available on the in ICO dashboard using payment methods: BTC, ETH, LTC and Credit / Debit Card
                            </div>
                        </div>
                    </div> -->
                    <div class="card">
                        <div class="card-header" id="heading15">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15" href="#">What is blockchain?</a>
                        </div>
                        <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordion">
                            <div class="card-body">
                                Blockchain is a distributed ledger that contains the history of every transaction recorded in blocks, linked by cryptography. Each block contains a cryptographic hash of the previous block and timestamp and transaction data. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading16">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16" href="#">What is Bitcoin?</a>
                        </div>
                        <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#accordion">
                            <div class="card-body">
                                Bitcoin is the first blockchain asset, a decentralized digital currency without a central bank or a single administrator that can be sent from one person to another without the need for intermediaries. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading17">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17" href="#">What is Ethereum?</a>
                        </div>
                        <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#accordion">
                            <div class="card-body">
                                Ethereum is an open source, decentralized blockchain platform, not owned or operated by any one person or business, featuring smart contract functionality. Ether (ETH) is the native payment currency of the Ethereum network.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading18">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse18" aria-expanded="false" aria-controls="collapse18" href="#">What is a wallet?</a>
                        </div>
                        <div id="collapse18" class="collapse" aria-labelledby="heading18" data-parent="#accordion">
                            <div class="card-body">
                                A blockchain wallet is used to store money or digital assets using private keys.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading19">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse19" aria-expanded="false" aria-controls="collapse19" href="#">What is an exchange?</a>
                        </div>
                        <div id="collapse19" class="collapse" aria-labelledby="heading19" data-parent="#accordion">
                            <div class="card-body">
                                An exchange is a system where users buy and sell crypto assets at the current prices based on supply and demand. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading120">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse120" aria-expanded="false" aria-controls="collapse120" href="#">What is a White Paper?</a>
                        </div>
                        <div id="collapse120" class="collapse" aria-labelledby="heading120" data-parent="#accordion">
                            <div class="card-body">
                                A White Paper is a document that offers a description for the project, vision, concept, use cases, competitive advantages, team and roadmap.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading121">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse121" aria-expanded="false" aria-controls="collapse121" href="#">What are utility tokens?</a>
                        </div>
                        <div id="collapse121" class="collapse" aria-labelledby="heading121" data-parent="#accordion">
                            <div class="card-body">
                                Tokens are a digital asset sold through an ICO campaign. The utility token is an app token that is required for the products or services offer by the app or platform.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading122">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse122" aria-expanded="false" aria-controls="collapse122" href="#">What is KYC?</a>
                        </div>
                        <div id="collapse122" class="collapse" aria-labelledby="heading122" data-parent="#accordion">
                            <div class="card-body">
                                KYC - Know Your Customer is a required process involving documents for proving the identity and address of an user.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-ico" role="tabpanel" aria-labelledby="v-pills-ico-tab">
                <div id="accordion5" class="collapse-icon accordion-icon-rotate left">
                    <div class="card">
                        <div class="card-header" id="heading51">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse51" aria-expanded="false" aria-controls="collapse51" href="#">What is Capitera platform?</a>
                        </div>
                        <div id="collapse51" class="collapse show" aria-labelledby="heading51" data-parent="#accordion5">
                            <div class="card-body">
                                Capitera platform is an investment platform for managing crypto portfolios with various risk levels.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading52">
                            <a class="card-title lead" data-toggle="collapse" data-target="#collapse52" aria-expanded="false" aria-controls="collapse52" href="#">Who are the users of Capitera?</a>
                        </div>
                        <div id="collapse52" class="collapse" aria-labelledby="heading52" data-parent="#accordion5">
                            <div class="card-body">
                                The users for Capitera platform are investors who want to grow their crypto assets.  Capitera offers it's platform to both individual investors and institutions.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading53">
                            <a class="card-title lead" data-toggle="collapse" data-target="#collapse53" aria-expanded="false" aria-controls="collapse53" href="#">What is a portfolio on Capitera Platform?</a>
                        </div>
                        <div id="collapse53" class="collapse" aria-labelledby="heading53" data-parent="#accordion5">
                            <div class="card-body">
                                A portfolio is a group of crypto assets that have a profit target, a stop loss and a time limit. Capitera offers portfolios for both short and long term investments.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading54">
                            <a class="card-title lead" data-toggle="collapse" data-target="#collapse54" aria-expanded="false" aria-controls="collapse54" href="#">How long  will Capitera ICO last?</a>
                        </div>
                        <div id="collapse54" class="collapse" aria-labelledby="heading54" data-parent="#accordion5">
                            <div class="card-body">
                                The public sale for Capitera ICO will last one year
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading55">
                            <a class="card-title lead" data-toggle="collapse" data-target="#collapse55" aria-expanded="false" aria-controls="collapse55" href="#">What are the use cases for Capitera token?</a>
                        </div>
                        <div id="collapse55" class="collapse" aria-labelledby="heading55" data-parent="#accordion5">
                            <div class="card-body">
                                CAPITERA tokens will be required for managing portfolios with near zero risk one to one - if a user wants to manage a portfolio of 100.000 USD will need to have 100,000 CAPITERA tokens. Users will be able to rent their tokens to other users, will be able to participate in a liquidity pool, participate in a buyback program, benefit from reduced fees, invest CAPTERA tokens in different portfolios, send and receive CAPITERA tokens to other users, cash out using CAPITERA card.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading56">
                            <a class="card-title lead" data-toggle="collapse" data-target="#collapse56" aria-expanded="false" aria-controls="collapse56" href="#">Is there a soft cap?</a>
                        </div>
                        <div id="collapse56" class="collapse" aria-labelledby="heading56" data-parent="#accordion5">
                            <div class="card-body">
                                There is not soft cap. CAPITERA platform will soon be available to investors so there is no need for a soft cap.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading57">
                            <a class="card-title lead" data-toggle="collapse" data-target="#collapse57" aria-expanded="false" aria-controls="collapse57" href="#">What is the total number of CAPITERA Tokens?</a>
                        </div>
                        <div id="collapse57" class="collapse" aria-labelledby="heading57" data-parent="#accordion5">
                            <div class="card-body">
                                There are 7 billion CAPITERA tokens. The main reason for this amount of tokens is because this is the maximum amount the platform will be able to manage with near zero risk because for every dollar with near zero risk in a portfolio will be required an CAPITERA token.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading58">
                            <a class="card-title lead" data-toggle="collapse" data-target="#collapse58" aria-expanded="false" aria-controls="collapse58" href="#">When will CAPITERA be listed?</a>
                        </div>
                        <div id="collapse58" class="collapse" aria-labelledby="heading58" data-parent="#accordion5">
                            <div class="card-body">
                                CAPITERA will be listed in 2020 on several exchanges
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading59">
                            <a class="card-title lead" data-toggle="collapse" data-target="#collapse59" aria-expanded="false" aria-controls="collapse59" href="#">Why should I hold CAPITERA?</a>
                        </div>
                        <div id="collapse59" class="collapse" aria-labelledby="heading59" data-parent="#accordion5">
                            <div class="card-body">
                                If you want to manage assets with near zero risk you are required to hold CAPITERA tokens. Also because if its many use cases you have several ways to make money holding CAPITERA tokens.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading560">
                            <a class="card-title lead" data-toggle="collapse" data-target="#collapse560" aria-expanded="false" aria-controls="collapse560" href="#">How do i know my CAPITERA tokens are secure?</a>
                        </div>
                        <div id="collapse560" class="collapse" aria-labelledby="heading560" data-parent="#accordion5">
                            <div class="card-body">
                                In order to make sure your tokens remain secure, we encourage you to use two-factor authentication (2FA).
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <div id="accordion1" class="collapse-icon accordion-icon-rotate left">
                    <!-- <div class="card">
                        <div class="card-header" id="heading23">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse23" aria-expanded="false" aria-controls="collapse23" href="#">When will I see purchased tokens at my personal account?</a>
                        </div>
                        <div id="collapse23" class="collapse show" aria-labelledby="heading23" data-parent="#accordion1">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <div class="card">
                        <div class="card-header" id="heading24">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse24" aria-expanded="false" aria-controls="collapse24" href="#">When should I do the KYC procedure?</a>
                        </div>
                        <div id="collapse24" class="collapse" aria-labelledby="heading24" data-parent="#accordion1">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card">
                        <div class="card-header" id="heading25">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse25" aria-expanded="false" aria-controls="collapse25" href="#">How can I purchase project tokens?</a>
                        </div>
                        <div id="collapse25" class="collapse" aria-labelledby="heading25" data-parent="#accordion1">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading21">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse21" aria-expanded="false" aria-controls="collapse21" href="#">How can I create a crypto-wallet?</a>
                        </div>
                        <div id="collapse21" class="collapse" aria-labelledby="heading21" data-parent="#accordion1">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading22">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse22" aria-expanded="false" aria-controls="collapse22" href="#">When will I receive purchased tokens?</a>
                        </div>
                        <div id="collapse22" class="collapse" aria-labelledby="heading22" data-parent="#accordion1">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <div class="card">
                        <div class="card-header" id="heading26">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse26" aria-expanded="false" aria-controls="collapse26" href="#">What payment options are available?</a>
                        </div>
                        <div id="collapse26" class="collapse" aria-labelledby="heading26" data-parent="#accordion1">
                            <div class="card-body">
                                You can pay for CAPITERA tokens with Bitcoin, Ethereum, Litecoin, XRP, Dash, USD and EUR.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading27">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse27" aria-expanded="false" aria-controls="collapse27" href="#">What is the maximum amount?</a>
                        </div>
                        <div id="collapse27" class="collapse" aria-labelledby="heading27" data-parent="#accordion1">
                            <div class="card-body">
                                The maximum amount per transaction using a debit or credit card is 10,000 USD. There is no maximum amount for paying with crypto. If you want to make a wire transfer please contact our support.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading28">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse28" aria-expanded="false" aria-controls="collapse28" href="#">Do I have to pass KYC to be able to purchase?</a>
                        </div>
                        <div id="collapse28" class="collapse" aria-labelledby="heading28" data-parent="#accordion1">
                            <div class="card-body">
                                Yes, you do need to pass the KYC process as it is required by our compliance department aligned to international standards.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading29">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse29" aria-expanded="false" aria-controls="collapse29" href="#">Can I buy multiple packages of CAPITERA tokens?</a>
                        </div>
                        <div id="collapse29" class="collapse" aria-labelledby="heading29" data-parent="#accordion1">
                            <div class="card-body">
                                Yes, you can buy multiple packages. The higher the investment the more CAPITERA tokens you get.
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card">
                        <div class="card-header" id="heading230">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse230" aria-expanded="false" aria-controls="collapse230" href="#">When will I receive the purchased tokens?</a>
                        </div>
                        <div id="collapse230" class="collapse" aria-labelledby="heading230" data-parent="#accordion1">
                            <div class="card-body">
                                It takes 24 to 48 hours to process your transactions. After this period of time you will see your tokens in the dashboard.
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                <div id="accordion2" class="collapse-icon accordion-icon-rotate left">
                    <!-- <div class="card">
                        <div class="card-header" id="heading34">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse34" aria-expanded="false" aria-controls="collapse34" href="#">When will I receive purchased tokens?</a>
                        </div>
                        <div id="collapse34" class="collapse show" aria-labelledby="heading34" data-parent="#accordion2">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading35">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse35" aria-expanded="false" aria-controls="collapse35" href="#">When should I do the KYC procedure?</a>
                        </div>
                        <div id="collapse35" class="collapse" aria-labelledby="heading35" data-parent="#accordion2">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading31">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse31" aria-expanded="false" aria-controls="collapse31" href="#">What are these addresses for?</a>
                        </div>
                        <div id="collapse31" class="collapse" aria-labelledby="heading31" data-parent="#accordion2">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading32">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse32" aria-expanded="false" aria-controls="collapse32" href="#">How can I create a crypto-wallet?</a>
                        </div>
                        <div id="collapse32" class="collapse" aria-labelledby="heading32" data-parent="#accordion2">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading33">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse33" aria-expanded="false" aria-controls="collapse33" href="#">What are these addresses for?</a>
                        </div>
                        <div id="collapse33" class="collapse" aria-labelledby="heading33" data-parent="#accordion2">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <div class="card">
                        <div class="card-header" id="heading36">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse36" aria-expanded="false" aria-controls="collapse36" href="#">Where can I withdraw the tokens?</a>
                        </div>
                        <div id="collapse36" class="collapse" aria-labelledby="heading36" data-parent="#accordion2">
                            <div class="card-body">
                                You will be able to withdraw the tokens to any ERC 20 compatible wallet.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading37">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse37" aria-expanded="false" aria-controls="collapse37" href="#">How do I create a wallet?</a>
                        </div>
                        <div id="collapse37" class="collapse" aria-labelledby="heading37" data-parent="#accordion2">
                            <div class="card-body">
                                Here is a <a style="padding-left: 0px;" href="https://kb.myetherwallet.com/en/getting-started/how-to-create-a-wallet/" target="_blank">link</a> to a guide t create "My Ether Wallet".
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading38">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse38" aria-expanded="false" aria-controls="collapse38" href="#">Why are the CAPITERA token frozen? </a>
                        </div>
                        <div id="collapse38" class="collapse" aria-labelledby="heading38" data-parent="#accordion2">
                            <div class="card-body">
                                In order to protect the price of CAPITERA tokens we implemented an initial frozen period and a distribution for one year.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading39">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse39" aria-expanded="false" aria-controls="collapse39" href="#">When will CAPITERA tokens be available?</a>
                        </div>
                        <div id="collapse39" class="collapse" aria-labelledby="heading39" data-parent="#accordion2">
                            <div class="card-body">
                                After the purchase the tokens will be frozen for a period of 60 days. After the initial frozen period, the CAPITERA tokens will be made available in equal quantities, every day for 365 days. At the end of the 425 days all tokens will be available.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                <div id="accordion3" class="collapse-icon accordion-icon-rotate left">
                    <!-- <div class="card">
                        <div class="card-header" id="heading42">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse42" aria-expanded="false" aria-controls="collapse42" href="#">How can I create a crypto-wallet?</a>
                        </div>
                        <div id="collapse42" class="collapse show" aria-labelledby="heading42" data-parent="#accordion3">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading43">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse43" aria-expanded="false" aria-controls="collapse43" href="#">When should I do the KYC procedure?</a>
                        </div>
                        <div id="collapse43" class="collapse" aria-labelledby="heading43" data-parent="#accordion3">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading41">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse41" aria-expanded="false" aria-controls="collapse41" href="#">How can I purchase project tokens?</a>
                        </div>
                        <div id="collapse41" class="collapse" aria-labelledby="heading41" data-parent="#accordion3">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading44">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse44" aria-expanded="false" aria-controls="collapse44" href="#">When will I receive purchased tokens?</a>
                        </div>
                        <div id="collapse44" class="collapse" aria-labelledby="heading44" data-parent="#accordion3">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="card">
                        <div class="card-header" id="heading45">
                            <a class="card-title lead collapsed" data-toggle="collapse" data-target="#collapse45" aria-expanded="false" aria-controls="collapse45" href="#">What are these addresses for?</a>
                        </div>
                        <div id="collapse45" class="collapse" aria-labelledby="heading45" data-parent="#accordion3">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2 col-12 order-1">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active show" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">General</a>
            <a class="nav-link" id="v-pills-ico-tab" data-toggle="pill" href="#v-pills-ico" role="tab" aria-controls="v-pills-ico" aria-selected="true">Capitera ICO</a>
            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Payments</a>
            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Withdraw</a>
            <!-- <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Other</a> -->
        </div>
    </div>
</div>