<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">MY WALLET</h4>
            </div>
            <div class="card-content">

                <div class="row" style="margin-left:0px; margin-right: 0px;">
                    <div class="col-md-12 table-responsive pl-0 pr-0">
                        <table class="table table-bordered table-striped text-left"  id="tbl_transaction">
                            <thead>
                                <tr>
                                    <th>Name</th> 
                                    <th>Amount</th>             
                                    <th>Percent</th>
                                    <th>Value, $</th>
                                    <th>Empty</th>
                                    <th>Empty</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($wallet) && !empty($wallet)) {
                                    foreach ($wallet as $val) {
                                        ?>
                                        <tr>
                                            <td>
                                                <p class="m-0"><?= $val->symbol ?></p>
                                                <small><?= $val->currency_name ?></small>
                                            </td>
                                            <td><?= $val->total_balance ?></td>                            
                                            <td>32%</td>
                                            <td><?= $val->total_balance ?></td>
                                            <td>n/a</td>
                                            <td></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>                                    
                </div> 

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl_transaction").dataTable({
            "ordering": true
        });
    });
</script>