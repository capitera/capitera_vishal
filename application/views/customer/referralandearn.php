<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header col-md-8 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Referral & Earn</h3>
            <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>customer/dashboard">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active"><a href="<?= base_url() ?>customer/referral">Referral & Earn</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Purchase token -->
<div class="row">
    <div class="col-12">
        <div class="card pull-up">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="col-12 col-md-12">
                        <h5>Referral Link</h5>
                        <hr/>
                        <form class="form-horizontal form-referral-link row mt-2" action="index.html">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <div class="col-12">
                                <?php
                                $AffiliateUserCommission = $this->common->getAffiliateUserCommission($this->session->userdata("cid"))
                                ?>   
                                <p><strong>Share your referral link with your friends & earn <?= ($AffiliateUserCommission) ? $AffiliateUserCommission : 5 ?>% ERA When your friends purchase ERA.</strong></p>
                                <p style="font-size: 18px;"><strong><label for="first-name">Referral link : </label> <a style="word-wrap: break-word;" href="<?= base_url() ?>referralcode/<?= $referralcode ?>" target="_blank"> <?= base_url() ?>referralcode/<?= $referralcode ?></a></strong></p>
                            </div>
                        </form>
                        <div class="card mt-3">
                            <h5>Referral User History</h5>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table id="recent-orders" class="table table-hover table-xl mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-top-0 text-center">Date</th>
                                                <th class="border-top-0 text-center">Name</th>
                                                <th class="border-top-0 text-center">Email</th>
                                                <th class="border-top-0 text-center">Purchase Tokens</th>
                                                <th class="border-top-0 text-center">Commission</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">
                                            <?php
                                            if (!empty($refferaluser)) {
                                                foreach ($refferaluser as $val) {
                                                    ?>
                                                    <tr>
                                                        <td class="text-truncate"><?= date('m-d-Y h:i A', strtotime($val->reg_date)) ?></td>
                                                        <td class="text-truncate"><?= $val->user ?></td>
                                                        <td class="text-truncate p-1"><?= $val->email ?></td>
                                                        <td class="text-truncate"><?= ($val->purchase_coins) ? $val->purchase_coins : 0 ?> ERA Coin</td>
                                                        <td class="text-truncate"><?= ($val->commission) ? $val->commission : 0 ?> ERA Coin</td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ Purchase token -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#recent-orders").dataTable({
            "ordering": false,
        });
    });
</script>
