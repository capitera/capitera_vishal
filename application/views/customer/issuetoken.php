</div>
<style>
    table{
        text-align: center;
        color: #000;
        font-size: 15px;
        white-space: nowrap;
    }
</style>
<div class="row justify-content-center">
    <div class="col-md-5">

        <div class="form-box">
            <div class="form-heading">
                <?= $this->session->flashdata('msg'); ?>
                <h4 class="h3 text-uppercase">Issue Token</h4>
            </div>
            <div class="form-body">
                <form id="frm_template" name="frm_template" method="post" action="<?= base_url() ?>customer/issuetoken/send_balance">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="hidden" class="form-control" value="<?= $this->session->userdata('cid') ?>" name="to_custid" id="to_custid">
                                <input type="hidden" class="form-control" name="custid" id="custid">
                                <label class="control-label">Enter Your Friend Email :</label>
                                <div>
                                    <input type="email" class="form-control" name="txtemail" id="txtemail" value="" placeholder="Enter Email Id"><span class="errortxtmsg" id="errortxtemail" style="color:red;"></span>
                                </div>                        
                            </div>
                            <div class="form-group">
                                <label class="control-label">Name :</label>
                                <div>
                                    <input type="text" class="form-control" name="txtname" id="txtname" readonly=""><span class="errortxtmsg" id="errortxtname" style="color:red;"></span>
                                </div>                        
                            </div>
                            <div class="form-group">
                                <label class="control-label">Number of Tokens :</label>
                                <div>
                                    <span class="input-icon">
                                        <input type="text" class="form-control" name="txtamount" id="txtamount" value="" onkeypress="return (event.charCode >= 48 & amp; & amp; event.charCode <= 57) || (event.charCode == 46)" placeholder="Enter Number of Tokens"><span class="errortxtmsg" id="errortxtamount" style="color:red;"></span>

                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label"> Wallet Balance :</label>
                                <div>
                                    <span class="input-icon">
                                        <input type="text" class="form-control" id="txtremainwallet" readonly="" value="<?= $wallate_blc ?>">
                                        <span class="errortxtmsg" id="errortxtremainwallet" style="color:red;"></span>

                                    </span>
                                </div>                        
                            </div>
                          
                            <div class="form-group text-center" style="margin-top:20px !important;">
                                <input type="submit" id="btnsubmit" class="btn-secondary btn-gradient-css btn-sm" value="Send Now">
                            </div>
                        </div>

                    </div>                
                </form>
            </div>
        </div>
    </div>
</div>
<br><br>
<div class="card">
    <div class="card-header" style="margin-bottom: 20px;">
        <h4 class="card-title">Issue Token Transactions</h4>
        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-content">
        <div class="row" style="margin-left:15px; margin-right: 15px;">
            <div class="col-md-12 table-responsive">
                <table class="table table-bordered table-striped" id="tbl_transaction">
                    <thead>
                        <tr>
                            <th class="text-truncate">Date</th> 
                            <th class="text-truncate">Email</th>                                      
                            <th class="text-truncate">Note</th>                                      
                            <th class="text-truncate">Token</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($issuetoken)) {
                            foreach ($issuetoken as $val) {
                                ?>
                                <tr>
                                    <td class="text-truncate"><?= date('m-d-Y h:i A', strtotime($val->reg_date)) ?></td>
                                    <td class="text-truncate"><?= $val->email ?></td>                     
                                    <td class="text-truncate"><?= $val->wallet_type; ?></td>                     
                                    <td class="text-truncate"><?= number_format($val->token, 2) ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>                                    
        </div>         
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $("#tbl_transaction").dataTable({
            "ordering": false,
        });

        $("#btnsubmit").on("click", function () {
            if ($("#txtemail").val().trim() == "") {
                $("#errortxtemail").text("Enter Email address..").fadeIn('slow').fadeOut(5000);
                return false;
            } else if (!validateEmail($("#txtemail").val().trim())) {
                $("#errortxtemail").text("Enter Valid Email Id..").fadeIn('slow').fadeOut(5000);
                return false;
            } else if ($("#txtamount").val().trim() == "") {
                $("#errortxtamount").text("Enter Number of Tokens..").fadeIn('slow').fadeOut(5000);
                return false;
            } else if (parseFloat($("#txtamount").val().trim()) < 0) {
                $("#errortxtamount").text("Enter Only Positive..").fadeIn('slow').fadeOut(5000);
                return false;
            } else if (parseFloat($("#txtamount").val().trim()) == 0) {
                $("#errortxtamount").text("Enter Token..").fadeIn('slow').fadeOut(5000);
                return false;
            } else {
                return true;
            }
            return false;
        });

        $('#txtemail').blur(function () {
            var email = $(this).val().trim();
            if (validateEmail(email)) {
                $.ajax({
                    url: '<?= base_url() ?>customer/issuetoken/get_userName',
                    type: 'post',
                    data: {'email': email,"<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (data) {
                            $('#txtname').val(data.userName);
                            $('#custid').val(data.custid);
                        }
                    }
                });
            } else {
                $("#errortxtemail").text("Enter Valid Email Id..").fadeIn('slow').fadeOut(5000);
            }
        });
    });


    function validateEmail(sEmail) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(sEmail);
    }



</script>