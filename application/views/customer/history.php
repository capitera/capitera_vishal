<style>
    .btn-gradient-secondary {
        background-image: linear-gradient(to right, #4CAF50 0%, #8BC34A 51%, #4CAF50 100%);
    }

    .social_icons li{
        margin: 5px;
    }

    .ct-labels .ct-label{
        -webkit-transform: rotate(-35deg);
        -moz-transform: rotate(-35deg);
        -ms-transform: rotate(-35deg);
        -o-transform: rotate(-35deg);
        transform: rotate(-35deg);
    }
    #my_portfoliyo{
        margin-top: 25px;   
    }
    #buy_hold{
        margin-top: 25px; 
    }
    #my_itera{
        margin-top: 25px;  
    }
    #my_wallet{
        margin-top: 25px; 
    }

    .ct-label{
        font-size: 10px;
    }
    .ct-label {
        fill: rgba(255, 255, 255);
        color: rgb(255, 255, 255);
        font-size: 0.75rem;
        line-height: 1;
    }
    /* START: Custom css */
    ul.token-distribution-list li.power::before {
        background: #ec7c68;
    }

    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px; 
        max-width: 500px;
        margin: 1em auto;
    }

    #container {
        height: 400px;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    ul.token-distribution-list li.crowd-sale::before {
        background: #2372a8;
    }
    ul.token-distribution-list li.team::before {
        background: #3bc1e6;
    }
    ul.token-distribution-list li.advisors::before {
        background: #2c3384;
    }
    ul.token-distribution-list li.power::before {
        background: #c6fff8;
    }


    ul.token-distribution-list li.graph1::before {
        background: #092e38;
    }
    ul.token-distribution-list li.graph2::before {
        background: #1c8691;
    }
    ul.token-distribution-list li.graph3::before {
        background: #35ffb8;
    }

    ul.token-distribution-list li.graph4::before {
        background: #72b0b3;
    }
    .header_padding_css{
        padding: 14px 15px 14px 15px;
    }
    .card_title_css{
        padding-top: 5px  
    }
    .btn_padding{
        padding: 0px 14px 0px 0px;
        text-align: right;
    }
    .btn_stye_in_btn{
        padding: 7px 11px 7px 11px;
        width: auto;
    }
    .nav.nav-tabs .nav-item .nav-link.active {
        background-color: #f1f1f1 !important;
    }
    .nav-tabs .nav-link {
        padding: 0.4rem 2.7rem !important;
        border-top-left-radius: 0rem;
        border-top-right-radius: 0.75rem;
    }

    .nav-tabs .nav-item {
        margin-bottom: 0px !important;
    }

    .nav.nav-tabs .nav-item .nav-link.active {
        background-color: #f1f1f1 !important;
        border-radius: 0rem 0.75rem 0 0 !important;
    }
    /* END: Custom css */
</style>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">HISTORY</h4>
            </div>
        </div>   
        <ul class="nav nav-tabs" style="background-color: #e8e8e8">
            <li class="nav-item">
                <a class="nav-link active" id="base-tab1" style="color: #000; background-color: #d8d8d8;" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">PORTFOLIOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab2" style="color: #000; background-color: #d8d8d8; margin-left: 5px; margin-right: 3px;" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">DEPOSIT</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab3" style="color: #000; background-color: #d8d8d8; margin-left: 3px; margin-right: 3px;" data-toggle="tab" aria-controls="tab3" href="#tab3" aria-expanded="false">WITHDRAWAL</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab4" style="color: #000; background-color: #d8d8d8; margin-left: 3px; margin-right: 3px;" data-toggle="tab" aria-controls="tab4" href="#tab4" aria-expanded="false">BUY / SELL</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab5" style="color: #000; background-color: #d8d8d8; margin-left: 3px; margin-right: 3px;" data-toggle="tab" aria-controls="tab5" href="#tab5" aria-expanded="false">REFERRALS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="base-tab6" style="color: #000; background-color: #d8d8d8; margin-left: 3px; margin-right: 3px;" data-toggle="tab" aria-controls="tab6" href="#tab6" aria-expanded="false">SENT TOKENS</a>
            </li>
        </ul>
        <div class="card">

            <div class="card-body" style="padding-left: 0px; padding-top: 10px;">

                <div class="tab-content px-1 pt-1" style="padding-top: 10px !important;">
                    <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                        <div class="row" style="margin-left:1px; margin-right: 3px;">
                            <div class="row table-responsive">
                                <table class="table table-bordered table-striped text-left" id="tbl_transaction_portfolios">
                                    <thead>
                                        <tr>
                                            <th>Transaction ID</th>
                                            <th>Portfolio/Manager</th> 
                                            <th>Base</th>
                                            <th>Initiated</th>
                                            <th>Opened</th>
                                            <th>Closed</th>
                                            <th>Invested</th>
                                            <th>Profit</th>
                                            <th>Final Value</th>
                                            <th></th>                                             
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (isset($portfolio) && !empty($portfolio)) {
                                            foreach ($portfolio as $val) {
                                                $date1 = $val->start_date;
                                                $date2 = $val->end_date;
                                                $diff = abs(strtotime($date2) - strtotime($date1));
                                                $years = floor($diff / (365 * 60 * 60 * 24));
                                                $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                                $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                                $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                                                $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
                                                ?>
                                                <tr>
                                                    <td>TXID 00000001</td>
                                                    <td>
                                                        <div class="name-tddata">
                                                            <img src="<?= base_url() ?>uploads/portfolio_image/<?= $val->portfolio_image ?>" width="40px" height="40px"> 
                                                            <span><?= strtoupper($val->name); ?></span>
                                                            <small><?= $val->manager ?></small>
                                                        </div>
                                                    </td>
                                                    <td ><?= $val->currency_name; ?></td>
                                                    <td class="text-right">2019-12-15</td>
                                                    <td class="text-right"></td>
                                                    <td class="text-right"></td>
                                                    <td class="text-right">$<?= $val->amount_invest; ?></td>
                                                    <td class="text-right">
                                                        <small class="text-success text-bold-700">97.3%</small>
                                                        <p>$11676</p>
                                                    </td>
                                                    <td class="text-right">$23676</td>
                                                    <td>
                                                        <?php if ($val->status == 1) { ?>
                                                            <label class="mb-0 btn-sm btn" style="font-size: 17px;"> <i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></label>
                                                        <?php } else if ($val->status == 2) { ?>
                                                            <label class="mb-0 btn-sm btn" style="font-size: 17px;"> <i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></label>
                                                        <?php } else { ?>
                                                            <label class="mb-0 btn-sm btn" style="font-size: 17px;"> <i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></label>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>                                    
                        </div> 
                    </div>
                    <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                        <div class="row" style="margin-left:1px; margin-right: 3px;">
                            <div class="row table-responsive">
                                <table class="table table-bordered table-striped" id="tbl_transaction_deposit">
                                    <thead>
                                        <tr>
                                            <th>Deposit ID</th> 
                                            <th>Date</th>             
                                            <th>Coin</th>
                                            <th>Amount</th>
                                            <th>Source Address</th>
                                            <th>Destination Address</th>
                                            <th>Status</th>                                             
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>D00000001</td>
                                            <td class="text-right"> <p class="m-0">2019-12-15</p>
                                                <small>14:35:20</small></td>                            
                                            <td> <p class="m-0">ETH</p>
                                                <small>Ethereum</small></td>
                                            <td class="text-right">8.745268</td>
                                            <td><small>0x508b</small></td>
                                            <td><small>0x508b</small></td>
                                            <td><label class="mb-0 btn-sm btn" style="font-size: 17px;">Finished</label></td>
                                        </tr>
                                        <tr>
                                            <td>D00000002</td>
                                            <td class="text-right"> <p class="m-0">2019-12-15</p>
                                                <small>14:35:20</small></td>                            
                                            <td> <p class="m-0">BTC</p>
                                                <small>BITCOIN</small></td>
                                            <td class="text-right">0.745268</td>
                                            <td><small></small></td>
                                            <td><small></small></td>
                                            <td><label class="mb-0 btn-sm btn" style="font-size: 17px;">Pending</label></td>
                                        </tr>
                                        <tr>
                                            <td>D00000003</td>
                                            <td class="text-right"> <p class="m-0">2020-12-15</p>
                                                <small>14:35:20</small></td>                            
                                            <td> <p class="m-0">BCH</p>
                                                <small>BITCOIN Cash</small></td>
                                            <td class="text-right">0.745268</td>
                                            <td><small></small></td>
                                            <td><small></small></td>
                                            <td><label class="mb-0 btn-sm btn" style="font-size: 17px;">Cancelled</label></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                                    
                        </div> 
                    </div>
                    <div class="tab-pane" id="tab3" aria-labelledby="base-tab3">
                        <div class="row" style="margin-left:1px; margin-right: 3px;">
                            <div class="row table-responsive">
                                <table class="table table-bordered table-striped" id="tbl_transaction_withdrawal">
                                    <thead>
                                        <tr>
                                            <th>Deposit ID</th> 
                                            <th>Date</th>             
                                            <th>Coin</th>
                                            <th>Amount</th>
                                            <th>Source Address</th>
                                            <th>Destination Address</th>
                                            <th>Status</th>                                             
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>D00000001</td>
                                            <td class="text-right"> <p class="m-0">2019-12-15</p>
                                                <small>14:35:20</small></td>                            
                                            <td> <p class="m-0">ETH</p>
                                                <small>Ethereum</small></td>
                                            <td class="text-right">8.745268</td>
                                            <td><small>0x508b</small></td>
                                            <td><small>0x508b</small></td>
                                            <td><label class="mb-0 btn-sm btn" style="font-size: 17px;">Finished</label></td>
                                        </tr>
                                        <tr>
                                            <td>D00000002</td>
                                            <td class="text-right"> <p class="m-0">2019-12-15</p>
                                                <small>14:35:20</small></td>                            
                                            <td> <p class="m-0">BTC</p>
                                                <small>BITCOIN</small></td>
                                            <td class="text-right">0.745268</td>
                                            <td><small></small></td>
                                            <td><small></small></td>
                                            <td><label class="mb-0 btn-sm btn" style="font-size: 17px;">Pending</label></td>
                                        </tr>
                                        <tr>
                                            <td>D00000003</td>
                                            <td class="text-right"> <p class="m-0">2020-12-15</p>
                                                <small>14:35:20</small></td>                            
                                            <td> <p class="m-0">BCH</p>
                                                <small>BITCOIN Cash</small></td>
                                            <td class="text-right">0.745268</td>
                                            <td><small></small></td>
                                            <td><small></small></td>
                                            <td><label class="mb-0 btn-sm btn" style="font-size: 17px;">Cancelled</label></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                                    
                        </div> 
                    </div>
                    <div class="tab-pane" id="tab4" aria-labelledby="base-tab4">
                        <div class="row" style="margin-left:1px; margin-right: 3px;">
                            <div class="row table-responsive">

                                <table class="table table-bordered table-striped" id="tbl_transaction_buy_sell">
                                    <thead>
                                        <tr>
                                            <th>Transaction ID</th> 
                                            <th>Date</th>             
                                            <th>Sell</th>
                                            <th>Amount</th>
                                            <th>Buy</th>
                                            <th>Amount</th>
                                            <th>Exchange Rate</th>
                                            <th>Status</th>                                             
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>E00000001</td>
                                            <td class="text-right"> <p class="m-0">2019-12-15</p>
                                                <small>14:35:20</small></td>                            
                                            <td> <p class="m-0">ETH</p>
                                                <small>Ethereum</small></td>
                                            <td class="text-right">8.745268</td>
                                            <td><p class="m-0">USDT</p><small>Tether</small></td>
                                            <td>58.26</td>
                                            <td>658.26</td>
                                            <td><label class="mb-0 btn-sm btn" style="font-size: 17px;">Finished  <i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></label></td>
                                        </tr>
                                        <tr>
                                            <td>E00000002</td>
                                            <td class="text-right"> <p class="m-0">2019-12-15</p>
                                                <small>14:35:20</small></td>                            
                                            <td> <p class="m-0">BTC</p>
                                                <small>BITCOIN</small></td>
                                            <td class="text-right">8.745268</td>
                                            <td><p class="m-0">USDT</p><small>Tether</small></td>
                                            <td>58.26</td>
                                            <td>658.26</td>
                                            <td><label class="mb-0 btn-sm btn" style="font-size: 17px;">Pending  <i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></label></td>
                                        </tr>
                                        <tr>
                                            <td>T00000003</td>
                                            <td class="text-right"> <p class="m-0">2020-12-15</p>
                                                <small>14:35:20</small></td>                            
                                            <td> <p class="m-0">BCH</p>
                                                <small>BITCOIN Cash</small></td>
                                            <td class="text-right">8.745268</td>
                                            <td><p class="m-0">USDT</p><small>Tether</small></td>
                                            <td>58.26</td>
                                            <td>658.26</td>
                                            <td><label class="mb-0 btn-sm btn" style="font-size: 17px;">Cancelled  <i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></label></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>                                    
                        </div> 
                    </div>
                    <div class="tab-pane" id="tab5" aria-labelledby="base-tab5">
                        <div class="row" style="margin-left:1px; margin-right: 3px;">
                            <div class="row table-responsive">
                                <table class="table table-bordered table-striped" id="tbl_transaction_refferals">
                                    <thead>
                                        <tr>
                                            <th>Referral ID</th> 
                                            <th>Join Date</th>             
                                            <th>Name</th>
                                            <th>E-Mail</th>
                                            <th>Level</th>
                                            <th>Commission</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                        if (isset($referral) && !empty($referral)) {
                                            foreach ($referral as $val) {
                                                ?>
                                        <tr>
                                            <td><?= $val->referralcode ?></td>
                                            <td class="text-right"> <p class="m-0"><?= date("Y-m-d",strtotime($val->register_date)) ?></p>
                                                <small>14:35:20</small></td>                            
                                            <td> <?= $val->fullname ?></td>
                                            <td> <?= $val->email ?></td>
                                            <td class="text-right">0</td>
                                            <td>0</td>
                                        </tr>
                                        <?php 
                                            } 
                                            } 
                                            ?>
                                    </tbody>
                                </table>
                            </div>                                    
                        </div> 
                    </div>
                    <div class="tab-pane" id="tab6" aria-labelledby="base-tab6">
                        <div class="row" style="margin-left:1px; margin-right: 3px;">
                            <div class="row table-responsive">
                                <table class="table table-bordered table-striped" id="tbl_transaction_sent_tokens">
                                    <thead>
                                        <tr>
                                            <th>Transaction ID</th> 
                                            <th>Email</th>             
                                            <th>Wallet Type</th>
                                            <th>Token</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (isset($send_token) && !empty($send_token)) {
                                            foreach ($send_token as $val) {
                                                ?>
                                                <tr>
                                                    <td><?= $val->reg_date ?></td>
                                                    <td><?= $val->email ?></td>
                                                    <td> <p class="m-0">ETH</p>
                                                    <td class="text-right"><?= $val->token ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>                                    
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl_transaction_portfolios").dataTable({
            "ordering": true
        });

        $("#tbl_transaction_deposit").dataTable({
            "ordering": true
        });

        $("#tbl_transaction_withdrawal").dataTable({
            "ordering": true
        });

        $("#tbl_transaction_buy_sell").dataTable({
            "ordering": true
        });

        $("#tbl_transaction_refferals").dataTable({
            "ordering": true
        });

        $("#tbl_transaction_sent_tokens").dataTable({
            "ordering": true
        });
    });
</script>
