<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">ACTIVE PORTFOLIO</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content">
                <div class="row" style="margin-left:0px; margin-right: 0px;">
                    <div class="col-md-12 table-responsive pl-0 pr-0">
                        <table class="table table-striped table-bordered text-left table-hover table-full-width" id="tbl_transaction">
                            <thead>
                                <tr>
                                    <th>Name/Manager</th> 
                                    <th>Base</th>
                                    <th>Time Frame</th>
                                    <th>Invested</th>
                                    <th>Current Profit</th>
                                    <th>Profit Target</th>
                                    <th>Current Value</th>
                                    <th>Status</th>                                             
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($portfolio) && !empty($portfolio)) {
                                    foreach ($portfolio as $val) {
                                        $date1 = $val->start_date;
                                        $date2 = $val->end_date;
                                        $diff = abs(strtotime($date2) - strtotime($date1));
                                        $years = floor($diff / (365 * 60 * 60 * 24));
                                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                                        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
                                        ?>
                                        <tr>
                                            <td>
                                                <div class="name-tddata">
                                                    <img src="<?= base_url() ?>uploads/portfolio_image/<?= $val->portfolio_image ?>" width="40px" height="40px"> 
                                                    <span><?= strtoupper($val->name); ?></span>
                                                    <small><?= $val->manager ?></small>
                                                </div>
                                            </td>
                                            <td ><?= $val->currency_name; ?></td>
                                            <td class="text-right"><?= $days . "d, " . $hours . "h, " . $minutes . "m" ?></td>
                                            <td class="text-right">$<?= $val->amount_invest; ?></td>
                                            <td class="text-right">
                                                <small class="text-success text-bold-700">97.3%</small>
                                                <p>$11676</p>
                                            </td>
                                            <td class="text-right">
                                                <small class="text-bold-700">150%</small>
                                                <p>$18000</p>
                                            </td>
                                            <td class="text-right">$23676</td>
                                            <td>
                                                <?php if ($val->status == 1) { ?>
                                                    <label class="mb-0 btn-sm btn" style="font-size: 17px;">Active <i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></label>
                                                <?php } else if ($val->status == 2) { ?>
                                                    <label class="mb-0 btn-sm btn" style="font-size: 17px;">Reject <i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></label>
                                                <?php } else { ?>
                                                    <label class="mb-0 btn-sm btn" style="font-size: 17px;">Pending <i class="fa fa-angle-right" style="font-size: 26px; margin-left: 16px;"></i></label>
                                                    <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>                                    
                </div>         
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#tbl_transaction").dataTable({
            "ordering": true,
        });

    });
</script>