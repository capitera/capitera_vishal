<!DOCTYPE html>
<?php
$uri_segment = $this->uri->segment(2);
?>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Dashboard - CAPITERA </title>
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>newassets/app-assets/images/ico/fevicon.png">
        <!--<link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i|Comfortaa:300,400,500,700" rel="stylesheet">-->
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/vendors/css/forms/icheck/icheck.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/vendors/css/forms/icheck/custom.css">

        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/vendors/css/charts/chartist.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
        <!-- END VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/css/pages/account-register.css">
        <!-- BEGIN MODERN CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/css/app.css">
        <!-- END MODERN CSS-->
        <link href="<?= base_url() ?>assets/alertify/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/alertify/alertify.default.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN Page Level CSS-->        
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/css/pages/transactions.css">
        <!-- END Page Level CSS-->

    

        <link href="<?= base_url() ?>assets/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">

        <link href="<?= base_url() ?>assets/vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
        <link href="<?= base_url() ?>assets/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/vendors/css/cryptocoins/cryptocoins.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/css/pages/timeline.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/app-assets/css/pages/dashboard-ico.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/assets/css/style.css">
        <!-- END Custom CSS-->
            <link href="<?= base_url() ?>assets/vendor/select2/select2.min.css" rel="stylesheet" media="screen">
        <link href="<?= base_url() ?>assets/vendor/DataTables/css/DT_bootstrap.css" rel="stylesheet" media="screen">
       
        <!-- BEGIN VENDOR JS-->
        
        
        <script src="<?= base_url() ?>newassets/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <style>
            .vertical-compact-menu .main-menu .navigation > li > a > span {
                visibility: visible !important;
                font-size: 0.7rem;
            }
            
            #tbl_transaction_wrapper{
                margin-top: 10px; 
            }
            /*            .dataTables_wrapper .dataTables_length label {
                            display: none;
                        }
                        .dataTables_wrapper .dataTables_filter label {
                            display: none;
                        }
                        .dataTables_wrapper .dataTables_info {
                            display: none;
                        }*/


        </style>
        <?php
        $theme_status = $this->session->userdata('theme_color');
        if ($theme_status == 1) {
            ?>
            <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/assets/css/style_dark.css">
        <?php } else { ?>
            <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/assets/css/style_light.css">
        <?php } ?>
    </head>
    <body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
        <!-- fixed-top-->
        <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
            <div class="navbar-wrapper">
                <div class="navbar-header d-md-none">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item d-md-none"><a class="navbar-brand" href="<?= base_url() ?>customer/dashboard"><img class="brand-logo d-none d-md-block" alt="crypto ico admin logo" src="<?= base_url() ?>newassets/app-assets/images/logo/logo.png"><img class="brand-logo d-sm-block d-md-none" alt="crypto ico admin logo sm" style="width: 130px;" src="<?= base_url() ?>/newassets/app-assets/images/logo/logo-sm.png"></a></li>
                        <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v">   </i></a></li>
                    </ul>
                </div>
                <div class="navbar-container">
                    <div class="collapse navbar-collapse" id="navbar-mobile">
                        <ul class="nav navbar-nav mr-auto float-left">
                            <a class="navigation-brand mt-1 mb-1 d-none d-md-block d-lg-block d-xl-block text-center" href="<?= base_url() ?>customer/dashboard">
                                <?php if ($theme_status == 0) { ?>
                                    <img class="brand-logo" alt="crypto ico admin logo" style="width: 200px;" src="<?= base_url() ?>newassets/app-assets/images/logo/logo.png"/>
                                <?php } else { ?>
                                    <img class="brand-logo" alt="crypto ico admin logo" style="width: 200px;" src="<?= base_url() ?>newassets/app-assets/images/logo/logo_2.png"/>
                                <?php } ?>
                            </a>
                        </ul>
                        <ul class="nav navbar-nav float-right">
                            <!--                            <li class="nav-item text-color">
                                                            <div class="form-group" style="margin-right: 10px; margin-top: 14px;">
                                                                <div class="btn-group" role="group">
                            <?php if ($theme_status == 0) { ?>
                                                                                        <button type="button" class="btn btn-outline-info dark_btn" id="dark_btn"><i class="ficon ft-moon"></i></button>
                            <?php } else { ?>
                                                                                        <button type="button" class="btn btn-outline-info light_btn" id="light_btn"><i class="ficon ft-sun"></i></button>
                            <?php } ?>
                                                                </div>
                                                            </div> 
                                                        </li>-->
                            <li class="nav-item text-color"><div id="account_wallet" class="mr-1" style="padding-top: 25px; font-weight: 400;">Account Value <span class="ml-1 font-medium-2" style="font-weight: 600;">$40,670.0</span></div></li>
                            <li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 1.7rem 1rem;"><i class="ficon ft-star"></i><span class="badge-pill badge-default badge-default badge-up badge-glow">3</span><span class="selected-language"></span></a>
                                <div class="dropdown-menu" aria-labelledby="dropdown-flag"><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-gb"></i> English</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> Chinese</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-de"></i> German</a></div>
                            </li>
                            <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-bell"></i><span class="badge-pill badge-default badge-default badge-up badge-glow">5</span></a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span></h6><span class="notification-tag badge badge-default badge-danger float-right m-0">5 New</span>
                                    </li>
                                    <li class="scrollable-container media-list w-100"><a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">You have new order!</h6>
                                                    <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p><small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">30 minutes ago</time></small>
                                                </div>
                                            </div></a><a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center"><i class="ft-download-cloud icon-bg-circle bg-red bg-darken-1"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading red darken-1">99% Server load</h6>
                                                    <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p><small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Five hour ago</time></small>
                                                </div>
                                            </div></a><a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center"><i class="ft-alert-triangle icon-bg-circle bg-yellow bg-darken-3"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading yellow darken-3">Warning notifixation</h6>
                                                    <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p><small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Today</time></small>
                                                </div>
                                            </div></a><a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center"><i class="ft-check-circle icon-bg-circle bg-cyan"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Complete the task</h6><small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last week</time></small>
                                                </div>
                                            </div></a><a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center"><i class="ft-file icon-bg-circle bg-teal"></i></div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Generate monthly report</h6><small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last month</time></small>
                                                </div>
                                            </div></a></li>
                                    <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="javascript:void(0)">Read all notifications</a></li>
                                </ul>
                            </li>
                            <!--<li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon ft-settings"> </i></a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <h6 class="dropdown-header m-0"><span class="grey darken-2">Setting</span></h6>
                                    </li>
                                    <li class="scrollable-container media-list w-100">
                                        <div class="form-group" style="margin-top: 30px; margin-left: 20px;">
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-outline-info light_btn" id="light_btn">Light</button>
                                                <button type="button" class="btn btn-outline-info dark_btn" id="dark_btn">Dark</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="dropdown-menu-footer"></li>
                                </ul>
                            </li>-->
                            <li class="dropdown dropdown-user nav-item">
                                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                    <span class="user-text">
                                        <span class="user-name text-bold-600">
                                            <?= $this->session->userdata('cname') ?>
                                        </span>
                                    </span>
                                    <span class="avatar avatar-online">
                                        <?php
                                        $profile_pic = $this->common->getProfile($this->session->userdata('cid'));
                                        if (!empty($profile_pic)) {
                                            ?>
                                            <img src="<?= base_url() ?>assets/profiles/<?= $profile_pic ?>" alt="avatar">
                                            <?php
                                        } else {
                                            ?>
                                            <img src="<?= base_url() ?>newassets/app-assets/images/portrait/small/avatar-s-1.png" alt="avatar">
                                            <?php
                                        }
                                        ?>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" style="margin-top: 10px; right: -7px; box-shadow: 1px 1px 4px 3px rgba(0,0,0,0.1) !important; border-radius: 0px;">
                                    <a class="dropdown-item" style="font-weight: 700;" href="<?= base_url() ?>customer/myprofile">My Profile</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item"  style="font-weight: 700;" href="#">Messages </a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item"  style="font-weight: 700;" href="<?= base_url() ?>customer/setting"> Security</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item"  style="font-weight: 700;" href="<?= base_url() ?>customer/changepassword"> Change Password</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item"  style="font-weight: 700;" href="<?= base_url() ?>customer/loginHistory">Login History</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item"  style="font-weight: 700;" href="<?= base_url() ?>login/logout"> Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow custom-margin-nav">
            <div class="main-menu-content">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class="nav-item border-first-top <?= ($uri_segment == 'dashboard') ? 'active' : ''; ?>"><a href="<?= base_url() ?>customer/dashboard"><i class="icon-grid"></i><span class="menu-title" data-i18n="">DASHBOARD</span></a></li>
                    <li class="nav-item <?= ($uri_segment == 'portfolio') ? 'active' : ''; ?>"><a href="<?= base_url() ?>customer/portfolios"><i class="icon-shuffle"></i><span class="menu-title" data-i18n="">PORTFOLIOS</span></a></li>
                    <li class="nav-item <?= ($uri_segment == 'portfolio') ? 'active' : ''; ?>"><a href="<?= base_url() ?>customer/invest"><i class="icon-shuffle"></i><span class="menu-title" data-i18n="">INVEST</span></a></li>
                    <li class="nav-item hide <?= ($uri_segment == 'buyandhold') ? 'active' : ''; ?>"><a href="<?= base_url() ?>customer/buyandhold"><i class="icon-basket"></i><span class="menu-title" data-i18n="">BUY & HOLD</span></a></li>
                    <li class="nav-item <?= ($uri_segment == 'myitera') ? 'active' : ''; ?>"><a href="<?= base_url() ?>customer/myitera"><i class="icon-shuffle"></i><span class="menu-title" data-i18n="">MY ITERA</span></a></li>
                    <li class="nav-item <?= ($uri_segment == 'withdraw') ? 'active' : ''; ?>"><a href="#"><i class="icon-credit-card"></i><span class="menu-title" data-i18n="">CARD</span></a></li>
                    <li class="nav-item <?= ($uri_segment == 'wallet') ? 'active' : ''; ?>"><a href="<?= base_url() ?>customer/wallet"><i class="icon-wallet"></i><span class="menu-title" data-i18n="">WALLET</span></a></li>
                    <li class="nav-item <?= ($uri_segment == 'history') ? 'active' : ''; ?>"><a href="<?= base_url() ?>customer/history"><i class="ft-link"></i><span class="menu-title" data-i18n="">HISTORY</span></a></li>
                    <li class="nav-item <?= ($uri_segment == 'downlink') ? 'active' : ''; ?>"><a href="#"><i class="ft-link"></i><span class="menu-title" data-i18n="">INVITE</span></a></li>
                    <!--<li class="nav-item <?= ($uri_segment == 'kyc_document') ? 'active' : ''; ?>"><a href="<?= base_url() ?>customer/kyc_document"><i class="ft-file"></i><span class="menu-title" data-i18n="">KYC Document</span></a></li>-->
                    <li class="nav-item"><a href="#"><i class="icon-doc"></i><span class="menu-title" data-i18n="">HELP</span></a></li>
                    <li class="nav-item"><a href="#"><i class="icon-question"></i><span class="menu-title" data-i18n="">TUTORIAL</span></a></li>
                </ul>
            </div>
        </div>

        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <!-- Dashboard Details -->
                    <!-- First 3 Column -->