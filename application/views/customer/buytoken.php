<style>
    .btn-gradient-secondary {
        background-image: linear-gradient(to right, #4CAF50 0%, #8BC34A 51%, #4CAF50 100%);
    }
</style>
<script src="<?= base_url() ?>assets/js/clipboard.min.js"></script>
<?php
$kyc_status = $this->common->getKycStatus($this->session->userdata('cid'));
if ($kyc_status == 1) {
    ?>
    <div class="row">
        <div class="col-lg-12 custom-padding-column">
            <div class="card">
                <div class="card-header" style="margin-bottom: 20px;">
                    <h4 class="card-title">BUY CAPITERA COIN</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content">
                    <div class="row ml-1 mr-1 my-itera">
                        <?php
                        if (!empty($token_packages)) {
                            foreach ($token_packages as $val) {
                                ?>
                                <div class="col-sm-6 col-md-3 col-xs-12">
                                    <div class="card pull-up profile-card-with-cover">
                                        <div class="card-content card-deck text-center">
                                            <div class="card box-shadow">
                                                <div class="card-header">
                                                    <h6><?= $val->package_name ?></h6>
                                                </div>
                                                <div class="card-body">
                                                    <h1 class="pricing-card-title">$<?= $val->price ?>/-
                                                    </h1>
                                                    <ul class="list-unstyled mt-2 mb-2">
                                                        <li><?= $val->no_of_token ?> Capitera Coin</li>                                        
                                                    </ul>
                                                    <img src="<?= base_url() ?>assets/package/<?= $val->image; ?>" style="width: 100%;" >
                                                    <br><br>
                                                    <button type="button" data-packageid="<?= $val->package_id ?>" class="buy_package_btn btn-secondary btn-gradient-css btn-sm">Buy Now</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="row justify-content-center">
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <div class="card">
                        <div class="card-content card-deck">
                            <div class="card box-shadow">
                                <div class="card-body">
                                    <!--<p class="text-center my-0 font-weight-bold primary">Please fill KYC first.</p>-->
                                    <div class="alert alert-icon-left alert-warning alert-dismissible mb-0" role="alert">
                                        <span class="alert-icon"><i class="la la-warning"></i></span>
                                        For buy packages you have to complete KYC first. Once you submit KYC and its approved you will able to purchase packages.
                                    </div>
                                    <div class="text-center mt-1"><a href="<?= base_url() ?>customer/myprofile" class="btn btn-primary btn-min-width">Submit KYC</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!-- Purchase token -->
        <div class="row justify-content-center calculation_row hidden">
            <div class="col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="card pull-up">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div id="bestrate-widget"></div>
                            <div class="form-horizontal form-purchase-token row">
                                <div class="col-md-12 ">
                                    <p>You've Selected <b class="packageName">Capitera Bronze Package</b></p>
                                    <fieldset class="form-group mb-1">
                                        <label for="amount">Quantity: <span id="errortxt_amount" style="color:red"></span></label>
                                        <input type="number" class="form-control col-md-8" placeholder="Quantity" value="1" id="quantity" name="quantity" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)'>
                                        <input type="hidden" name="package_id" id="package_id">
                                    </fieldset>
                                    <fieldset class="form-group mb-1">
                                        <!-- <label for="amount">Amount in USD: <span id="errortxt_amount" style="color:red"></span></label> -->
                                        <input type="hidden" class="form-control col-md-8" placeholder="Amount in USD" id="txt_amount" name="txt_amount" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)'>
                                    </fieldset>
                                    <input type="hidden" id="rate" name="rate" value="<?= $coin_rate ?>">
                                    <input type="hidden" id="coins" name="coins" value="0" >
                                    <input type="hidden" id="no_of_coin" name="no_of_coin" value="0" >
                                    <input type="hidden" id="bonus" name="bonus" value="0" >
                                    <input type="hidden" id="total_bonus" name="total" value="0" >
                                    <!-- <p><label class="control-label">You will get <span class="fans_amt" id="actual_coin">0</span> ERA Coin + <span class="fans_amt" id="bonus_coin">0</span> Bonus ERA Coin</label></p>
                                    <p><label class="control-label">Total = <span class="fans_amt" id="total_coin">0</span> ERA Coin</label></p>   -->
                                    <!-- <input type="hidden" id="rate" name="rate" value="<?= $coin_rate ?>">
                                    <input type="hidden" id="coins" name="coins" value="0" >
                                    <input type="hidden" id="bonus" name="bonus" value="0" >
                                    <input type="hidden" id="total_bonus" name="total" value="0" > -->
                                    <input type="hidden" id="price" name="price" value="0" >
                                    <p><label class="control-label">You will get <span class="no_of_coins" id="actual_coin">0</span> Capitera Coin </label></p>
                                    <p><label class="control-label">Total Payment : <span class="total_payment" id="total_amt"></span></label></p>  
                                    <fieldset class="mb-3">
                                        <div class="input-group col-md-8">
                                            <div class="d-inline-block custom-radio mr-1" style="width: 100%; height: 25px;">
                                                <input type="radio" class="custom-control-input" name="pay_method" id="payRadios2" value="bitcoin" checked>
                                                <label class="custom-control-label" for="payRadios2">Pay Using Bitcoin</label>
                                            </div>
                                            <div class="d-inline-block custom-radio mr-1" style="width: 100%; height: 25px;">
                                                <input type="radio" class="custom-control-input" name="pay_method" id="payRadios1" value="ethereum">
                                                <label class="custom-control-label" for="payRadios1">Pay Using Ethereum</label>
                                            </div>
                                            <div class="d-inline-block custom-radio mr-1" style="width: 100%; height: 25px;">
                                                <input class="custom-control-input" type="radio" name="pay_method" id="payRadios5" value="litecoin">
                                                <label class="custom-control-label" for="payRadios5">Pay Using Litecoin</label>
                                            </div>
                                            <div class="d-inline-block custom-radio mr-1" style="width: 100%; height: 25px;">
                                                <input class="custom-control-input" type="radio" name="pay_method" id="payRadios6" value="XRP">
                                                <label class="custom-control-label" for="payRadios6">Pay Using XRP</label>
                                            </div>
                                            <div class="d-inline-block custom-radio mr-1" style="width: 100%; height: 25px;">
                                                <input class="custom-control-input" type="radio" name="pay_method" id="payRadios8" value="Dash">
                                                <label class="custom-control-label" for="payRadios8">Pay Using Dash</label>
                                            </div>
                                            <div class="d-inline-block custom-radio mr-1" style="width: 100%; height: 25px;">
                                                <input class="custom-control-input" type="radio" name="pay_method" id="payRadios9" value="Credit-Card">
                                                <label class="custom-control-label" for="payRadios9">Pay Using Credit Card</label>
                                            </div>
                                            <!-- <div class="d-inline-block custom-radio mr-1">
                                                <input type="radio" name="pay_method" id="payRadios3" value="stripe" class="custom-control-input">
                                                <label class="custom-control-label" for="pay-credit">Pay Using Credit / Debit Card</label>
                                            </div> --> 
                                            <!-- <div class="d-inline-block custom-radio mr-1" style="width: 100%; height: 25px;">
                                                <input class="custom-control-input" type="radio" name="pay_method" id="payRadios9" value="paypal">
                                                <label class="custom-control-label" for="pay-ltc">Pay Using Paypal</label>
                                            </div> -->
                                            <!-- <div class="d-inline-block custom-radio mr-1" style="width: 100%; height: 25px;">
                                                <input class="custom-control-input" type="radio" name="pay_method" id="payRadios10" value="offline">
                                                <label class="custom-control-label" for="pay-ltc">Pay Using Offline</label>
                                            </div> -->
                                            <div class="form-check"><span id="errorpublishablekey" style="color:red; font-size:16px"></span><span id="successpublishablekey" style="color:green; font-size:16px"></span></div>
                                        </div>
                                    </fieldset>
                                    <div class="col-md-12 text-center">
                                        <button type="button"  id="btn_submit_dashboard" name="btn_submit_dashboard" class="btn btn-secondary btn-gradient-css btn-sm">Pay Now</button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-12"></div>
            <div class="col-md-12 col-xs-12 second_section hidden">
                <div class="card pull-up">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form name="frm_paid" id="frm_paid" method="post" action="<?= base_url() ?>customer/buytoken/transaction_paid">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                <div class="row col-md-12  col-xs-12 mt-3">
                                    <div class="row">

                                        <input type="hidden" name="c_txnid" id="c_txnid" >
                                        <input type="hidden" name="c_address" id="c_address">
                                        <input type="hidden" name="c_crypto" id="c_crypto">
                                        <div class="col-md-6">
                                            <div class="coin_address_div coin_address_kent" style="margin-top:25px;margin-left: 20px;display: none;">
                                                <div class="">
                                                    <div class="form-group" style="font-size: larger;">
                                                        <p class="mb-0"><label class="control-label"><span id="bit_method_all">Please send</span> <span id="bit_method_ethereum">Please send </span><span class="fans_amt" id="bit_coin"></span> <span id="bit_method_all1">to the address below. If you send any other amount, Payment system will ignore it ! <br><b> <span id="coin_type_copy"></span> Address <input type="text" style="width: 90%; border: hidden;" readonly id="fans_amt_alladdress"><img class="btn" id="cycopy"  src="<?= base_url() ?>assets/images/clippy.png" style="width: 25px; padding: 0" alt="Copy to clipboard"></b></span><span id="bit_method_ethereum1"> to Ethereum Address <b> <input type="text" style="width: 360px; border: hidden;" readonly id="fans_amt_ethe"><img class="btn" id="ethecycopy"  src="<?= base_url() ?>assets/images/clippy.png" style="width: 25px; padding: 0" alt="Copy to clipboard"> </b></span><span id="msg1"></span> </label></p>                                                
                                                        <p class="mb-0"><label class="control-label">Send the amount requested using either the barcode to the right or by cutting and pasting the address into the requested sending field of your <span id="coin_type_copy_second"></span> wallet when transferring.</label></p>
                                                        <p class="mb-0"><label class="control-label">Only after your wallet has shown a successful send, click on the green button to the right which says Click here when transfer is complete to acknowledge that your transfer was completed.</label></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-md-6 col-xs-12 coin_address_kent">
                                            <div class="">
                                                <div class="form-group" style="text-align: center;">
                                                    <img src="" alt="QR Code" class="img img-thumbnail address_qr_code" style="width:40%; height: 40%;" ><br>
                                                    <p class="mb-0"><label class="control-label coin_address_label" style="width: 100%;"><span id="coin_type"></span>  Address: <input type="text" style="width: 85%; border: hidden; text-align: center; height: 35px;" readonly class="fans_amt" id="coin_address"> <img class="btn" id="copy"  src="<?= base_url() ?>assets/images/clippy.png" style="width: 25px; padding: 0" alt="Copy to clipboard"></label></p>
                                                    <p class="mb-0"><label class="control-label coin_address_label_ethereum" style="margin-top:10px; width: 100%"><span id="coin_type_ethe"></span>  Address: <input type="text" style="width: 85%; border: hidden; text-align: center;" readonly class="fans_amt" id="coin_address_ethe"> <img class="btn" id="ethacopy"  src="<?= base_url() ?>assets/images/clippy.png" style="width: 25px; padding: 0" alt="Copy to clipboard"></label></p>
                                                    <span id="msg"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xs-12 text-center" style="margin-top:10px;">
                                                <div class="form-group">
                                                    <button type="submit" id="btn_paid_coin" class="btn-gradient-secondary" >Click here when transfer is complete</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </form>
                            <form name="frm_paymnet_offline" style="display: none" id="frm_paymnet_offline" method="post" action="<?= base_url() ?>customer/buytoken/transaction_offline">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                <div class="row col-12">
                                    <div class="row">
                                        <input type="hidden" id="coin_offline" name="coin_offline" value="">
                                        <input type="hidden" id="bonus_offline" name="bonus_offline" value="">
                                        <input type="hidden" id="total_bonus_offline" name="total_bonus_offline" value="">
                                        <input type="hidden" id="amount_offline" name="amount_offline" value="">
                                        <input type="hidden" id="rate_offline" name="rate_offline" value="">
                                        <input type="hidden" id="method_offline" name="method_offline" value="">
                                        <div class="col-md-12 col-12">
                                            <div class="col-md-8 col-sm-12 offset-md-2">
                                                <div class="form-group" style="font-size: larger;">
                                                    <p class="mb-0"><b>STEP-1 : </b>If you send any other amount, Payment system will ignore it !</p>                                                
                                                    <p class="mb-0"><b>STEP-2 : </b>Send the amount requested using either the barcode to the right or by cutting and pasting the address into the requested sending field of your</p>
                                                    <p class="mb-0"><b>STEP-3 : </b>Only after your wallet has shown a successful send, click on the green button to the right which says Click here when transfer is complete to acknowledge that your transfer was completed.</p>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 text-center" style="margin-top:10px;">
                                                <div class="form-group">
                                                    <button type="submit" id="btn_offline_payment" class="btn btn-gradient-secondary" >Offline Payment</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                            </form>
                            <div class="row">
                                <form id="paypalForm" method="post" action="<?= base_url() ?>customer/buytoken/transactionPaypal">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <input type="hidden" id="coin_1" name="coin_1" value="" >
                                    <input type="hidden" id="bonus_1" name="bonus_1" value="">
                                    <input type="hidden" id="total_bonus_1" name="total_bonus_1" value="">
                                    <input type="hidden" id="amount_1" name="amount_1" value="">
                                    <input type="hidden" id="rate_1" name="rate_1" value="">
                                    <input type="hidden" name="method_1" value="paypal">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--/ Purchase token -->
<script src="https://checkout.stripe.com/checkout.js"></script>
<?php
$msg = $this->input->get("msg");
switch ($msg) {
    case "S":
        $m = "Transaction was Successful...!!!";
        $t = "success";
        break;
    case "E":
        $m = "Transaction was Declined!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>
<script type="text/javascript">
                                    $(document).ready(function () {
                                        document.addEventListener('contextmenu', function (e) {
                                            e.preventDefault();
                                        });
                                        $(".coin_address_kent").hide();

<?php if ($msg): ?>
                                            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>

                                        $('.buy_package_btn').on('click', function () {
                                            var package_id = $(this).attr('data-packageid');

                                            $.ajax({
                                                url: "<?= base_url() ?>customer/buytoken/getPackageCoins",
                                                type: "post",
                                                data: {'package_id': package_id, "<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                                                dataType: "json",
                                                success: function (data, textStatus, jqXHR) {

                                                    if (data != '0') {
                                                        $('.packageName').text(data.package_name);
                                                        $('.no_of_coins').text(data.no_of_token);
                                                        $('#no_of_coin').val(data.no_of_token);
                                                        $('#coins').val(data.no_of_token);
                                                        $('.total_payment').text('$' + data.price);
                                                        $('#price').val(data.price);
                                                        $('#package_id').val(data.package_id);
                                                        $('#quantity').focus();
                                                        $('.calculation_row').removeClass('hidden');
                                                        $('#txt_amount').val(data.no_of_token);
                                                        $('#quantity').focus();
                                                    } else {
                                                        alertify.error('Something went wrong, Please try again!');
                                                    }
                                                }
                                            });
                                        });

                                        $('#quantity').blur(function () {
                                            var qty = $('#quantity').val();

                                            if (0 >= qty) {
                                                alertify.error('Please enter valid quantity..!');
                                                $('#btn_submit_dashboard').attr('disabled', 'disabled');
                                            } else {
                                                var coins = $('#no_of_coin').val();
                                                var payment = $('#price').val();
                                                var total_coins = qty * coins;
                                                var total_payment = qty * payment;
                                                $('.no_of_coins').text(total_coins);
                                                $('.total_payment').text('$' + total_payment);
                                                $('#txt_amount').val(total_payment);
                                                $('#coins').val(total_coins);
                                                $('#btn_submit_dashboard').removeAttr('disabled', 'disabled');
                                            }

                                        });

                                        /*$('#quantity').blur(function () {
                                         var bonus_arr = JSON.parse('<?php echo json_encode($icoround); ?>');
                                         if (bonus_arr) {
                                         var amount = parseFloat($('#txt_amount').val());
                                         var rate = '<?= (float) $coin_rate ?>';
                                         var bonus = 0;
                                         var actual_coin = 0;
                                         var total_coin = 0;
                                         if (amount > 0 && rate > 0) {
                                         actual_coin = amount / rate;
                                         
                                         // $.each(bonus_arr, function (key, val) {
                                         //  if (amount >= val.from && amount <= val.to) {
                                         //  bonus = (amount * val.percentage) / 100;
                                         
                                         //  }
                                         //  })
                                         
                                         // bonus = (actual_coin * bonus_arr.bonus) / 100;
                                         total_coin = actual_coin;
                                         
                                         }
                                         
                                         if (parseFloat(bonus_arr.total_token) <= parseFloat(total_coin))
                                         {
                                         $("#errortxt_amount").text("ICO Token Outsold").fadeIn('slow').fadeOut(5000);
                                         $(':input[type="button"]').prop('disabled', true);
                                         } else {
                                         $(':input[type="button"]').prop('disabled', false);
                                         }
                                         
                                         $('#actual_coin').text(actual_coin.toFixed(2));
                                         $('#bonus_coin').text(bonus.toFixed(2));
                                         $('#total_coin').text(total_coin.toFixed(2));
                                         $('#coins').val(actual_coin.toFixed(8));
                                         $('#bonus').val(bonus.toFixed(8));
                                         $('#total_bonus').val(total_coin.toFixed(8));
                                         } else {
                                         $("#errortxt_amount").text("ICO Token Outsold").fadeIn('slow').fadeOut(5000);
                                         $(':input[type="button"]').prop('disabled', true);
                                         }
                                         });*/

                                        $('#btn_submit_dashboard').click(function () {
                                            $('.second_section').removeClass('hidden');
                                            var qty = $('#quantity').val();
                                            if (0 >= qty) {

                                                alertify.error('Please enter valid quantity..!');
                                                $('#btn_submit_dashboard').attr('disabled', 'disabled');

                                            } else {

                                                var coin = $('#coins').val();
                                                var bonus = 0;
                                                var total_bonus = 0;
                                                var amount = parseFloat($('#txt_amount').val());
                                                var package_id = $('#package_id').val();
                                                var rate = $('#rate').val();
                                                var pay_method = $('input[name=pay_method]:checked').val();

                                                if (pay_method == 'stripe') {
                                                    if ('<?= $publickey->account_key ?>' != '') {
                                                        var handler = StripeCheckout.configure({
                                                            key: '<?= trim($publickey->account_key) ?>',
                                                            image: "",
                                                            locale: 'auto',
                                                            token: function (token) {
                                                                $.ajax({
                                                                    url: "<?= base_url() ?>customer/buytoken/transactionStrip",
                                                                    type: "post",
                                                                    data: {'coin': coin, 'bonus': bonus, 'total_bonus': total_bonus, 'amount': amount, 'rate': rate, 'method': pay_method, 'stripeToken': token.id, "<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                                                                    dataType: "json",
                                                                    success: function (data, textStatus, jqXHR) {
                                                                        if (data.result == 'success') {
                                                                            $("#successpublishablekey").text("Transaction was Successful...!").fadeIn('slow').fadeOut(5000);
                                                                            window.setTimeout('location.reload()', 3000);
                                                                        } else {
                                                                            $("#errorpublishablekey").text("Transaction was Declined").fadeIn('slow').fadeOut(5000);
                                                                            window.setTimeout('location.reload()', 3000);
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        });
                                                        handler.open({
                                                            currency: 'USD',
                                                            name: "Capitera Token",
                                                            description: "Purchase Tokens",
                                                            amount: amount * 100,
                                                            allowRememberMe: false,
                                                            email: "info@capitera.io"

                                                        });
                                                        return false;
                                                    } else {
                                                        $("#errorpublishablekey").text("Please Enter Publishable key...!").fadeIn('slow').fadeOut(5000);
                                                    }
                                                } else if (pay_method == 'paypal') {
                                                    $('#coin_1').val(coin);
                                                    $('#bonus_1').val(bonus);
                                                    $('#total_bonus_1').val(total_bonus);
                                                    $('#amount_1').val(amount);
                                                    $('#rate_1').val(rate);
                                                    $('#paypalForm').submit();
                                                } else if (pay_method == 'offline') {
                                                    $('#frm_paymnet_offline').show();
                                                    $(':radio:not(:checked)').attr('disabled', true);
                                                    $('#coin_offline').val(coin);
                                                    $('#bonus_offline').val(bonus);
                                                    $('#total_bonus_offline').val(total_bonus);
                                                    $('#amount_offline').val(amount);
                                                    $('#rate_offline').val(rate);
                                                    $('#method_offline').val(pay_method);
                                                    $('#btn_submit_dashboard').hide();
                                                } else {
                                                    $.ajax({
                                                        url: "<?= base_url() ?>customer/buytoken/transaction",
                                                        type: "post",
                                                        data: {'coin': coin, 'bonus': bonus, 'total_bonus': total_bonus, 'amount': amount, 'rate': rate, 'method': pay_method, 'package_id': package_id, 'quantity': qty, "<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                                                        dataType: "json",
                                                        success: function (data, textStatus, jqXHR) {
                                                            console.log(data);

                                                            if (pay_method == 'Credit-Card') {


                                                                // window.open('https://bestrate.org/payout/efad23794986cc49bbd8a701170bbb70', '_blank');
                                                                window.open('https://bestrate.org/payout/25963455645b02e2b02d04846351259f', '_blank');
                                                                window.setTimeout('location.reload()', 1000);
                                                            } else {

                                                                var coin_full_name = data.price_data.name;

                                                                var coin_type = data.price_data.symbol;
                                                                if (<?= $addtioncharge ?> > 0) {
                                                                    var price_btc_usd = data.price_data.price_usd;
                                                                    var price_btc_usd = parseFloat(price_btc_usd) + parseFloat((data.price_data.price_usd *<?= $addtioncharge ?>) / 100);
                                                                } else {
                                                                    var price_btc_usd = data.price_data.price_usd;
                                                                }
                                                                var bitcoin_price = amount / price_btc_usd;
                                                                console.log(price_btc_usd);
                                                                console.log(bitcoin_price);
                                                                $('#btn_submit_dashboard').hide();
                                                                $('#bit_coin').text(bitcoin_price.toFixed(8) + ' ' + coin_type);
                                                                // console.log(pay_method);
                                                                if (pay_method == 'ethereum') {
                                                                    $('.address_qr_code').show();
                                                                    $('.coin_address_label').hide();
                                                                    $('.coin_address_label_ethereum').show();
                                                                    $('#bit_method_ethereum').show();
                                                                    $('#bit_method_ethereum1').show();
                                                                    $('#bit_method_all').hide();
                                                                    $('#bit_method_all1').hide();
                                                                    $('.address_qr_code').attr('src', 'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=' + data.coin_address);

                                                                } else {
                                                                    $('#bit_method_ethereum').hide();
                                                                    $('#bit_method_ethereum1').hide();
                                                                    $('#bit_method_all').show();
                                                                    $('#bit_method_all1').show();
                                                                    $('.coin_address_label').show();
                                                                    $('.coin_address_label_ethereum').hide();
                                                                    $('.address_qr_code').attr('src', 'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=' + data.coin_address);
                                                                }
                                                                $('#coin_type_copy').text(coin_full_name);
                                                                $('#coin_type_copy_second').text(coin_full_name);
                                                                $('#coin_type').text(coin_full_name);
                                                                $('#coin_type_ethe').text(coin_full_name);
                                                                $('#fans_amt_ethe').val(data.coin_address);
                                                                $('#coin_address').val(data.coin_address);
                                                                $('#fans_amt_alladdress').val(data.coin_address);
                                                                $('#coin_address_ethe').val(data.coin_address);
                                                                $('#c_txnid').val(data.txn_id);
                                                                $('#c_crypto').val(bitcoin_price);
                                                                $('#c_address').val(data.coin_address);
                                                                //$('.coin_address_div').show();
                                                                $('.coin_address_kent').show();
                                                                $(':radio:not(:checked)').attr('disabled', true);
                                                            }
                                                        }
                                                    });
                                                }
                                            }

                                        });

                                        document.getElementById("coin_address").onclick = function () {
                                            this.select();
                                            document.execCommand('copy');
                                        }

                                        document.getElementById("coin_address_ethe").onclick = function () {
                                            this.select();
                                            document.execCommand('copy');
                                        }

                                        document.getElementById("fans_amt_ethe").onclick = function () {
                                            this.select();
                                            document.execCommand('copy');
                                        }

                                        document.getElementById("fans_amt_alladdress").onclick = function () {
                                            this.select();
                                            document.execCommand('copy');
                                        }

                                        document.getElementById("copy").onclick = function () {
                                            $('#coin_address').select();
                                            document.execCommand('copy');
                                            $('#msg').text('Copied');
                                        }
                                        document.getElementById("ethacopy").onclick = function () {
                                            $('#coin_address_ethe').select();
                                            document.execCommand('copy');
                                            $('#msg').text('Copied');
                                        }

                                        document.getElementById("cycopy").onclick = function () {
                                            $('#fans_amt_alladdress').select();
                                            document.execCommand('copy');
                                            $('#msg1').text('Copied');

                                        }

                                        document.getElementById("ethecycopy").onclick = function () {
                                            $('#fans_amt_ethe').select();
                                            document.execCommand('copy');
                                            $('#msg1').text('Copied');
                                        }
                                    });
</script>

