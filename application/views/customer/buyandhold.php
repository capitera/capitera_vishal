<style>
    .btn-gradient-secondary {
        background-image: linear-gradient(to right, #4CAF50 0%, #8BC34A 51%, #4CAF50 100%);
    }

    .social_icons li{
        margin: 5px;
    }

    .ct-labels .ct-label{
        -webkit-transform: rotate(-35deg);
        -moz-transform: rotate(-35deg);
        -ms-transform: rotate(-35deg);
        -o-transform: rotate(-35deg);
        transform: rotate(-35deg);
    }
    #my_portfoliyo{
        margin-top: 25px;   
    }
    #buy_hold{
        margin-top: 25px; 
    }
    #my_itera{
        margin-top: 25px;  
    }
    #my_wallet{
        margin-top: 25px; 
    }

    .ct-label{
        font-size: 10px;
    }
    .ct-label {
        fill: rgba(255, 255, 255);
        color: rgb(255, 255, 255);
        font-size: 0.75rem;
        line-height: 1;
    }
    /* START: Custom css */
    ul.token-distribution-list li.power::before {
        background: #ec7c68;
    }

    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px; 
        max-width: 500px;
        margin: 1em auto;
    }

    #container {
        height: 400px;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    ul.token-distribution-list li.crowd-sale::before {
        background: #2372a8;
    }
    ul.token-distribution-list li.team::before {
        background: #3bc1e6;
    }
    ul.token-distribution-list li.advisors::before {
        background: #2c3384;
    }
    ul.token-distribution-list li.power::before {
        background: #c6fff8;
    }


    ul.token-distribution-list li.graph1::before {
        background: #092e38;
    }
    ul.token-distribution-list li.graph2::before {
        background: #1c8691;
    }
    ul.token-distribution-list li.graph3::before {
        background: #35ffb8;
    }

    ul.token-distribution-list li.graph4::before {
        background: #72b0b3;
    }
    .header_padding_css{
        padding: 14px 15px 14px 15px;
    }
    .card_title_css{
        padding-top: 5px  
    }
    .btn_padding{
        padding: 0px 14px 0px 0px;
        text-align: right;
    }
    .btn_stye_in_btn{
        padding: 7px 11px 7px 11px;
        width: auto;
    }
    .nav.nav-tabs .nav-item .nav-link.active {
        background-color: #f7f7f7 !important;
    }
    .nav-tabs .nav-link {
        padding: 0.5rem 4.7rem !important;
        border-top-left-radius: 0rem;
        border-top-right-radius: 0.75rem;
    }

    .nav-tabs .nav-item {
        margin-bottom: 0px !important;
    }

    .nav.nav-tabs .nav-item .nav-link.active {
        background-color: #fff !important;
        border-radius: 0rem 0.75rem 0 0 !important;
    }

    #tbl_transaction_crypto_wrapper{
        margin-top: 10px;
    }

    #tbl_transaction_fiat_wrapper{
        margin-top: 10px;
    }

    #tbl_transaction_etep_wrapper{
        margin-top: 10px;
    }

    #tbl_transaction_stocks_wrapper{
        margin-top: 10px;
    }

    /* END: Custom css */
</style>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">BUY & HOLD</h4>
            </div>
            <div class="card-content">
                <div class="card-body" style="padding-left: 0px; padding-top: 10px;">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" id="base-tab1" style="color: #000; background-color: #e7e7e7;  border-left-color: #ffffff;" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">CRYPTO</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab2" style="color: #000; background-color: #e7e7e7; margin-left: 5px; margin-right: 5px;" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">STOCKS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab3" style="color: #000; background-color: #e7e7e7; margin-right: 5px;" data-toggle="tab" aria-controls="tab3" href="#tab3" aria-expanded="false">ETFP</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab4" style="color: #000; background-color: #e7e7e7;" data-toggle="tab" aria-controls="tab4" href="#tab4" aria-expanded="false">FIAT</a>
                        </li>
                    </ul>
                    <div class="tab-content px-1 pt-1" style="padding-top: 0rem !important;">
                        <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                            <div class="row" style="margin-left:1px; margin-right: 3px;">
                                <div class="row table-responsive">
                                    <table class="table table-bordered table-striped text-left" id="tbl_transaction_crypto">
                                        <thead>
                                            <tr>
                                                <th>Name</th> 
                                                <th>Amount</th>             
                                                <th>Percent</th>
                                                <th>Value, $</th>
                                                <th>Empty</th>
                                                <th>Empty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($buy_hold) && !empty($buy_hold)) {
                                                foreach ($buy_hold as $val) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <p class="m-0"><?= $val->symbol ?></p>
                                                            <small><?= $val->currency_name ?></small>
                                                        </td>
                                                        <td class="text-right"><?= $val->total_balance ?></td>                            
                                                        <td class="text-right">32%</td>
                                                        <td class="text-right"><?= $val->total_balance ?></td>
                                                        <td class="text-right">n/a</td>
                                                        <td></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>                                    
                            </div> 
                        </div>
                        <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                            <div class="row" style="margin-left:1px; margin-right: 3px;">
                                <div class="row table-responsive">
                                    <table class="table table-bordered table-striped text-left" id="tbl_transaction_stocks">
                                        <thead>
                                            <tr>
                                                <th>Name</th> 
                                                <th>Amount</th>             
                                                <th>Percent</th>
                                                <th>Value, $</th>
                                                <th>Empty</th>
                                                <th>Empty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($buy_hold) && !empty($buy_hold)) {
                                                foreach ($buy_hold as $val) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <p class="m-0"><?= $val->symbol ?></p>
                                                            <small><?= $val->currency_name ?></small>
                                                        </td>
                                                        <td class="text-right"><?= $val->total_balance ?></td>                            
                                                        <td class="text-right">32%</td>
                                                        <td class="text-right"><?= $val->total_balance ?></td>
                                                        <td class="text-right">n/a</td>
                                                        <td></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>                                    
                            </div> 
                        </div>
                        <div class="tab-pane" id="tab3" aria-labelledby="base-tab3">
                            <div class="row" style="margin-left:1px; margin-right: 3px;">
                                <div class="row table-responsive">
                                    <table class="table table-bordered table-striped text-left" id="tbl_transaction_etep">
                                        <thead>
                                            <tr>
                                                <th>Name</th> 
                                                <th>Amount</th>             
                                                <th>Percent</th>
                                                <th>Value, $</th>
                                                <th>Empty</th>
                                                <th>Empty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($buy_hold) && !empty($buy_hold)) {
                                                foreach ($buy_hold as $val) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <p class="m-0"><?= $val->symbol ?></p>
                                                            <small><?= $val->currency_name ?></small>
                                                        </td>
                                                        <td class="text-right"><?= $val->total_balance ?></td>                            
                                                        <td class="text-right">32%</td>
                                                        <td class="text-right"><?= $val->total_balance ?></td>
                                                        <td class="text-right">n/a</td>
                                                        <td></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>                                    
                            </div> 
                        </div>
                        <div class="tab-pane" id="tab4" aria-labelledby="base-tab4">
                            <div class="row" style="margin-left:1px; margin-right: 3px;">
                                <div class="row table-responsive">
                                    <table class="table table-bordered table-striped text-left" id="tbl_transaction_fiat">
                                        <thead>
                                            <tr>
                                                <th>Name</th> 
                                                <th>Amount</th>             
                                                <th>Percent</th>
                                                <th>Value, $</th>
                                                <th>Empty</th>
                                                <th>Empty</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($buy_hold) && !empty($buy_hold)) {
                                                foreach ($buy_hold as $val) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <p class="m-0"><?= $val->symbol ?></p>
                                                            <small><?= $val->currency_name ?></small>
                                                        </td>
                                                        <td class="text-right"><?= $val->total_balance ?></td>                            
                                                        <td class="text-right">32%</td>
                                                        <td class="text-right"><?= $val->total_balance ?></td>
                                                        <td class="text-right">n/a</td>
                                                        <td></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>                                    
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $("#tbl_transaction_crypto").dataTable({
            "ordering": true,
        });

        $("#tbl_transaction_fiat").dataTable({
            "ordering": true,
        });

        $("#tbl_transaction_etep").dataTable({
            "ordering": true,
        });

        $("#tbl_transaction_stocks").dataTable({
            "ordering": true,
        });

    });
</script>

