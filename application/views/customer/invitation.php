<style>
    .iframe-container {    
        padding-bottom: 60%;
        padding-top: 30px; height: 0; overflow: hidden;
    }

    .iframe-container iframe,
    .iframe-container object,
    .iframe-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .panel-light-primary {
        border: 1px solid #403f7b !important;
    }

    .modal-dialog {
        max-width: 800px !important;
    }
</style>
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header col-md-8 col-12 mb-2 breadcrumb-new">
            <h3 class="content-header-title mb-0 d-inline-block">Invite Friend</h3>
            <div class="row breadcrumbs-top d-inline-block">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>customer/dashboard">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active"><a href="<?= base_url() ?>customer/downlink/invitation">Invite Friend</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Dashboard Details -->
<!-- Purchase token -->
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card pull-up">
            <div class="card-content collapse show">
                <div class="card-body">
                    <h4 class="mb-2 mt-1 text-center">Invite Friend by Email</h4>
                    <?= ($this->session->flashdata('msg')) ? $this->session->flashdata('msg') : ''; ?>
                    
                    <form class="form-horizontal form-purchase-token row" id="frm_register" name="frm_register" method="post" action="<?= base_url() ?>customer/downlink/send_invitation">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <div class="col-12 col-md-8 col-xs-12 col-sm-12 offset-md-2">
                            <fieldset class="form-group">
                                <label for="your-email">Your Email</label>
                                <input type="text" class="form-control" id="your-email" name="your-email" readonly disabled="disabled" value="<?= $this->common->getCustEmail($this->session->userdata('cid')) ?>">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="friend_email">Friend's Email</label>
                                <input type="text" class="form-control" id="friend_email" name="friend_email" placeholder="Enter Your Friend's Email" required="" autofocus="">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="subject">Message</label>
                                <textarea class="form-control" name="message" id="message" rows="5"><?= ($referralcode) ? base_url().'referralcode/'.$referralcode : ''?></textarea>
                            </fieldset>
                            <fieldset class="form-group text-center">
                                <button type="submit" class="btn-gradient-primary" id="btn_send_invite">Send</button>
                            </fieldset>
                        </div> 
                    </form> 
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<?php
$msg = $this->input->get('msg');
switch ($msg) {
    case "U":
        $m = "Update Successfully...!!!";
        $t = "success";
        break;
    case "A":
        $m = "Email or Phone alredy exist!!!";
        $t = "error";
        break;
    case "E":
        $m = "Something went wrong, Please try again!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>
<!-- start: JavaScript Event Handlers for this page -->

<script type="text/javascript">
    
    $(document).ready(function () {

<?php if ($msg): ?>
            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>
        
        $("#btn_send_invite").on("click", function () {
            
            var email_patten = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var email = $("#friend_email").val().trim();

            if ($("#friend_email").val().trim() == "")
            {
                alertify.error("Please Enter Friend Email!!!");
                $("#friend_email").focus();   
                return false;

            } else if ($("#subject").val() == "") {
                
                alertify.error("Please Enter Subject!!!");
                $("#subject").focus();
                return false;

            } else if ($("#message").val().trim() == "") {
                alertify.error("Please Enter Message!!!");
                $("#message").focus();
                return false;
            } else {
                return true; //submit form
            }
            return false; //Prevent form to submitting
        });
    });
</script>
<!-- end: JavaScript Event Handlers for this page -->
<!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
<!--/ Purchase token -->