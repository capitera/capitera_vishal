<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Loign History</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
            </div>
            <div class="card-content">
                <div class="row" style="margin-left:0px; margin-right: 0px;">
                    <div class="col-md-12 table-responsive pl-0 pr-0">
                        <table id="tbl_transaction" class="table table-hover table-xl mb-0 table-bordered table-striped text-left">
                            <thead>
                                <tr>
                                    <th class="border-top-0 text-center">Date&Time</th>
                                    <th class="border-top-0 text-center">Browser</th>
                                    <th class="border-top-0 text-center">IP Address</th>
                                </tr>
                            </thead>
                            <tbody class="text-left">
                                <?php
                                if (!empty($loginhistory)) {
                                    foreach ($loginhistory as $val) {
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= date('m-d-Y h:i A', strtotime($val->reg_date)) ?></td>
                                            <td class="text-truncate"><?= $val->browser ?></td>                       
                                            <td class="text-right"><?= $val->ip_address ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>                                    
                </div>         
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tbl_transaction").dataTable({
            "ordering": true
        });
    });
</script>



