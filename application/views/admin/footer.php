</div>
</div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->


<footer class="footer footer-static footer-transparent">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2019 <a class="text-bold-800 grey darken-2" href="#" target="_blank">CAPITERA </a>, All rights reserved. </span></p>
</footer>



<script type="text/javascript">
    $(function () {

        $('.mycheck').prop('disabled', true);
        $("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)").iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_minimal'
        });
        checkbox();
        function checkbox() {
            $('table thead :checkbox').on('ifChecked ifUnchecked', function (event) {
                if (event.type == 'ifChecked') {
                    $('.icheckbox_minimal').iCheck('check');
                    $('.mycheck').removeAttr('disabled');
                } else {
                    $('.icheckbox_minimal').iCheck('uncheck');
                    $('.mycheck').prop('disabled', true);
                }
            });
            $('table tbody :checkbox').on('ifChanged', function (event) {
                var len = parseInt($('table tbody :checkbox').filter(':checked').length);
                if ($('table tbody :checkbox').filter(':checked').length == $('table tbody :checkbox').length) {
                    $('table thead :checkbox').prop('checked', true);
                } else {
                    $('table thead :checkbox').prop('checked', false);
                    $('.mycheck').removeAttr('disabled');
                }
                if (len > 0) {
                    $('.mycheck').removeAttr('disabled');
                } else {
                    $('.mycheck').prop('disabled', true);
                }
                $('table thead :checkbox').iCheck('update');
            });
        }
        //-----------------------------iCheck All-----------------------------//

        $('ul.pagination').on('click', function () {
            $('.icheckbox_minimal').iCheck('uncheck');
            $('.mycheck').prop('disabled', true);
            $("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)").iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal'
            });
            checkbox();
        });
    });
</script>



<script src="<?= base_url() ?>/newassets/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?= base_url() ?>/newassets/app-assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>/newassets/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>/newassets/app-assets/vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<script src="<?= base_url() ?>assets/vendor/DataTables/jquery.dataTables.min.js"></script>
<!-- BEGIN MODERN JS-->
<script src="<?= base_url() ?>assets/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

<script src="<?= base_url() ?>/newassets/app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="<?= base_url() ?>/newassets/app-assets/js/core/app.js" type="text/javascript"></script>
<script src="<?= base_url() ?>/newassets/app-assets/js/scripts/customizer.js" type="text/javascript"></script>
<!-- END MODERN JS-->
<script src="<?= base_url() ?>assets/alertify/alertify.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?= base_url() ?>/newassets/app-assets/js/scripts/pages/dashboard-ico.js" type="text/javascript"></script>
<script src="<?= base_url() ?>/newassets/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
</body>
</html>