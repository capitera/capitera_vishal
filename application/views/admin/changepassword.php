<div class="row justify-content-center">
    <div class="col-md-5">
        <div class="form-box">
            <div class="form-heading">
                <h4 class="h3 text-uppercase">Change Password</h4>
            </div>
            <div class="form-body">
                <form class="form-horizontal form-user-profile mt-2" method="post" id="customerForm">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <fieldset class="form-group">
                        <label for="user-password">Old Password</label><label style="float: right; padding-right: 2px;"><span id="erroropassword" style="color:red; font-weight: 500;"></span></label>
                        <input  name="opassword" id="user-password" value="" type="password" class="form-control"  placeholder="Old Password" required="">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="full-name">New Password</label><label style="float: right; padding-right: 2px;"><span id="errornpassword" style="color:red; font-weight: 500;"></span></label>
                        <input  name="npassword" id="full-name" value="" type="password" class="form-control"  placeholder="New Password" required="">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="user-cnf-password">Confirm Password</label><label style="float: right; padding-right: 2px;"><span id="errorcpassword" style="color:red; font-weight: 500;"></span></label>
                        <input  name="cpassword" id="user-cnf-password" value="" type="password" class="form-control"  placeholder="Confirm Password" required="">
                    </fieldset>
                    <div class="text-center">
                        <input type="button" name="changepassword" id="changepassword" class="btn-secondary btn-gradient-css btn-sm" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php
$msg = $this->input->get("msg");
switch ($msg) {
    case "S":
        $m = "Update Password Successfully...!!!";
        $t = "success";
        break;
    case "E":
        $m = "Something went wrong, Please try again!!!";
        $t = "error";
        break;
    case "NM":
        $m = "Old password not match!, Please try again!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>
<!-- start: JavaScript Event Handlers for this page -->

<script type="text/javascript">
    $(document).ready(function () {
<?php if ($msg): ?>
            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>
        $('#changepassword').on('click', function () {
            if ($('#user-password').val() == '')
            {
                $("#erroropassword").text("Enter old passowrd").fadeIn('slow').fadeOut(5000);
            } else if ($('#full-name').val() != '')
            {

                if ($('#user-cnf-password').val() == $('#full-name').val())
                {
                    $url = "<?= site_url() ?>admin/changepassword/updatePassword";
                    $('#customerForm').attr('action', $url);
                    $('#customerForm').submit();
                } else
                {
                    $("#errorcpassword").text("New password amd confirm password not match!").fadeIn('slow').fadeOut(5000);
                }
            } else
            {
                $("#errornpassword").text("Enter new password").fadeIn('slow').fadeOut(5000);
            }
        });
    });
</script>
<!-- end: JavaScript Event Handlers for this page -->
<!-- end: CLIP-TWO JAVASCRIPTS -->
<!--/ Purchase token -->


