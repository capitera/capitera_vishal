<style>
    table{
        text-align: center;
        color: #000;
        /*font-size: 12px;*/
        white-space: nowrap;
    }
</style>
<link href="<?= base_url() ?>assets/vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<div class="row package_frm display-hidden" style="margin-bottom: 10px;">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="form-box">
            <div class="form-heading">
                <h4 class="h3 text-uppercase">Token Package</h4>
            </div>
            <div class="form-body">                  
                <form name="packageForm" method="POST" id="packageForm" action="">  
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <input type="hidden" name="package_id" id="package_id">
                    <div class="form-group">
                        <input type="text" class="form-control" name="package_name" id="package_name" placeholder="Package Name" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="price" id="price" placeholder="Price in USD" required onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)'>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="no_of_token" id="no_of_token" placeholder="No. of Token" required onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 46)'>
                    </div>
                    <div class="form-group">
                        <input type="file" class="form-control" name="package_img" id="package_img">
                        <img src="" class="p_img" style="width: 100px;height: 100px;">
                    </div>
                    <div class="form-group" style="text-align: center;">
                        <button type="button" class="btn btn-secondary btn-gradient-css btn-sm" id="updatePackage">
                            Save 
                        </button>
                    </div>
                    <br>
                </form>                    
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header" >
                <h4 class="card-title">Token Package</h4>
                <?php if ($this->session->flashdata('bns') != '') { ?>
                    <div class="alert alert-success">
                        <?= $this->session->flashdata('bns'); ?>
                    </div>
                <?php } ?>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content">
                <div class="row" style="margin-left:0px; margin-right:0px;">
                    <div class="col-md-12 table-responsive pr-0 pl-0">
                        <table id="tbl_transaction" class="table table-bordered table-striped text-left">
                            <thead>
                                <tr>
                                    <th>Package Name</th> 
                                    <th>Price in USD</th> 
                                    <th>No of Token</th> 
                                    <th>Image</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-left">
                                <?php
                                if (isset($packages) && !empty($packages)) {
                                    foreach ($packages as $val) {
                                        ?>
                                        <tr>
                                            <td><?= $val->package_name; ?></td>
                                            <td class="text-right"><?= '$ ' . $val->price ?></td>                                                                    
                                            <td class="text-right"><?= $val->no_of_token ?> Capitera Token</td>
                                            <td><img style="width: 100px;height: 100px;" src="<?= base_url() ?>assets/package/<?= $val->image ?>"></td>
                                            <td>
                                                <a href="#" id_rate="<?= $val->package_id ?>" value="<?= $val->package_id ?>" class="mb-0 btn-sm btn btn-outline-primary round update">
                                                    <i class="fa fa-pencil-square-o"></i>Edit
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>                                    
                </div> 
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url() ?>assets/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/select2/select2.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/selectFx/selectFx.js"></script>
<?php
$msg = $this->input->get('msg');
?>
<script>

                            $(document).ready(function () {
                                $("#tbl_transaction").dataTable({
                                    "ordering": true,
                                });

                                $('.update').on('click', function () {
                                    $('.package_frm').removeClass('display-hidden');
                                    var pid = $(this).attr('value');
                                    $.ajax({
                                        type: "POST",
                                        url: "<?= base_url() ?>admin/token_package/getPackage",
                                        data: {'package_id': pid, "<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                                        dataType: "Json",
                                        success: function (data) {
                                            if (data != 0)
                                            {
                                                $('#package_id').val(pid);
                                                $('#package_name').val(data.package_name);
                                                $('#price').val(data.price);
                                                $('#no_of_token').val(data.no_of_token);
                                                $('.p_img').attr('src', '<?= base_url() ?>assets/package/' + data.image);
                                            }
                                        }
                                    });
                                });

                                $('#updatePackage').on('click', function () {


                                    if ($('#package_name').val().trim() == "") {
                                        alertify.error('Please Enter Package Name');
                                        return false;
                                    } else if ($('#price').val().trim() == "") {
                                        alertify.error('Please Enter Price in USD');
                                        return false;
                                    } else if ($('#no_of_token').val().trim() == "") {
                                        alertify.error('Please Enter No of Token');
                                        return false;
                                    } else {
                                        var file_data = $('#package_img').prop('files')[0];
                                        var p_id = $('#package_id').val();
                                        var p_name = $('#package_name').val();
                                        var p_price = $('#price').val();
                                        var no_token = $('#no_of_token').val();
                                        var form_data = new FormData();
                                        form_data.append('file', file_data);
                                        form_data.append('package_id', p_id);
                                        form_data.append('p_name', p_name);
                                        form_data.append('p_price', p_price);
                                        form_data.append('no_token', no_token);
                                        form_data.append("<?= $this->security->get_csrf_token_name(); ?>", "<?= $this->security->get_csrf_hash(); ?>");

                                        $.ajax({
                                            url: "<?= base_url() ?>admin/token_package/editPackage",
                                            dataType: "Json",
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            data: form_data,
                                            type: "POST",
                                            // data: {'package_id': p_id, 'p_name': p_name, 'p_price': p_price, 'no_token': no_token },
                                            success: function (data) {
                                                // console.log(data);
                                                if (data != '0') {
                                                    alertify.success('Package updated successfully!');
                                                    window.setTimeout('location.reload()', 2000);
                                                } else
                                                {
                                                    alertify.error('Something went wrong!');
                                                    window.setTimeout('location.reload()', 2000);
                                                }
                                            }
                                        });
                                    }
                                });

                                $('#update_coinrate').click(function () {
                                    if ($("#ico_round_name").val().trim() == "")
                                    {
                                        alertify.error('Please Enter Name');
                                        return false;
                                    } else if ($("#total_token").val().trim() == "") {
                                        alertify.error('Please Enter Token');
                                        return false;
                                    } else if ($("#round_rate").val() == "") {
                                        alertify.error('Please Enter Rate');
                                        return false;
                                    } else if ($("#bonus").val() == "") {
                                        alertify.error('Please Enter Bonus');
                                        return false;
                                    } else if ($("#start_date").val() == "") {
                                        alertify.error('Please Enter Start Date');
                                        return false;
                                    } else if ($("#end_date").val() == "") {
                                        alertify.error('Please Enter End Date');
                                        return false;
                                    } else {
                                        var ico_round_id = $("#ico_round_id").val();
                                        var ico_round_name = $("#ico_round_name").val();
                                        var total_token = $("#total_token").val();
                                        var round_rate = $("#round_rate").val();
                                        var bonus = $("#bonus").val();
                                        var start_date = $("#start_date").val();
                                        var end_date = $("#end_date").val();
                                        $.ajax({
                                            type: "POST",
                                            url: "<?= base_url() ?>admin/icoround/editIcoRound",
                                            data: {'ico_round_id': ico_round_id, 'ico_round_name': ico_round_name, 'total_token': total_token, 'round_rate': round_rate, 'bonus': bonus, 'start_date': start_date, 'end_date': end_date, "<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                                            dataType: "Json",
                                            success: function (data) {
                                                console.log(data);
                                                if (data == 1)
                                                {
                                                    alertify.success('Data updated successfully!');
                                                    window.setTimeout('location.reload()', 1000);
                                                } else
                                                {
                                                    alertify.error('something is wrong');
                                                    window.setTimeout('location.reload()', 1000);
                                                }

                                            }
                                        });
                                    }
                                });
                            });
</script>





