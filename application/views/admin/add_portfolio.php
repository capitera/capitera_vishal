<style>
    .iframe-container {    
        padding-bottom: 60%;
        padding-top: 30px; height: 0; overflow: hidden;
    }

    .iframe-container iframe,
    .iframe-container object,
    .iframe-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .panel-light-primary {
        border: 1px solid #403f7b !important;
    }

    .modal-dialog {
        max-width: 800px !important;
    }
</style>
<link href="<?= base_url() ?>assets/vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<!-- Purchase token -->
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="form-box">
            <div class="form-heading">
                <h4 class="h3 text-uppercase">Add New Portfolio</h4>
            </div>
            <div class="form-body">
                <?= ($this->session->flashdata('msg')) ? $this->session->flashdata('msg') : ''; ?>

                <form class="form-horizontal form-purchase-token row" enctype="multipart/form-data" id="frm_portfolio" name="frm_portfolio" method="post" action="<?= isset($edit_portfolio) ? base_url() . 'admin/portfolios/updatePortfolio' : base_url() . 'admin/portfolios/addNewPortfolios' ?>">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="col-12 col-md-8 col-xs-12 col-sm-12 offset-md-2">
                        <input type="hidden" name="portfolio_id" value="<?= isset($edit_portfolio) ? $edit_portfolio->portfolio_id : '' ?>">
                        <fieldset class="form-group">
                            <label for="name">Name :</label><label style="float: right; padding-right: 2px;"><span id="errorname" style="color:red; font-weight: 500;"></span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?= isset($edit_portfolio) ? $edit_portfolio->name : '' ?>" placeholder="Enter Name">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="manager">Manager :</label><label style="float: right; padding-right: 2px;"><span id="errormanager" style="color:red; font-weight: 500;"></span></label>
                            <input type="text" class="form-control" id="manager" value="<?= isset($edit_portfolio) ? $edit_portfolio->manager : '' ?>" name="manager" placeholder="Enter Manager">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="risk_level">Risk Level :</label><label style="float: right; padding-right: 2px;"><span id="errorrisk_level" style="color:red; font-weight: 500;"></span></label>
                            <select class="form-control" name="risk_level" id="risk_level">
                                <option value="high" <?= isset($edit_portfolio) ? ($edit_portfolio->risk_level == 'high') ? 'selected' : '' : '' ?>>High</option>
                                <option value="low" <?= isset($edit_portfolio) ? ($edit_portfolio->risk_level == 'low') ? 'selected' : '' : '' ?>>Low</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="assets">Base(Assets) :</label><label style="float: right; padding-right: 2px;"><span id="errorassets" style="color:red; font-weight: 500;"></span></label>
                            <select class="form-control" name="assets" id="assets">
                                <?php
                                if (isset($assets) && !empty($assets)) {
                                    foreach ($assets as $val) {
                                        ?>
                                        <option value="<?= $val->currency_wallet_id ?>" <?= isset($edit_portfolio) ? ($val->currency_wallet_id == $edit_portfolio->assets_base) ? "selected" : "" : '' ?>><?= $val->currency_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                        </fieldset>
                        <fieldset class="form-group">
                            <label>Time Limit :</label><label style="float: right; padding-right: 2px;"><span id="errortimelimit" style="color:red; font-weight: 500;"></span></label>
                            <div class="input-group input-daterange datepicker">
                                <input type="text" name="start_date" id="start_date" placeholder="Start Date" class="form-control" value="<?= isset($edit_portfolio) ? date('d/m/Y', strtotime($edit_portfolio->start_date)) : '' ?>" />
                                <span class="input-group-addon bg-primary">to</span>
                                <input type="text" name="end_date" id="end_date" placeholder="End Date" class="form-control" value="<?= isset($edit_portfolio) ? date('d/m/Y', strtotime($edit_portfolio->end_date)) : '' ?>" />
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="amount_invest">Amount :</label><label style="float: right; padding-right: 2px;"><span id="erroramount_invest" style="color:red; font-weight: 500;"></span></label>
                            <input type="text" class="form-control" id="amount_invest" name="amount_invest" value="<?= isset($edit_portfolio) ? $edit_portfolio->amount_invest : '' ?>" placeholder="Enter Amount">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="portfolio_image">Icon :</label><label style="float: right; padding-right: 2px;"><span id="errorportfolio_image" style="color:red; font-weight: 500;"></span></label>
                            <input type="file" class="form-control" id="portfolio_image" name="portfolio_image">
                            <?php if (isset($edit_portfolio)) { ?>
                                <img src="<?= base_url() ?>uploads/portfolio_image/<?= $edit_portfolio->portfolio_image ?>"/>
                            <?php } ?>
                        </fieldset>
                        <fieldset class="form-group text-center">
                            <button type="submit" class="btn-secondary btn-gradient-css" id="btn_portfolio">Save</button>
                        </fieldset>
                    </div> 
                </form> 
            </div>
        </div>
    </div>
</div>
</div>
<script src="<?= base_url() ?>assets/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/select2/select2.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/selectFx/selectFx.js"></script> 
<?php
$msg = $this->input->get('msg');
switch ($msg) {
    case "U":
        $m = "Update Successfully...!!!";
        $t = "success";
        break;
    case "A":
        $m = "Email or Phone alredy exist!!!";
        $t = "error";
        break;
    case "E":
        $m = "Something went wrong, Please try again!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>
<!-- start: JavaScript Event Handlers for this page -->
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>
<script type="text/javascript">

    $(document).ready(function () {


<?php if ($msg): ?>
            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>
        $('.datepicker').datepicker({
            autoclose: true
        });
        $("#btn_portfolio").on("click", function () {
            if ($("#name").val().trim() == "")
            {
                $("#errorname").text("Please Enter Name").fadeIn('slow').fadeOut(5000);
                return false;
            } else if ($("#manager").val() == "") {
                $("#errormanager").text("Please Enter Manager").fadeIn('slow').fadeOut(5000);
                return false;
            } else if ($("#risk_level").val() == "") {
                $("#errorrisk_level").text("Please Select Risk Level").fadeIn('slow').fadeOut(5000);
                return false;
            } else if ($("#assets").val() == "") {
                $("#errorassets").text("Please Enter Assets").fadeIn('slow').fadeOut(5000);
                return false;
            } else if ($("#start_date").val() == "") {
                $("#errortimelimit").text("Please Enter Start Date").fadeIn('slow').fadeOut(5000);
                return false;
            } else if ($("#end_date").val() == "") {
                $("#errortimelimit").text("Please Enter End Date").fadeIn('slow').fadeOut(5000);
                return false;
            } else if ($("#amount_invest").val() == "") {
                $("#erroramount_invest").text("Please Enter Amount").fadeIn('slow').fadeOut(5000);
                return false;
<?php if (!isset($edit_portfolio)) { ?>
                } else if ($("#portfolio_image").val() == "") {
                    $("#errorportfolio_image").text("Please Select Image").fadeIn('slow').fadeOut(5000);
                    return false;
<?php } ?>
            } else {
                return true; //submit form
            }
            return false; //Prevent form to submitting
        });
    });
</script>
<!-- end: JavaScript Event Handlers for this page -->
<!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
<!--/ Purchase token -->