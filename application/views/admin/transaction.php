</div>
<style>
    table{
        text-align: center;
        color: #000;
        font-size: 15px;
        white-space: nowrap;
    }
</style>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">TRANSACTIONS</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content">
                <div class="row" style="margin-left:0px; margin-right:0px;">
                    <div class="col-md-12 table-responsive pr-0 pl-0">
                        <table class="table table-bordered table-striped text-left" id="tbl_transaction">
                            <thead>
                                <tr>

                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Method</th>
                                    <th>USD</th>
                                    <th>Quantity</th>
                                    <th>Package</th>
                                    <th>Token</th>
                                    <th>Frozen Token</th>
                                    <th>Available Token</th>
                                    <th>Remaining Day</th>
                                    <th>Verify</th>
                                    <th>Browser</th>
                                    <th>IP Address</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($transaction)) {
                                    foreach ($transaction as $tr) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?= $tr->user_name ?>
                                            </td>
                                            <td>
                                                <p><?= date('m-d-Y h:i A', strtotime($tr->transaction_date)) ?></p>
                                            </td>
                                            <td>
                                                <?php if ($tr->method == 'bitcoin') { ?>
                                                    <img src="<?= base_url(); ?>assets/images/bitcoin.svg" height="40" width="40"/>
                                                <?php } elseif ($tr->method == 'ethereum') { ?>
                                                    <img src="<?= base_url(); ?>assets/images/ethereum.svg" height="40" width="40"/>
                                                <?php } elseif ($tr->method == 'litecoin') { ?>
                                                    <img src="<?= base_url(); ?>assets/images/litecoin.svg" height="40" width="40"/>
                                                <?php } elseif ($tr->method == 'XRP') { ?>   
                                                    <img src="<?= base_url(); ?>assets/images/XRP.png" height="40" width="40"/>
                                                <?php } elseif ($tr->method == 'Dash') { ?>  
                                                    <img src="<?= base_url(); ?>assets/images/dash.png" height="40" width="40"/>
                                                <?php } elseif ($tr->method == 'paypal') { ?>    
                                                    <img src="<?= base_url(); ?>assets/images/paypal.png" height="40" width="40"/>
                                                <?php } elseif ($tr->method == 'offline') { ?>   
                                                    <img src="<?= base_url(); ?>assets/images/offline.png" height="40" width="40"/>
                                                <?php } elseif ($tr->method == 'Credit-Card') { ?>   
                                                    <img src="<?= base_url(); ?>assets/images/bestrate.png" height="40" width="40"/>
                                                <?php } ?>
                                            </td>
                                            <!--<td><?= $tr->transaction_id ?></td>-->
                                            <td class="text-right">$<?= $tr->amount ?></td>
                                            <td class="text-right"><?= $tr->quantity ?></td>
                                            <td><?= $tr->package_name ?></td>
                                            <td class="text-right"><?= $tr->coins ?></td>
                                            <td class="text-right"><?= $tr->remaining_coin ?></td>
                                            <td class="text-right"><?= number_format($tr->coins - $tr->remaining_coin, 2) ?></td>
                                            <td class="text-right"><?= $tr->frozen_days ?></td>
                                            <td><?php if ($tr->method == 'bitcoin') { ?> <a target="_blank" href="https://blockchain.info/address/<?= $tr->address ?>"><i class="ficon ft-search" aria-hidden="true"></i></a> <?php } elseif ($tr->method == 'ethereum') { ?><a target="_blank" href="https://etherscan.io/address/<?= $tr->address ?>"><i class="ficon ft-search" aria-hidden="true"></i></a> <?php } elseif ($tr->method == 'litecoin') { ?> <a target="_blank" href="http://explorer.litecoin.net/address/<?= $tr->address ?>"><i class="ficon ft-search" aria-hidden="true"></i></a><?php } elseif ($tr->method == 'Dash') { ?> <a target="_blank" href="https://explorer.dash.org/address/<?= $tr->address ?>"><i class="ficon ft-search" aria-hidden="true"></i></a> <?php } elseif ($tr->method == 'XRP') { ?><a target="_blank" href="https://xrpcharts.ripple.com/#/graph/<?= $tr->address ?>"><i class="ficon ft-search" aria-hidden="true"></i></a> <?php } ?></td>
                                            <td><?= $tr->browser ?></td>
                                            <td><?= $tr->ip_address ?></td>
                                            <td>
                                                <?php if ($tr->status == 1) { ?>
                                                    <label class="mb-0 btn-sm btn btn-outline-success round" >Paid</label>
                                                <?php } elseif ($tr->status == 2) { ?>
                                                    <label class="mb-0 btn-sm btn btn-outline-danger round" >Reject</label>
                                                <?php } else { ?>
                                                    <label class="mb-0 btn-sm btn btn-outline-warning round" >Pending</label>
                                                    <?php
                                                }
                                                ?>
                                            </td>

                                            <td>
                                                <div class="btn-group">
                                                    <?php if ($tr->status == 1) { ?>
                                                        <?php if ($tr->wallet_status == 0) { ?>
                                                                                                            <!-- <a href="<?= base_url() ?>admin/transaction/sendToken/<?= $tr->txn_id ?>" class="mb-0 btn-sm btn btn-outline-success">SendToken</a> -->
                                                            <?php
                                                        }
                                                    } elseif ($tr->status == 2) {
                                                        ?>
                                                        <!--<label class="btn btn-danger btn-sm" >Reject</label>-->
                                                    <?php } else { ?>
                                                        <button type="button" id="btn_submit_approve" name="btn_submit_approve" value="<?= $tr->txn_id ?>" class="mb-0 btn-sm btn btn-outline-success round btn_submit_approve" style="padding: 5px; margin-bottom: 5px; border-radius: 4px; margin-right: 5px;"> Approve </button>
                                                        <!--<a href="<?= base_url() ?>admin/transaction/approveTransaction/<?= $tr->txn_id ?>">Approve</a>-->
                                                        <button type="button" id="btn_submit_reject" name="btn_submit_reject" value="<?= $tr->txn_id ?>" class="mb-0 btn-sm btn btn-outline-danger round btn_submit_reject" style="border-radius: 4px;"> Reject </button>
                                                       <!-- <a href="<?= base_url() ?>admin/transaction/rejectTransaction/<?= $tr->txn_id ?>" style="color:red">Reject</a>-->
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>                                    
                </div>         
            </div>
        </div>
    </div>
</div>


<?php
$msg = $this->input->get("msg");
switch ($msg) {
    case "S":
        $m = "Successfully...!!!";
        $t = "success";
        break;
    case "D":
        $m = "Successfully Delete Record...!!!";
        $t = "success";
        break;
    case "E":
        $m = "Something went wrong, Please try again!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>
<script>

    $(document).ready(function () {
<?php if ($msg): ?>
            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>

        $("#tbl_transaction").dataTable({
            "ordering": true,
        });

        $("#tbl_referral_transaction").dataTable({
            "ordering": false,
        });

        $("#tbl_withdraw").dataTable({
            "ordering": false,
        });

        $("#tbl_issue_token").dataTable({
            "ordering": false,
        });


        $('#select_all').click(function () {
            if (this.checked)
            {
                $('.grid_checkbox').each(function () {
                    this.checked = true;
                });
            } else
            {
                $('.grid_checkbox').each(function () {
                    this.checked = false;
                });
            }
        });

        $('.delete').on('click', function () {
            if (confirm('Are you sure delete selected record?')) {
                $url = '<?= site_url() ?>admin/transaction/allSelTrasDelete';
                $('#customerForm').attr('action', $url);
                $('#customerForm').submit();
            } else {
                return false;
            }

        });

        $('#category').change(function () {
            $id = $(this).val();
            $.ajax({
                url: "<?= site_url() ?>dashboard/getApplist",
                type: "POST",
                data: {'cat_id': $id, "<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                success: function (data) {

                    $('#appname').html(data);
                }

            });
        });

        $('#tbl_transaction').on('click', '.btn_submit_approve', function () {
            var cid = $(this).val();
            $.ajax({
                url: "<?= base_url() ?>admin/transaction/approveTransaction/" + cid,
                type: "POST",
                data: {"<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                success: function (data) {
                    if (data = 'success') {

                        alertify.success('Successfully Approval Record...!!!');
                        window.setTimeout('location.reload()', 3000);
                    } else {
                        alertify.error('Something went wrong, Please try again!!!');
                    }
                }

            });
        });

        $('#tbl_transaction').on('click', '.btn_submit_reject', function () {
            var cid = $(this).val();
            $.ajax({
                url: "<?= base_url() ?>admin/transaction/rejectTransaction/" + cid,
                type: "POST",
                data: {"<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                success: function (data) {
                    if (data = 'success') {

                        alertify.success('Successfully Reject Record...!!!');
                        window.setTimeout('location.reload()', 3000);
                    } else {
                        alertify.error('Something went wrong, Please try again!!!');
                    }
                }

            });
        });

        if ('<?= $msg ?>' != '')
        {
            if ('<?= $msg ?>' == 'success')
            {
                alertify.success('Record Deleted Successfully');
            }
        }

    });

</script>
