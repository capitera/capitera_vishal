<style>
    #tbl_user_wrapper{
        margin-top: 10px; 
    }
</style>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">USER</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content">
                <div class="row" style="margin-left:0px; margin-right: 0px;">
                    <div class="col-md-12 table-responsive pl-0 pr-0">
                        <table class="table table-bordered table-striped" id="tbl_user">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Date</th>
                                    <th>Country</th>
                                    <th>Status</th>
                                    <th>Verify</th>
                                    <th>Action</th>                                             
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($user)) {
                                    foreach ($user as $val) {
                                        ?>
                                        <tr class="">
                                            <td><?= $val->fullname ?></td>
                                            <td><?= $val->email ?></td>
                                            <td class="text-right"><?= date('m-d-Y', strtotime($val->register_date)) ?></td>

                                            <td><?= $val->country ?></td>
                                            <td>   
                                                <?php if ($val->verified_status == '1') { ?> 
                                        <lable class="mb-0 btn-sm btn btn-outline-success round">Approved</lable>
                                    <?php } else if ($val->verified_status == '2') { ?>
                                        <lable class="mb-0 btn-sm btn btn-outline-danger round">Reject</lable>
                                    <?php } else { ?>  
                                        <lable class="mb-0 btn-sm btn btn-outline-warning round">Pending User</lable><?php } ?>

                                    </td>
                                    <td style="text-align: center;">
                                        <a href="<?= base_url() ?>admin/kycDoc/showKyc/<?= $val->cust_id ?>">Go</a>
                                    </td>
                                    <td>
                                        <a href="<?= base_url() ?>admin/users/deleteUser/<?= $val->cust_id ?>" class="mb-0 btn-sm btn btn-outline-danger round">
                                            <i class="fa fa-trash-o"></i>Delete
                                        </a>
                                    </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>                                    
                </div>         
            </div>
        </div>
    </div>
</div>


<?php
$msg = $this->input->get('msg');
switch ($msg) {
    case "S":
        $m = "Regiter Successfully...!!!";
        $t = "success";
        break;
    case "U":
        $m = "Update Successfully...!!!";
        $t = "success";
        break;
    case "D":
        $m = "Record Delete Successfully...!!!";
        $t = "success";
        break;
    case "M":
        $m = "Email Send Successfully...!!!";
        $t = "success";
        break;
    case "A":
        $m = "Email or Phone alredy exist!!!";
        $t = "error";
        break;
    case "E":
        $m = "Something went wrong, Please try again!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>

<script>
    $(document).ready(function () {
<?php if ($msg): ?>
            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>
        $("#tbl_user").dataTable({
            "ordering": true,
        });

        $('#select_all').click(function () {
            if (this.checked)
            {
                $('.grid_checkbox').each(function () {
                    this.checked = true;
                });
            } else
            {
                $('.grid_checkbox').each(function () {
                    this.checked = false;
                });
            }
        });
    });
</script>
