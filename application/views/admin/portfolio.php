</div>
<style>
    table{
        text-align: center;
        color: #000;
        font-size: 15px;
        line-height: 25px;
        white-space: nowrap;
    }
    *:not(.gem-table):not(.cart_totals) > table:not(.shop_table):not(.group_table):not(.variations) td {
        border-top: 0px solid #dfe5e8;
        border-bottom: 1px solid #dfe5e8;
        /*border-left: 0px solid #fff;
        border-right: 0px solid #dfe5e8;*/
        /*background-color: rgb(247, 247, 247);*/
        font-weight: 500;
        font-size: 16px;
        color: #1c1c1c;
    }
</style>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Portfolio</h4>
            </div>
            <div class="card-content">
                <div class="card-body pr-0 pl-0">
                    <div class="row" style="margin-left:0px; margin-right: 0px;">
                        <div class="mb-2">
                            <a href="<?= base_url() ?>admin/portfolios/add_portfolio" class="btn btn-secondary btn-gradient-css btn-sm ml-1">Add Portfolio</a>
                        </div>
                        <span id="errortxtsendemail" style="color:red;"></span>
                        <div class="col-md-12 table-responsive pr-0 pl-0">
                            <table class="table table-bordered table-striped text-left" id="tbl_transaction">
                                <thead>
                                    <tr>
                                        <th> Portfolios / Manager </th>             
                                        <th>Base</th>
                                        <th>Time Frame</th>
                                        <th>Invested</th>
                                        <th>Current Profit</th>
                                        <th>Profit Target</th>
                                        <th>Current Value</th>
                                        <th>Status</th>      
                                        <th>Action</th>      
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($portfolio) && !empty($portfolio)) {
                                        foreach ($portfolio as $val) {
                                            $date1 = $val->start_date;
                                            $date2 = $val->end_date;
                                            $diff = abs(strtotime($date2) - strtotime($date1));
                                            $years = floor($diff / (365 * 60 * 60 * 24));
                                            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                            $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
                                            $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                                            $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
                                            ?>
                                            <tr>
                                                <td>
                                                    <div class="name-tddata">
                                                        <img src="<?= base_url() ?>uploads/portfolio_image/<?= $val->portfolio_image ?>" width="40px" height="40px"> 
                                                        <span><?= strtoupper($val->name); ?></span>
                                                        <small><?= $val->manager ?></small>
                                                    </div>
                                                </td>                         
                                                <td><?= $val->currency_name; ?></td>
                                                <td class="text-right"><?= $days . "d, " . $hours . "h, " . $minutes . "m" ?></td>
                                                <td class="text-right">$<?= $val->amount_invest; ?></td>
                                                <td class="text-right">
                                                    <small class="text-success text-bold-700">97.3%</small>
                                                    <p>$11676</p>
                                                </td>
                                                <td class="text-right">
                                                    <small class="text-bold-700">150%</small>
                                                    <p>$18000</p>
                                                </td>
                                                <td class="text-right">$23676</td>
                                                <td>
                                                    <?php if ($val->status == 1) { ?>
                                                        <label class="mb-0 btn-sm btn btn-outline-success round" >Active</label>
                                                    <?php } else if ($val->status == 2) { ?>    
                                                        <label class="mb-0 btn-sm btn btn-outline-danger round" >Reject</label>
                                                    <?php } else { ?>
                                                        <label class="mb-0 btn-sm btn btn-outline-warning round" >Pending</label>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <button type="button" id="btn_submit_approve" name="btn_submit_approve" value="<?= $val->portfolio_id ?>" class="mb-0 btn-sm btn btn-outline-success round btn_submit_approve" style="padding: 5px; margin-bottom: 5px; border-radius: 4px; margin-right: 5px;"> Approve </button>
                                                    <button type="button" id="btn_submit_reject" name="btn_submit_reject" value="<?= $val->portfolio_id ?>" class="mb-0 btn-sm btn btn-outline-danger round btn_submit_reject" style="border-radius: 4px; padding: 5px;"> Reject </button>
                                                    <a href="<?= base_url() ?>admin/portfolios/editPortfolio/<?= $val->portfolio_id ?>" class="mb-0 btn-sm btn btn-outline-primary round" style="padding: 5px; margin-bottom: 5px; border-radius: 4px; margin-right: 5px; color: #1B65D3"> Edit </a>
                                                    <button type="button" id="btn_submit_delete" name="btn_submit_delete" value="<?= $val->portfolio_id ?>" class="mb-0 btn-sm btn btn-outline-danger round btn_submit_delete" style="padding: 5px; margin-bottom: 5px; border-radius: 4px; margin-right: 5px;"> Delete </button>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>                                    
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>


<?php
$msg = $this->input->get('msg');
switch ($msg) {
    case "S":
        $m = "Regiter Successfully...!!!";
        $t = "success";
        break;
    case "U":
        $m = "Update Successfully...!!!";
        $t = "success";
        break;
    case "D":
        $m = "Record Delete Successfully...!!!";
        $t = "success";
        break;
    case "M":
        $m = "Email Send Successfully...!!!";
        $t = "success";
        break;
    case "A":
        $m = "Email or Phone alredy exist!!!";
        $t = "error";
        break;
    case "E":
        $m = "Something went wrong, Please try again!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>

<script>
    $(document).ready(function () {
<?php if ($msg): ?>
            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>
        $("#tbl_transaction").dataTable({
            "ordering": true,
        });

        $('#select_all').click(function () {
            if (this.checked)
            {
                $('.grid_checkbox').each(function () {
                    this.checked = true;
                });
            } else
            {
                $('.grid_checkbox').each(function () {
                    this.checked = false;
                });
            }
        });

        $('#tbl_user').on('click', '.btn_submit_approve', function () {
            var pid = $(this).val();
            $.ajax({
                url: "<?= base_url() ?>admin/portfolios/approvePortfolios/" + pid,
                type: "POST",
                data: {"<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                success: function (data) {
                    if (data = 'success') {

                        alertify.success('Successfully Approval Record...!!!');
                        window.setTimeout('location.reload()', 3000);
                    } else {
                        alertify.error('Something went wrong, Please try again!!!');
                    }
                }

            });
        });

        $('#tbl_user').on('click', '.btn_submit_reject', function () {
            var pid = $(this).val();
            $.ajax({
                url: "<?= base_url() ?>admin/portfolios/rejectPortfolios/" + pid,
                type: "POST",
                data: {"<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                success: function (data) {
                    if (data = 'success') {
                        alertify.success('Successfully Reject Record...!!!');
                        window.setTimeout('location.reload()', 3000);
                    } else {
                        alertify.error('Something went wrong, Please try again!!!');
                    }
                }

            });
        });

        $('#tbl_user').on('click', '.btn_submit_delete', function () {
            var pid = $(this).val();
            $.ajax({
                url: "<?= base_url() ?>admin/portfolios/deletePortfolios/" + pid,
                type: "POST",
                data: {"<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                success: function (data) {
                    if (data = 'success') {
                        alertify.success('Successfully delete Record...!!!');
                        window.setTimeout('location.reload()', 3000);
                    } else {
                        alertify.error('Something went wrong, Please try again!!!');
                    }
                }

            });
        });


    });
</script>
