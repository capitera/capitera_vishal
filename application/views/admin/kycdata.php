<style>
    table{
        text-align: center;
        color: #000;
        font-size: 15px;
        white-space: nowrap;
    }
    .iframe-container {    
        padding-bottom: 60%;
        padding-top: 30px; height: 0; overflow: hidden;
    }

    .iframe-container iframe,
    .iframe-container object,
    .iframe-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .panel-light-primary {
        border: 1px solid #007aff !important;
    }

</style>
<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">User KYC</h4>
            </div>
            <div class="card-content">
                <div class="card-body pr-0 pl-0">
                    <div class="col-md-12 table-responsive pr-0 pl-0">
                        <?php if (isset($removebutton)) { ?>

                        <?php } else { ?>
                            <button id="Verify" type="button" class="btn btn-outline-primary ml-1 mycheck Verify" style="margin-bottom:10px;"> <i class="fa fa-check"></i> Approve</button>
                            <button id="Reject" type="button" class="btn btn-outline-danger mycheck Reject" style="margin-bottom:10px;"> <i class="fa fa-check"></i> Reject</button>
                        <?php } ?>
                        <span id="errortxtsendemail" style="color:red;"></span>
                        <span id="successtxt" style="color:green;"></span>
                        <table class="table table-bordered table-striped text-left" id="tbl_userkyc">
                            <thead>
                                <tr>
                                    <th><!--<input type="checkbox" id="select_all" name="select_all">--></th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Address</th>
                                    <th>Front-ID</th>
                                    <th>Back-ID</th>
                                    <th>Selfie-ID</th>
                                    <th>Proof Of Address</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($user)) {
                                    foreach ($user as $val) {
                                        // if ($val->phone_bill != '' || $val->passport != '' || $val->national_id != '') {
                                        ?>
                                        <tr>
                                            <td> <?php if ($val->verified_status == 0) { ?><input type="checkbox" name="selectall[]"  class="selectall" id="selectall" value="<?= $val->cust_id ?>"><?php } ?></td>
                                            <td><?= $val->fullname ?></td>
                                            <td><?= $val->email ?></td>
                                            <td><?= $val->phone ?></td>
                                            <td class="text-left" style="font-weight: normal;"><?= ($val->address != '' ) ? $val->address : '<p style="color:red;">Address Not Updated..!</p>' ?></td>
                                            <td>
                                                <?php
                                                if ($val->phone_bill != '') {

                                                    $phone_bill = array();
                                                    $phone_bill = explode('.', $val->phone_bill);
                                                    ?>
                                                    <?php if (end($phone_bill) == 'pdf') { ?>
                                                        <a class="btn btn-primary" target="_blank" data-name="Front-ID" href="<?= base_url() ?>./assets/KycDoc/<?= $val->phone_bill ?>">OPEN PDF</a> 
                                                    <?php } else if (end($phone_bill) == 'docx' || end($phone_bill) == 'doc') { ?>
                                                        <a class="btn btn-primary" target="_blank" data-name="Front-ID" href="<?= base_url() ?>./assets/KycDoc/<?= $val->phone_bill ?>">OPEN Doc</a> 
                                                    <?php } else { ?>
                                                        <img style="width:100px;height:100px;cursor: pointer;" data-imgurl="<?= base_url() ?>./assets/KycDoc/<?= $val->phone_bill ?>" class="imgphone_bill" src="<?= base_url() ?>./assets/KycDoc/<?= $val->phone_bill ?>" data-toggle="modal" data-target="#phoneBill">
                                                    <?php } ?>
                                                    <?php
                                                } else {
                                                    echo "<p style='color:red'>Phone bill not updated</p>";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($val->passport != '') {
                                                    $passport = array();
                                                    $passport = explode('.', $val->passport);
                                                    ?>
                                                    <?php if (end($passport) == 'pdf') { ?>
                                                        <a class="btn btn-primary" target="_blank" data-name="Back-ID" href="<?= base_url() ?>./assets/KycDoc/<?= $val->passport ?>">OPEN PDF</a> 
                                                    <?php } else if (end($passport) == 'docx' || end($passport) == 'doc') { ?>
                                                        <a class="btn btn-primary" target="_blank" data-name="Back-ID" href="<?= base_url() ?>./assets/KycDoc/<?= $val->passport ?>">OPEN Doc</a> 
                                                    <?php } else { ?>
                                                        <img style="width:100px;height:100px;cursor: pointer;" data-imgurl="<?= base_url() ?>./assets/KycDoc/<?= $val->passport ?>" class="imgpassport" src="<?= base_url() ?>./assets/KycDoc/<?= $val->passport ?>" data-toggle="modal" data-target="#passport">
                                                    <?php } ?>
                                                    <?php
                                                } else {
                                                    echo "<p style='color:red'>Passport not updated</p>";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($val->national_id != '') {
                                                    $national_id = array();
                                                    $national_id = explode('.', $val->national_id);
                                                    ?>
                                                    <?php if (end($national_id) == 'pdf') { ?>
                                                        <a class="btn btn-primary" target="_blank" data-name="Utility Bill or Phone Bill" href="<?= base_url() ?>./assets/KycDoc/<?= $val->national_id ?>">OPEN PDF</a> 
                                                    <?php } else if (end($national_id) == 'docx' || end($national_id) == 'doc') { ?>
                                                        <a class="btn btn-primary" target="_blank" data-name="Utility Bill or Phone Bill" href="<?= base_url() ?>./assets/KycDoc/<?= $val->national_id ?>">OPEN Doc</a> 
                                                    <?php } else { ?>
                                                        <img style="width:100px;height:100px;cursor: pointer;" data-imgurl="<?= base_url() ?>./assets/KycDoc/<?= $val->national_id ?>" class="imgnational_id" src="<?= base_url() ?>./assets/KycDoc/<?= $val->national_id ?>" data-toggle="modal" data-target="#nationalID">
                                                    <?php } ?>
                                                    <?php
                                                } else {
                                                    echo "<p style='color:red'>Selfie-ID</p>";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($val->proof_of_address != '') {
                                                    $proof_of_address = array();
                                                    $proof_of_address = explode('.', $val->proof_of_address);
                                                    ?>
                                                    <?php if (end($proof_of_address) == 'pdf') { ?>
                                                        <a class="btn btn-primary" target="_blank" data-name="Utility Bill or Phone Bill" href="<?= base_url() ?>./assets/KycDoc/<?= $val->proof_of_address ?>">OPEN PDF</a> 
                                                    <?php } else if (end($proof_of_address) == 'docx' || end($proof_of_address) == 'doc') { ?>
                                                        <a class="btn btn-primary" target="_blank" data-name="Utility Bill or Phone Bill" href="<?= base_url() ?>./assets/KycDoc/<?= $val->proof_of_address ?>">OPEN Doc</a> 
                                                    <?php } else { ?>
                                                        <img style="width:100px;height:100px;cursor: pointer;" data-imgurl="<?= base_url() ?>./assets/KycDoc/<?= $val->proof_of_address ?>" class="imgnational_id" src="<?= base_url() ?>./assets/KycDoc/<?= $val->proof_of_address ?>" data-toggle="modal" data-target="#nationalID">
                                                    <?php } ?>
                                                    <?php
                                                } else {
                                                    echo "<p style='color:red'>Proof Of Address</p>";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($val->verified_status == 2) {
                                                    echo "<label class='btn-sm btn btn-outline-danger round'>Rejected</label>";
                                                } else if ($val->verified_status == 0) {
                                                    echo "<label class='btn-sm btn btn-outline-warning round'>Pending</label>";
                                                } else if ($val->verified_status == 1) {
                                                    echo "<label class='btn-sm btn btn-outline-success round'>Success</label>";
                                                }
                                                ?>        
                                            </td>
                                        </tr>
                                        <?php
                                        //  }
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>                
                </div>                    
            </div>
        </div>
    </div> 
</div> 
<div class="row">
    <div class="col-md-12">
        <div class="modal fade" id="phoneBill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: white !important;">
                        <h4 class="modal-title text-blue" id="myModalLabel"><b>Front-ID</b></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body" style="background-color: white !important;">
                        <p class="text-center"><img style="width: 100%;" id="imgimgphone_bill"></p>
                    </div>
                    <div class="modal-footer" style="background-color: white !important;">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="passport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: white !important;">
                        <h4 class="modal-title text-blue" id="myModalLabel"><b>Back-ID</b></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body" style="background-color: white !important;">
                        <p class="text-center"><img style="width: 100%;" id="imgimgpassport"></p>
                    </div>
                    <div class="modal-footer" style="background-color: white !important;">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="nationalID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: white !important;">
                        <h4 class="modal-title text-blue" id="myModalLabel"><b>Selfie-ID</b></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body" style="background-color: white !important;">
                        <p class="text-center"><img style="width: 100%;" id="imgsrcnationalID"></p>
                    </div>
                    <div class="modal-footer" style="background-color: white !important;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$msg = $this->input->get('msg');
switch ($msg) {
    case "S":
        $m = "Regiter Successfully...!!!";
        $t = "success";
        break;
    case "U":
        $m = "Update Successfully...!!!";
        $t = "success";
        break;
    case "D":
        $m = "Record Delete Successfully...!!!";
        $t = "success";
        break;
    case "M":
        $m = "Email Send Successfully...!!!";
        $t = "success";
        break;
    case "A":
        $m = "Email or Phone alredy exist!!!";
        $t = "error";
        break;
    case "E":
        $m = "Something went wrong, Please try again!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>

<script>
    $(document).ready(function () {

//        if($('#from').val() == '')
//        {
//            $('.disa').attr('disabled','disabled');
//            alert();
//        }

<?php if ($msg): ?>
            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>

        $("#tbl_userkyc").dataTable({
            "ordering": true,
        });

        $('table thead :checkbox').click(function () {
            if (this.checked)
            {
                $('.selectall').each(function () {
                    this.checked = true;
                });
            } else
            {
                $('.selectall').each(function () {
                    this.checked = false;
                });
            }
        });

        $(".imgnational_id").click(function () {
            $('#imgsrcnationalID').attr('src', $(this).data('imgurl'));
        });

        $(".imgpassport").click(function () {
            $('#imgimgpassport').attr('src', $(this).data('imgurl'));
        });

        $(".imgphone_bill").click(function () {
            $('#imgimgphone_bill').attr('src', $(this).data('imgurl'));
        });


        $("#Verify").on("click", function () {
            SmscheckValues = "";
            SmscheckValues = $('.selectall:checked').map(function ()
            {
                return $(this).val();
            }).get();
            if (SmscheckValues.length != 0) {

                $.ajax({
                    url: "<?= site_url() ?>admin/kycDoc/verify_kyc",
                    type: "POST",
                    data: {'cat_id': SmscheckValues, "<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                    success: function (data) {
                        $("#successtxt").text("Successfully Verify.!").fadeIn('slow').fadeOut(5000);
                        location.reload();
                    }

                });
            } else {
                $("#errortxtsendemail").text("Select any record for Verify.!").fadeIn('slow').fadeOut(5000);
            }
        });

        $("#Reject").on("click", function () {
            SmscheckValues = "";
            SmscheckValues = $('.selectall:checked').map(function ()
            {
                return $(this).val();
            }).get();
            if (SmscheckValues.length != 0) {

                $.ajax({
                    url: "<?= site_url() ?>admin/kycDoc/reject_kyc",
                    type: "POST",
                    data: {'cat_id': SmscheckValues, "<?= $this->security->get_csrf_token_name(); ?>": "<?= $this->security->get_csrf_hash(); ?>"},
                    success: function (data) {
                        $("#successtxt").text("Successfully Reject.!").fadeIn('slow').fadeOut(5000);
                        location.reload();
                    }

                });
            } else {
                $("#errortxtsendemail").text("Select any record for Reject!").fadeIn('slow').fadeOut(5000);
            }
        });

    });

</script>
<script type="text/javascript">
    (function (a) {
        a.createModal = function (b) {
            defaults = {title: "", message: "Your Message Goes Here!", closeButton: true, scrollable: false};
            var b = a.extend({}, defaults, b);
            var c = (b.scrollable === true) ? 'style="max-height: 420px;overflow-y: auto;"' : "";
            html = '<div class="modal fade" id="myModal">';
            html += '<div class="modal-dialog">';
            html += '<div class="modal-content">';
            html += '<div class="modal-header">';
            if (b.title.length > 0) {
                html += '<h4 class="modal-title">' + b.title + "</h4>"
            }
            html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';

            html += "</div>";
            html += '<div class="modal-body" ' + c + ">";
            html += b.message;
            html += "</div>";
            html += '<div class="modal-footer">';
            if (b.closeButton === true) {
                html += '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'
            }
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            a("body").prepend(html);
            a("#myModal").modal().on("hidden.bs.modal", function () {
                a(this).remove()
            })
        }
    })(jQuery);

    /*
     * Here is how you use it
     */
    $(function () {
        $('.view-pdf').on('click', function () {
            var pdf_link = $(this).attr('href');
            var pdf_name = $(this).data('name');

            var iframe = '<div class="iframe-container"><iframe src="' + pdf_link + '"></iframe></div>'
            $.createModal({
                title: pdf_name,
                message: iframe,
                closeButton: true,
                scrollable: false
            });
            return false;
        });
    })
</script>




