<!DOCTYPE html>
<?php
$uri_segment = $this->uri->segment(2);
?>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Admin - CAPITERA </title>
        <!-- <link rel="apple-touch-icon" href="<?= base_url() ?>/newassets/app-assets/images/ico/apple-icon-120.png"> -->
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>/newassets/app-assets/images/ico/fevicon.png">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i|Comfortaa:300,400,500,700" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/forms/icheck/icheck.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/forms/icheck/custom.css">

        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/charts/chartist.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
        <!-- END VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/pages/account-register.css">
        <!-- BEGIN MODERN CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/app.css">
        <!-- END MODERN CSS-->
        <link href="<?= base_url() ?>assets/alertify/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/alertify/alertify.default.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/cryptocoins/cryptocoins.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/pages/transactions.css">
        <!-- END Page Level CSS-->

        <link href="<?= base_url() ?>assets/vendor/select2/select2.min.css" rel="stylesheet" media="screen">
        <link href="<?= base_url() ?>assets/vendor/DataTables/css/DT_bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?= base_url() ?>assets/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">

        <link href="<?= base_url() ?>assets/vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
        <link href="<?= base_url() ?>assets/vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/pages/timeline.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/pages/dashboard-ico.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/assets/css/style.css">
        
         <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/assets/css/style_light.css">
        <!-- END Custom CSS-->
        <!-- BEGIN VENDOR JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <style>
            .vertical-compact-menu .main-menu .navigation > li > a > span {
                visibility: visible !important;
                font-size: 0.7rem;
            }
/*            .dataTables_wrapper .dataTables_length label {
                display: none;
            }
            .dataTables_wrapper .dataTables_filter label {
                display: none;
            }
            .dataTables_wrapper .dataTables_info {
                display: none;
            }*/

 #tbl_transaction_wrapper{
                margin-top: 10px; 
            }

        </style>  
        <style>
            html body .content .content-wrapper {
                padding-top: 10px;
                padding-left: 20px;
                padding-right: 20px;
            }
        </style>
    </head>

   <body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
        <!-- fixed-top-->
        <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
            <div class="navbar-wrapper">
                <div class="navbar-header d-md-none">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item d-md-none"><a class="navbar-brand" href="<?= base_url() ?>admin/dashboard"><img class="brand-logo d-none d-md-block" alt="crypto ico admin logo" src="<?= base_url() ?>/newassets/app-assets/images/logo/logo.png"><img class="brand-logo d-sm-block d-md-none" alt="crypto ico admin logo sm" style="width: 130px;" src="<?= base_url() ?>/newassets/app-assets/images/logo/logo-sm.png"></a></li>
                        <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v">   </i></a></li>
                    </ul>
                </div>
                <div class="navbar-container">
                    <div class="collapse navbar-collapse" id="navbar-mobile">
                        <ul class="nav navbar-nav mr-auto float-left">
                             <a class="navigation-brand d-none mt-1 mb-1 d-md-block d-lg-block d-xl-block text-center" href="<?= base_url() ?>admin/dashboard"><img class="brand-logo" alt="crypto ico admin logo" style="width: 210px;" src="<?= base_url() ?>/newassets/app-assets/images/logo/logo.png"/></a>
                            <!-- <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu">         </i></a></li> -->
                        </ul>
                        <ul class="nav navbar-nav float-right">         
                            <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">             <span class="avatar avatar-online"><img src="<?= base_url() ?>/newassets/app-assets/images/portrait/small/avatar-s-1.png" alt="avatar"></span><span class="mr-1"><span class="user-name text-bold-700"><?= ucfirst($this->session->userdata('uname')) ?></span></span></a>
                                <div class="dropdown-menu dropdown-menu-right" style="margin-top: 10px; right: -7px; box-shadow: 1px 1px 4px 3px rgba(0,0,0,0.1) !important;">
                                    <a class="dropdown-item" href="<?= base_url() ?>admin/loginHistory"><i class="ft-user"></i> Login History</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="<?= base_url() ?>admin/setting"><i class="ft-settings"></i> Security </a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="<?= base_url() ?>admin/changepassword"><i class="ft-settings"></i> Change Password</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="<?= base_url() ?>admin/alogin/logout"><i class="ft-power"></i> Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- //////////////////////////////////////////////////////////////////////////// -->
         <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow custom-margin-nav">
            <div class="main-menu-content">
               
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class="nav-item <?= ($uri_segment == 'dashboard') ? 'active' : ''; ?>"><a href="<?= site_url() ?>admin/dashboard"><i class="icon-grid"></i><span class="menu-title" data-i18n=""> Dashboard</span></a></li>
                    <li class="nav-item <?= ($uri_segment == 'users') ? 'active' : ''; ?>"><a href="<?= site_url() ?>admin/users"><i class="icon-users"></i><span class="menu-title" data-i18n=""> Users</span></a></li>
                    <li class="nav-item <?= ($uri_segment == 'portfolios') ? 'active' : ''; ?>"><a href="<?= site_url() ?>admin/portfolios"><i class="icon-doc"></i><span class="menu-title" data-i18n=""> Portfolio</span></a></li>
                    <li class=" nav-item <?= ($uri_segment == 'transaction') ? 'active' : ''; ?>"><a href="<?= site_url() ?>admin/transaction"><i class="icon-shuffle"></i><span class="menu-title" data-i18n=""> Transactions</span></a>
                    </li>
                    <li class=" nav-item <?= ($uri_segment == 'kycDoc') ? 'active' : ''; ?>"><a href="<?= site_url() ?>admin/kycDoc"><i class="icon-doc"></i><span class="menu-title" data-i18n=""> User KYC</span></a></li>
                    <li class=" nav-item <?= ($uri_segment == 'settings') ? 'active' : ''; ?>"><a href="#"><i class="icon-settings"></i><span class="menu-title" data-i18n=""> Setting</span></a>
                        <ul class="menu-content">
                            <li><a class="menu-item" href="<?= site_url() ?>admin/token_package"> Token Package</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

        <div class = "app-content content">
            <div class = "content-wrapper">
                <div class = "content-header row">
                </div>
                <div class = "content-body">
                    <!--Dashboard Details -->
                    <!--First 3 Column -->      