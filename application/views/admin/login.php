<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Admin Login - Capitera Token </title>
        <!-- <link rel="apple-touch-icon" href="<?= base_url() ?>/newassets/app-assets/images/ico/apple-icon-120.png"> -->
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>/newassets/app-assets/images/ico/fevicon.png">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i|Comfortaa:300,400,500,700" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/forms/icheck/icheck.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/forms/icheck/custom.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN MODERN CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/app.css">
        <!-- END MODERN CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/core/menu/menu-types/vertical-compact-menu.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/cryptocoins/cryptocoins.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/pages/account-login.css">
        <!-- END Page Level CSS-->
        <!-- START alerify CSS -->
        <link href="<?= base_url() ?>assets/alertify/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/alertify/alertify.default.css" rel="stylesheet" type="text/css"/>
        <!-- END -->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/assets/css/style.css">
        <!-- END Custom CSS-->
    </head>
    <body class="vertical-layout vertical-compact-menu 1-column menu-expanded blank-page blank-page" data-open="click" data-menu="vertical-compact-menu" data-col="1-column">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="flexbox-container">

                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <!-- image -->
                      
                            <div class="col-xl-3 col-lg-4 col-md-5 col-sm-5 col-12 p-0 text-center d-none">
                                
                            </div>
                            <!-- login form -->
                            <div class="col-xl-4 col-lg-4 col-md-5 col-sm-5 col-12 p-0">

                                <div class="form-box">
                                    <div class="form-heading">
                                        <h4 class="h3 text-uppercase">Admin Login</h4>
                                        <?php
                                        echo ($this->session->flashdata('msg')) ? $this->session->flashdata('msg') : '';
                                        ?> 
                                    </div>
                                    <div class="form-body">
                                        <form class="form-horizontal form-signin" id="frm_login" name="frm_login" method="post" action="<?= base_url() ?>admin/alogin/authentication">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <fieldset class="form-group">
                                                <label for="user-name">Username</label>
                                                <input type="text" class="form-control" id="user-name" name="email" placeholder="Your Username" value=""  autofocus="">
                                                <span id="erroremail" style="color:red"></span>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label for="user-password">Password</label>
                                                <input type="password" class="form-control" id="user-password" name="password" placeholder="Enter Password" value="" autofocus="">
                                                <span id="errorpassword" style="color:red"></span>
                                            </fieldset>
                                            
                                            <div class="text-center">
                                                <button type="submit" class="btn-secondary btn-gradient-css btn-sm my-1" id="btn_login">Log In</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>        
                        </div>    
                    </section>

                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- BEGIN VENDOR JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN MODERN JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/newassets/app-assets/js/core/app.js" type="text/javascript"></script>
        <!-- END MODERN JS-->

        <!-- START alerify JS -->    
        <script src="<?= base_url() ?>assets/alertify/alertify.min.js" type="text/javascript"></script>
        <!-- END -->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->

        <script type="text/javascript">
            $(document).ready(function () {
                $("#btn_login").on("click", function () {
                    if ($("#user-name").val().trim() == "") {
                        $("#erroremail").text("Please Enter Username").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else if ($("#user-password").val() == "") {
                        $("#errorpassword").text("Please Enter Password").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else {
                        return true; //submit form
                    }
                    return false; //Prevent form to submitting
                });
            });
        </script>
    </body>
</html>