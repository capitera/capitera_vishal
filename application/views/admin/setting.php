<div class="row">
    <div class="col-lg-12 custom-padding-column">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Google Authenticator</h4>
            </div>
            <div class="card-content">
                <div class="col-md-12" style="margin-top: 20px;">
                    <form class="form-register" id="frm_register" name="frm_register" method="post" action="<?= base_url() ?>admin/setting/updateSetting" >
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <fieldset>
                            <h4 class="box-title">Enable Authenticator Support</h4>
                            <p>Authenticator Secret Code : <?= isset($google_auth_code) ? $google_auth_code : '' ?></p>
                            <?php if (isset($qrCodeUrl) && $qrCodeUrl != '') { ?>
                                <div class="form-group" style="text-align: center;">
                                    <img src='<?php echo $qrCodeUrl; ?>'/>
                                </div>
                            <?php }
                            ?>
                        </fieldset>
                        <br>
                        <fieldset>
                            <h4 class="box-title">Scan to bind your Google Authenticator</h4>
                            <p>1. Install an authenticator app on your mobile device if you don't already have one Here are the link for <a href="https://itunes.apple.com/in/app/google-authenticator/id388497605?mt=8">iOS</a> and <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en_IN">Android</a> phone. Go ahead and download it now to get started.. </p>
                            <p>2. Scan QR code with the authenticator</p>
                            <p>3. Please write down or print a copy of the 16 digit secret code and put it in a safe place. If your phone gets lost. stolen or erased, you will need this code to link Capitera to a new Authenticator app install once again.</p>
                            <p>4. Do not share it with anyone. Be aware of phishing scams. We Will never ask you for this key</p>
                        </fieldset>  
                        <br> 
                        <fieldset>
                            <h4 class="box-title">Once an Authenticator app is enable</h4>
                            <div class="form-group">
                                <div cass="row">
                                    <div class="col-md-2" style="display: inline-block;">
                                        <img src="<?= base_url() ?>assets/images/google-authenticator.png"  width="100%">
                                    </div>
                                    <input type="hidden" id="adrs" name="adrs" value="<?= isset($google_auth_code) ? $google_auth_code : '' ?>" readonly>
                                    <div class="col-md-8" style="display: inline-block;">
                                        <label>Enter the 2-step verification code provided by your authentication app</label>
                                        <input type="text" id="appcode" name="appcode" required="" value="" placeholder=" Enter the 6-Digit verification code" style="width: 80%; height: 40px;border: 1px #000 solid;">
                                        <input type="hidden" name="twoAuthSts" value="<?= ($twoAuth->two_auth == 0) ? '1' : '0' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions" style="text-align: center;">
                                <?php
                                if (!empty($twoAuth)) {
                                    ?>
                                    <button type="submit" class="btn btn-secondary btn-gradient-css btn-sm" id="btn_register"><?= ($twoAuth->two_auth == 0) ? 'ENABLE' : 'DISABLE' ?></button>
                                    <?php
                                }
                                ?>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end: DYNAMIC TABLE -->
</div>
</div>
</div>   
<?php
$msg = $this->input->get('msg');
switch ($msg) {
    case "U":
        $m = "Update Successfully...!!!";
        $t = "success";
        break;
    case "E":
        $m = "Something went wrong, Please try again!!!";
        $t = "error";
        break;
    default:
        $m = 0;
        break;
}
?>
<!-- start: JavaScript Event Handlers for this page -->

<script type="text/javascript">
    $(document).ready(function () {

<?php if ($msg): ?>
            alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>

    });
</script>
<!-- end: JavaScript Event Handlers for this page -->
<!-- end: CLIP-TWO JAVASCRIPTS -->
