<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Account Register - Capitera Token </title>
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>/newassets/app-assets/images/ico/fevicon.png">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i|Comfortaa:300,400,500,700" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/forms/icheck/icheck.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/forms/icheck/custom.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN MODERN CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/app.css">
        <!-- END MODERN CSS-->
        <!-- START alerify CSS -->
        <link href="<?= base_url() ?>assets/alertify/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/alertify/alertify.default.css" rel="stylesheet" type="text/css"/>
        <!-- END -->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/core/menu/menu-types/vertical-compact-menu.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/cryptocoins/cryptocoins.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/pages/account-register.css">
        <!-- END Page Level CSS-->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/assets/css/style.css">
        <!-- END Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/assets/css/style_light.css">
        <style>
            html body .content.app-content {
                overflow: auto;
            }
            .field-icon {
                float: right;
                margin-left: -25px;
                margin-top: -25px;
                position: relative;
                z-index: 2;
            }

            .container{
                padding-top:50px;
                margin: auto;
            }
            form label {
                color: #2f2f2f;
                font-weight: 500;
            }
        </style>
    </head>
    <body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
        <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
            <div class="navbar-wrapper">
                <div class="navbar-header d-md-none">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item d-md-none"><a class="navbar-brand" href="<?= base_url() ?>customer/dashboard"><img class="brand-logo d-none d-md-block" alt="crypto ico admin logo" src="<?= base_url() ?>newassets/app-assets/images/logo/logo.png"><img class="brand-logo d-sm-block d-md-none" alt="crypto ico admin logo sm" style="width: 130px;" src="<?= base_url() ?>/newassets/app-assets/images/logo/logo-sm.png"></a></li>
                    </ul>
                </div>
                <div class="navbar-container">
                    <div class="collapse navbar-collapse" id="navbar-mobile">
                        <ul class="nav navbar-nav mr-auto float-left">
                            <a class="navigation-brand mt-1 mb-1 d-none d-md-block d-lg-block d-xl-block text-center" href="<?= base_url() ?>customer/dashboard" style="padding-top: 8px;">
                                <img class="brand-logo" alt="crypto ico admin logo" style="width: 200px;" src="<?= base_url() ?>newassets/app-assets/images/logo/logo.png"/>
                            </a>
                        </ul>
                        <ul class="nav navbar-nav float-right">
                            <li class="nav-item" style="margin-top: 10px;">
                                <a class="" href="<?= base_url() ?>login" style="color: #000">
                                    <span class="user-name text-bold-600">LOG IN</span>
                                </a>
                                or
                                <a class="" href="<?= base_url() ?>register" style="color: #000">
                                    <span class="user-name text-bold-600">REGISTER</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="flexbox-container">    
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <!-- image -->
                            <div class="col-xl-3 col-lg-4 col-md-5 col-sm-5 col-12 p-0 text-center d-none">
                            </div>
                            <!-- register form -->
                            <div class="col-xl-4 col-lg-4 col-md-5 col-sm-5 col-12 p-0">
                                <div class="form-box">
                                    <div class="form-heading">
                                        <h4 class="h3 text-uppercase">Register</h4>
                                    </div>
                                    <div class="form-body">
                                        <form class="form-horizontal form-signin" id="frm_register" name="frm_register" method="post" action="<?= base_url() ?>register/add_customer">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <fieldset class="form-group">
                                                        <label for="email">E-mail</label><label style="float: right; padding-right: 2px;"><span id="erroremail" style="color:red; font-weight: 500;"></span></label>
                                                        <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">

                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <fieldset class="form-group">
                                                        <label for="user-password">Choose Password</label><label style="float: right; padding-right: 2px;"><span id="errorpassword" style="color:red; font-weight: 500;"></span> <span style="color:#39d1a7;"  id="errorpass_strength">Password strength: STRONG</span></label>
                                                        <input type="password" class="form-control" id="user-password" name="password" placeholder="Choose Password">
                                                        <span toggle="#password-field" style="font-size: 20px; margin-top: -31px; padding-right: 30px;" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <fieldset class="form-group">
                                                        <label for="user-cnf-password">Re-enter Password</label><label style="float: right; padding-right: 2px;"><span id="errorconf_password" style="color:red; font-weight: 500;"></span> <span id="errorconf_passwordgreen" style="color:red; font-weight: 500;"></span></label>
                                                        <input type="password" class="form-control" id="user-cnf-password" name="conf_password" placeholder="Re-enter Password">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <fieldset class="form-group">
                                                        <label for="referral_id">Referral ID (Optional)</label>
                                                        <input type="text" class="form-control" id="referral_id" name="referral_id" placeholder="Referral ID">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-center text-sm-left">
                                                            <div class="form-group" style="font-weight: 500; color: #000;">
                                                                <input type="checkbox" name="terms" id="terms" onchange=""> I am over the age of 18 and I agree to the  <a href="#" target="_blank" style="color: #296083">Terms & Conditions</a>.<br>
                                                                Already registered ? <a href="<?= base_url() ?>" style="color: #296083">Login In.</a>
                                                            </div>
                                                        </div>                                
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-right">
                                                <button type="submit" class="btn btn-secondary btn-gradient-css btn-sm my-1 btn-wide" disabled id="btn_register" style="margin-right: 8px; font-size: 15px; letter-spacing: 1px;">Register</button>
                                                <button type="reset" class="btn btn-secondary btn-gradient-css btn-sm" style="font-size: 15px; letter-spacing: 1px;">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>    
                            </div>        
                        </div>    
                    </section>
                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- BEGIN VENDOR JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN MODERN JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/newassets/app-assets/js/core/app.js" type="text/javascript"></script>
        <!-- END MODERN JS-->
        <!-- START alerify JS -->    
        <script src="<?= base_url() ?>assets/alertify/alertify.min.js" type="text/javascript"></script>
        <!-- END -->

        <!-- BEGIN PAGE LEVEL JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <?php
        $msg = $this->input->get("msg");
        switch ($msg) {
            case "A":
                $m = "Email alredy exist!!!";
                $t = "error";
                break;
            case "E":
                $m = "Something went wrong, Please try again!!!";
                $t = "error";
                break;
            case "C":
                $m = "Enter Valid Captcha, Please try again!!!";
                $t = "error";
                break;
            default:
                $m = 0;
                break;
        }
        ?>
        <script>
            jQuery(document).ready(function () {
                Main.init();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {

<?php if ($msg): ?>
                    alertify.<?= $t ?>("<?= $m ?>");
<?php endif; ?>

                $('#terms').click(function () {
                    if ($(this).is(':checked')) {
                        $('#btn_register').removeAttr('disabled');
                    } else {
                        $('#btn_register').attr('disabled', 'disabled');
                    }
                });

                $(".toggle-password").click(function () {
                    $(this).toggleClass("fa-eye fa-eye-slash");
                    var input = $('#user-password');
                    if (input.attr("type") == "password") {
                        input.attr("type", "text");
                    } else {
                        input.attr("type", "password");
                    }
                });

                $("#btn_register").on("click", function () {
                    var pwd_pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^.&*]{8,}$/;
                    var pwd = $("#user-password").val();

                    var email_patten = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    var email = $("#email").val().trim();
                    if ($("#email").val().trim() == "") {
                        $("#erroremail").text("Please Enter Email").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else if (!email_patten.test(email)) {
                        $("#erroremail").text("Please Enter Valid Email").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else if ($("#user-password").val() == "") {
                        $("#errorpassword").text("Please Enter Password").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else if (!pwd_pattern.test(pwd)) {
                        $("#errorpass_strength").text("");
                        $("#errorpassword").text("Please password strength strong").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else if ($("#user-cnf-password").val() == "") {
                        $("#errorconf_password").text("Please Enter Confirm Password").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else if ($("#user-password").val() != $("#user-cnf-password").val()) {
                        $("#errorconf_passwordgreen").text("The passwords don't match").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else {
                        console.log("okokokok");
                        return true; //submit form
                    }
                    return false; //Prevent form to submitting
                });
            });
        </script>
    </body>
</html>