<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <title>Account Login - Capitera Token </title>
        <!-- <link rel="apple-touch-icon" href="<?= base_url() ?>/newassets/app-assets/images/ico/apple-icon-120.png"> -->
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>/newassets/app-assets/images/ico/fevicon.png">
        <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i|Comfortaa:300,400,500,700" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/forms/icheck/icheck.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/forms/icheck/custom.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN MODERN CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/app.css">
        <!-- END MODERN CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/core/menu/menu-types/vertical-compact-menu.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/vendors/css/cryptocoins/cryptocoins.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/app-assets/css/pages/account-login.css">
        <!-- END Page Level CSS-->
        <script src='https://www.google.com/recaptcha/api.js'></script> 
        <!-- START alerify CSS -->
        <link href="<?= base_url() ?>assets/alertify/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/alertify/alertify.default.css" rel="stylesheet" type="text/css"/>
        <!-- END -->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/newassets/assets/css/style.css">
        <!-- END Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>newassets/assets/css/style_light.css">
    </head>
    <body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
        <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow">
            <div class="navbar-wrapper">
                <div class="navbar-header d-md-none">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item d-md-none"><a class="navbar-brand" href="<?= base_url() ?>customer/dashboard"><img class="brand-logo d-none d-md-block" alt="crypto ico admin logo" src="<?= base_url() ?>newassets/app-assets/images/logo/logo.png"><img class="brand-logo d-sm-block d-md-none" alt="crypto ico admin logo sm" style="width: 130px;" src="<?= base_url() ?>/newassets/app-assets/images/logo/logo-sm.png"></a></li>
                    </ul>
                </div>
                <div class="navbar-container">
                    <div class="collapse navbar-collapse" id="navbar-mobile">
                        <ul class="nav navbar-nav mr-auto float-left">
                            <a class="navigation-brand mt-1 mb-1 d-none d-md-block d-lg-block d-xl-block text-center" href="<?= base_url() ?>customer/dashboard" style="padding-top: 8px;">
                                <img class="brand-logo" alt="crypto ico admin logo" style="width: 200px;" src="<?= base_url() ?>newassets/app-assets/images/logo/logo.png"/>
                            </a>
                        </ul>
                        <ul class="nav navbar-nav float-right">
                            <li class="nav-item" style="margin-top: 10px;">
                                <a class="" href="<?= base_url() ?>login" style="color: #000">
                                    <span class="user-name text-bold-600">LOG IN</span>
                                </a>
                                or
                                <a class="" href="<?= base_url() ?>register" style="color: #000">
                                    <span class="user-name text-bold-600">REGISTER</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="flexbox-container">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-6 col-12 p-0">
                                <div class="form-box">
                                    <div class="form-heading">
                                        <h4 class="h3 text-uppercase">Log In</h4>

                                    </div>
                                    <div class="form-body">
                                        <form class="form-horizontal form-signin" id="frm_login" name="frm_login" method="post" action="<?= base_url() ?>login/authentication">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <fieldset class="form-group">
                                                <label for="user-name">E-mail</label> <label style="float: right; padding-right: 2px;"><span id="erroremail" style="color:red; font-weight: 500;"> <?php echo ($this->session->flashdata('msg_email')) ? $this->session->flashdata('msg_email') : ''; ?> </span></label>
                                                <input type="email" class="form-control" id="user-name" name="email" placeholder="Your Username" value=""  autofocus="">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label for="user-password">Password</label><label style="float: right; padding-right: 2px;"><span id="errorpassword" style="color:red; font-weight: 500;"><?php echo ($this->session->flashdata('msg_password')) ? $this->session->flashdata('msg_password') : ''; ?> </span></label>
                                                <input type="password" class="form-control" id="user-password" name="password" placeholder="Enter Password" value="" autofocus="">
                                                <div class="text-right">
                                                    <a href="<?= base_url() ?>forgotpassword" class="card-link text-dark">Forgot your password?</a>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label for="authenticator">2FA Authenticator Code</label><label style="float: right; padding-right: 2px;"><span id="errorauthenticator" style="color:red; font-weight: 500;"><?php echo ($this->session->flashdata('msg_2fa')) ? $this->session->flashdata('msg_2fa') : ''; ?> </span></label>
                                                <input type="text" class="form-control" id="authenticator" name="authenticator" placeholder="Enter 2FA Code if enabled" value="" autofocus="">
                                            </fieldset>
                                            <div class="text-right">
                                                <button type="submit" class="btn btn-secondary btn-gradient-css btn-sm my-1 btn-wide" id="btn_login" style="margin-right: 8px; font-size: 15px; letter-spacing: 1px;">Log In</button> 
                                                <button type="reset" class="btn btn-secondary btn-gradient-css btn-sm" style="font-size: 15px; letter-spacing: 1px;">Cancel</button>
                                            </div>
                                            <p> Don't have an account yet? <a href="<?= base_url() ?>register">Create an Account.</a></p>
                                        </form>
                                    </div>
                                </div>
                            </div>        
                        </div>    
                    </section>
                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- BEGIN VENDOR JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN MODERN JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/newassets/app-assets/js/core/app.js" type="text/javascript"></script>
        <!-- END MODERN JS-->

        <!-- START alerify JS -->    
        <script src="<?= base_url() ?>assets/alertify/alertify.min.js" type="text/javascript"></script>
        <!-- END -->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="<?= base_url() ?>/newassets/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->

        <script>
            function onSubmit(token) {
                document.getElementById("frm_login").submit();
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#btn_login").on("click", function () {
                    if ($("#user-name").val().trim() == "") {
                        $("#erroremail").text("Please Enter Email").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else if ($("#user-password").val() == "") {
                        $("#errorpassword").text("Please Enter Password").fadeIn('slow').fadeOut(5000);
                        return false;
                    } else {
                        return true; //submit form
                    }
                    return false; //Prevent form to submitting
                });
            });
        </script>
    </body>
</html>