<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Common {

    private $_CI;

    function __construct() {
        $this->_CI = & get_instance();
    }

    function sendEmail($from, $to, $subject, $message, $name = NULL) {
        $config = Array(
            // 'protocol' => 'smtp',
            // 'smtp_host' => 'ssl://smtp.googlemail.com',
            // 'smtp_port' => 465,
            // 'smtp_user' => 'vishaltesting7@gmail.com', // change it to yours
            // 'smtp_pass' => 'vishal789', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->_CI->load->library('email', $config);
        $this->_CI->email->set_newline("\r\n");
        $this->_CI->email->from($from, $name);
        $this->_CI->email->to($to);
        $this->_CI->email->subject($subject);
        $this->_CI->email->message($message);
        if ($this->_CI->email->send()) {
            return 1;
        } else {
            return 0;
        }
    }

    function getTransction($tid) {
        $this->_CI->db->select('*');
        $this->_CI->db->from('transaction_mast');
        $this->_CI->db->where('txn_id', $tid);
        $result = $this->_CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

    function getCustTotalFanscoin($userid) {
        $this->_CI->db->select('*');
        $this->_CI->db->from('wallet');
        $this->_CI->db->where(array('cust_id' => $userid));
        $totalamount = $this->_CI->db->get();
        if ($totalamount->num_rows() > 0) {
            return $totalamount->row()->total_coins;
        } else {
            return '0';
        }
    }

    function getCustEmail($cid) {
        $this->_CI->db->select('*');
        $this->_CI->db->from('customer_master');
        $this->_CI->db->where('cust_id', $cid);
        $result = $this->_CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->row()->email;
        } else {
            return '';
        }
    }

    function getCustWallet($cid) {
        $this->_CI->db->select('*');
        $this->_CI->db->from('wallet');
        $this->_CI->db->where('cust_id', $cid);
        $result = $this->_CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

    function getCustData($cid) {
        $this->_CI->db->select('*');
        $this->_CI->db->from('customer_master');
        $this->_CI->db->where('cust_id', $cid);
        $result = $this->_CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

    function getProfile($cid) {
        $q = $this->_CI->db->select('profile_pic')->from('customer_master')->where('cust_id', $cid)->get();
        if ($q->num_rows() > 0) {
            return $q->row()->profile_pic;
        } else {
            return '';
        }
    }

    function select_subject($sub_id) {
        $q = $this->_CI->db->select('*')->from('email_template')->where('temp_id', $sub_id)->get();
        if ($q->num_rows() > 0) {
            return $q->row()->subject;
        } else {
            return '';
        }
    }

    function getThemeStatus() {
        $result = $this->_CI->db->get_where('theme_setting');
        if ($result->num_rows() > 0) {
            return $result->row()->theme_color;
        } else {
            return 0;
        }
    }

    function getUserAuthenticationStatus($cid) {
        $result = $this->_CI->db->get_where('customer_master', array('cust_id' => $cid));
        if ($result->num_rows() > 0) {
            return $result->row()->two_auth;
        } else {
            return '';
        }
    }

    function getKycStatus($cid) {
        $this->_CI->db->select('verified_status');
        $this->_CI->db->from('customer_master');
        $this->_CI->db->where('cust_id', $cid);
        $q = $this->_CI->db->get();
        if ($q->num_rows() > 0) {
            return $q->row()->verified_status;
        } else {
            return '';
        }
    }

    function getPackageName($pid) {
        $q = $this->_CI->db->get_where('token_package', array('package_id' => $pid));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
    }

    function getTransactionMast($tid) {
        $this->_CI->db->select('*');
        $this->_CI->db->from('transaction_mast');
        $this->_CI->db->where('txn_id', $tid);
        $result = $this->_CI->db->get();
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return '';
        }
    }

}
