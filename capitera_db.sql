-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2020 at 12:56 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `capitera_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `additional_charge`
--

CREATE TABLE IF NOT EXISTS `additional_charge` (
  `additional_charge_id` int(11) NOT NULL AUTO_INCREMENT,
  `charge` int(11) DEFAULT NULL,
  PRIMARY KEY (`additional_charge_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `additional_charge`
--

INSERT INTO `additional_charge` (`additional_charge_id`, `charge`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `two_auth` tinyint(4) DEFAULT NULL,
  `google_auth_code` varchar(255) DEFAULT NULL,
  `oauth_provider` text,
  `oauth_uid` text,
  `dashboard` int(11) NOT NULL DEFAULT '0',
  `users` int(11) DEFAULT NULL,
  `email_template` int(11) DEFAULT NULL,
  `withdraw` int(11) DEFAULT NULL,
  `transaction` int(11) DEFAULT NULL,
  `referral` int(11) DEFAULT NULL,
  `kycdoc` int(11) DEFAULT NULL,
  `setting` int(11) DEFAULT NULL,
  `rolemanager` int(11) DEFAULT NULL,
  `twofa_setting` int(11) DEFAULT NULL,
  `change_password` tinyint(4) NOT NULL DEFAULT '0',
  `support_ticket` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `email`, `password`, `two_auth`, `google_auth_code`, `oauth_provider`, `oauth_uid`, `dashboard`, `users`, `email_template`, `withdraw`, `transaction`, `referral`, `kycdoc`, `setting`, `rolemanager`, `twofa_setting`, `change_password`, `support_ticket`) VALUES
(1, 'admin', 'vishaltesting10@gmail.com', '31e2fc9cf155dc1db00f2e5e2690f6e4', 0, 'BAOIWB3HL2VNP2ZF', '', '', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adminloginhistory`
--

CREATE TABLE IF NOT EXISTS `adminloginhistory` (
  `adminloginhistory_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `browser` varchar(555) DEFAULT NULL,
  `ip_address` varchar(555) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`adminloginhistory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `adminloginhistory`
--

INSERT INTO `adminloginhistory` (`adminloginhistory_id`, `user_id`, `browser`, `ip_address`, `reg_date`) VALUES
(1, 1, 'Chrome', '::1', '2019-12-09 06:00:25'),
(2, 1, 'Chrome', '::1', '2019-12-09 06:39:27'),
(3, 1, 'Chrome', '::1', '2019-12-10 04:43:08'),
(4, 1, 'Chrome', '::1', '2019-12-11 03:24:06'),
(5, 1, 'Chrome', '::1', '2019-12-11 05:56:29'),
(6, 1, 'Chrome', '::1', '2019-12-11 05:57:41'),
(7, 1, 'Chrome', '::1', '2019-12-12 03:20:57'),
(8, 1, 'Chrome', '::1', '2019-12-14 03:31:21'),
(9, 1, 'Chrome', '::1', '2019-12-14 08:55:06'),
(10, 1, 'Chrome', '::1', '2019-12-26 06:56:15'),
(11, 1, 'Chrome', '::1', '2019-12-26 09:57:29'),
(12, 1, 'Chrome', '::1', '2019-12-26 09:58:36'),
(13, 1, 'Chrome', '::1', '2019-12-28 04:28:44'),
(14, 1, 'Chrome', '::1', '2019-12-28 08:54:11'),
(15, 1, 'Chrome', '::1', '2019-12-28 09:18:21'),
(16, 1, 'Chrome', '::1', '2019-12-29 03:35:03'),
(17, 1, 'Chrome', '::1', '2020-01-01 04:35:30'),
(18, 1, 'Chrome', '::1', '2020-01-01 05:07:10'),
(19, 1, 'Chrome', '::1', '2020-01-01 08:51:13'),
(20, 1, 'Chrome', '::1', '2020-01-02 03:50:23'),
(21, 1, 'Chrome', '::1', '2020-01-02 10:38:17'),
(22, 1, 'Chrome', '::1', '2020-01-03 03:15:49'),
(23, 1, 'Chrome', '::1', '2020-01-03 04:51:21'),
(24, 1, 'Chrome', '::1', '2020-01-06 06:39:26'),
(25, 1, 'Chrome', '::1', '2020-01-08 10:02:45'),
(26, 1, 'Chrome', '::1', '2020-01-09 03:35:39'),
(27, 1, 'Chrome', '::1', '2020-01-10 03:31:37'),
(28, 1, 'Chrome', '::1', '2020-01-11 03:18:16'),
(29, 1, 'Chrome', '::1', '2020-01-11 05:45:53'),
(30, 1, 'Chrome', '::1', '2020-01-11 08:29:05'),
(31, 1, 'Chrome', '::1', '2020-01-11 08:39:23'),
(32, 1, 'Chrome', '::1', '2020-01-11 08:49:04'),
(33, 1, 'Chrome', '::1', '2020-01-12 02:08:05'),
(34, 1, 'Chrome', '::1', '2020-01-12 03:33:27'),
(35, 1, 'Chrome', '::1', '2020-01-13 04:12:52'),
(36, 1, 'Chrome', '::1', '2020-01-17 04:50:11'),
(37, 1, 'Chrome', '::1', '2020-01-17 08:27:46'),
(38, 1, 'Chrome', '::1', '2020-01-17 08:34:24'),
(39, 1, 'Chrome', '::1', '2020-01-17 08:37:58'),
(40, 1, 'Chrome', '::1', '2020-01-17 09:55:21'),
(41, 1, 'Chrome', '::1', '2020-01-22 08:43:16'),
(42, 1, 'Chrome', '::1', '2020-01-23 03:10:26'),
(43, 1, 'Chrome', '::1', '2020-01-24 02:40:24'),
(44, 1, 'Chrome', '::1', '2020-01-24 09:01:32'),
(45, 1, 'Chrome', '::1', '2020-01-25 03:17:09');

-- --------------------------------------------------------

--
-- Table structure for table `archive_emails`
--

CREATE TABLE IF NOT EXISTS `archive_emails` (
  `archive_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_subject` varchar(255) NOT NULL,
  `email_message` text NOT NULL,
  `sent_by` int(11) NOT NULL,
  `user_ids` varchar(255) NOT NULL,
  `sent_date` datetime NOT NULL,
  PRIMARY KEY (`archive_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `archive_emails`
--

INSERT INTO `archive_emails` (`archive_id`, `email_subject`, `email_message`, `sent_by`, `user_ids`, `sent_date`) VALUES
(1, 'test e mail', '<p>Dear [NAME]</p>\r\n\r\n<p>This is a test email</p>', 1, '62,61,59,58,57', '2019-10-21 09:12:37'),
(2, 'test e mail', '<p>Dear [NAME]</p>\r\n\r\n<p>Test mail</p>\r\n\r\n<p>by, Capitera Team</p>', 1, '31,30,29,28,27', '2019-10-22 04:03:10'),
(3, 'test e mail', '<p>Dear [NAME]</p>\n\n<p>This is a test email or the users</p>', 1, '68', '2019-10-24 06:26:27'),
(4, 'test e mail', '<p>Dear [NAME]</p>\n\n<p>Testing...!</p>', 1, '68', '2019-10-24 06:37:00'),
(5, 'test e mail', '<p>Dear [NAME]</p>\n\n<p>Testing...!</p>', 1, '68', '2019-10-24 06:41:07'),
(6, 'test e mail', '<p>Dear [NAME]</p>\n\n<p>Testing...!</p>', 1, '68', '2019-10-24 06:42:21'),
(7, 'test e mail', '<p>Dear [NAME]</p>\n\n<p>Testing...!</p>', 1, '68', '2019-10-24 06:43:20'),
(8, 'test new email', '<p>test new email</p> \n\ntest', 1, '73', '2019-10-25 12:09:19');

-- --------------------------------------------------------

--
-- Table structure for table `assets_currency`
--

CREATE TABLE IF NOT EXISTS `assets_currency` (
  `currency_wallet_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(255) NOT NULL,
  `symbol` varchar(255) NOT NULL,
  `icon_name` varchar(255) NOT NULL,
  `currency_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`currency_wallet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `assets_currency`
--

INSERT INTO `assets_currency` (`currency_wallet_id`, `currency_name`, `symbol`, `icon_name`, `currency_status`) VALUES
(1, 'Bitcoin', 'BTC', 'btc.png', 1),
(2, 'Ethereum', 'ETH', 'eth.png', 1),
(3, 'Ripple', 'XRP', 'xrp.png', 1),
(4, 'Tether', 'USDT', 'usdt.png', 1),
(5, 'Bitcoin Cash', 'BCH', 'btc_cash.png', 1),
(6, 'Litecoin', 'LTC', 'ltc.png', 1),
(7, 'EOS', 'EOS', 'eos.png', 1),
(8, 'Binancecoin', 'BNB', 'binance_coin.png', 1),
(9, 'Bitcoin Cash SV', 'BSV', 'btc_sv.png', 1),
(10, 'Cardano', 'ADA', 'cardano.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE IF NOT EXISTS `bonus` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `from` text,
  `to` text,
  `percentage` text,
  PRIMARY KEY (`b_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`b_id`, `from`, `to`, `percentage`) VALUES
(5, '500', '1000', '1'),
(6, '1000', '2000', '2'),
(7, '2000', '5000', '3'),
(8, '5000', '10000', '4'),
(12, '10000', '50000', '5');

-- --------------------------------------------------------

--
-- Table structure for table `bonus_display_setting`
--

CREATE TABLE IF NOT EXISTS `bonus_display_setting` (
  `bonus_display_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`bonus_display_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bonus_display_setting`
--

INSERT INTO `bonus_display_setting` (`bonus_display_setting_id`, `status`) VALUES
(1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `coin_address`
--

CREATE TABLE IF NOT EXISTS `coin_address` (
  `a_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` text,
  `coin_name` text,
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `coin_address`
--

INSERT INTO `coin_address` (`a_id`, `address`, `coin_name`) VALUES
(59, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin'),
(60, '0x6b8550B7401dc5C745a7316Ab6F79efbb3b99C15', 'ethereum'),
(61, 'XgVXXrALw3a63Q5Fr2PzgS68T9xBpXZjPv', 'Dash'),
(62, 'rP3QvoUqcHEcqiwYVCRJ2roUxSU8Pf8SVi', 'XRP'),
(63, 'MSw1aJabU2THkv2tiTJFXBJvJecFJ6dRhU', 'litecoin');

-- --------------------------------------------------------

--
-- Table structure for table `coin_rate`
--

CREATE TABLE IF NOT EXISTS `coin_rate` (
  `coin_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `coin_rate` varchar(50) DEFAULT NULL,
  `rate_status` varchar(100) DEFAULT '0',
  PRIMARY KEY (`coin_rate_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `coin_rate`
--

INSERT INTO `coin_rate` (`coin_rate_id`, `coin_rate`, `rate_status`) VALUES
(8, '0.50', '1');

-- --------------------------------------------------------

--
-- Table structure for table `customerloginhistory`
--

CREATE TABLE IF NOT EXISTS `customerloginhistory` (
  `customerloginhistory_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `browser` varchar(555) DEFAULT NULL,
  `ip_address` varchar(555) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customerloginhistory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `customerloginhistory`
--

INSERT INTO `customerloginhistory` (`customerloginhistory_id`, `user_id`, `browser`, `ip_address`, `reg_date`) VALUES
(1, 1, 'Chrome', '::1', '2019-12-09 05:41:36'),
(2, 1, 'Chrome', '::1', '2019-12-11 03:25:22'),
(3, 1, 'Chrome', '::1', '2019-12-11 05:06:12'),
(4, 1, 'Chrome', '::1', '2019-12-11 05:06:49'),
(5, 1, 'Chrome', '::1', '2019-12-11 05:12:06'),
(6, 1, 'Chrome', '::1', '2019-12-11 05:12:22'),
(7, 1, 'Chrome', '::1', '2019-12-11 11:55:31'),
(8, 1, 'Chrome', '::1', '2019-12-12 03:20:45'),
(9, 1, 'Chrome', '::1', '2019-12-14 05:48:35'),
(10, 1, 'Chrome', '::1', '2019-12-26 06:56:00'),
(11, 1, 'Chrome', '::1', '2019-12-26 08:43:39'),
(12, 1, 'Chrome', '::1', '2019-12-28 03:58:07'),
(13, 1, 'Chrome', '::1', '2019-12-28 04:40:34'),
(14, 2, 'Chrome', '::1', '2019-12-28 09:11:22'),
(15, 1, 'Chrome', '::1', '2019-12-28 10:05:08'),
(16, 1, 'Chrome', '::1', '2019-12-28 10:13:40'),
(17, 2, 'Chrome', '::1', '2019-12-28 10:14:15'),
(18, 2, 'Chrome', '::1', '2019-12-29 03:33:34'),
(19, 1, 'Chrome', '::1', '2020-01-02 03:29:37'),
(20, 2, 'Chrome', '::1', '2020-01-03 03:16:08'),
(21, 2, 'Chrome', '::1', '2020-01-08 10:02:33'),
(22, 2, 'Chrome', '::1', '2020-01-08 10:46:52'),
(23, 2, 'Chrome', '::1', '2020-01-09 03:34:46'),
(24, 2, 'Chrome', '::1', '2020-01-09 04:49:37'),
(25, 2, 'Chrome', '::1', '2020-01-09 05:03:00'),
(26, 2, 'Chrome', '::1', '2020-01-09 05:21:27'),
(27, 2, 'Chrome', '::1', '2020-01-09 05:31:23'),
(28, 2, 'Chrome', '::1', '2020-01-09 05:32:43'),
(29, 2, 'Chrome', '::1', '2020-01-09 05:47:17'),
(30, 2, 'Chrome', '::1', '2020-01-09 06:38:18'),
(31, 2, 'Chrome', '::1', '2020-01-09 07:32:40'),
(32, 2, 'Chrome', '::1', '2020-01-09 07:54:20'),
(33, 2, 'Chrome', '::1', '2020-01-09 07:56:32'),
(34, 2, 'Chrome', '::1', '2020-01-09 08:04:55'),
(35, 2, 'Chrome', '::1', '2020-01-09 08:05:38'),
(36, 2, 'Chrome', '::1', '2020-01-09 08:05:54'),
(37, 2, 'Chrome', '::1', '2020-01-09 08:16:38'),
(38, 2, 'Chrome', '::1', '2020-01-09 08:39:30'),
(39, 2, 'Chrome', '::1', '2020-01-10 03:23:19'),
(40, 2, 'Chrome', '::1', '2020-01-10 03:23:35'),
(41, 5, 'Chrome', '::1', '2020-01-10 04:14:11'),
(42, 5, 'Chrome', '::1', '2020-01-11 03:12:55'),
(43, 2, 'Chrome', '::1', '2020-01-11 05:45:31'),
(44, 2, 'Chrome', '::1', '2020-01-11 08:43:12'),
(45, 2, 'Chrome', '::1', '2020-01-11 09:28:08'),
(46, 2, 'Chrome', '::1', '2020-01-12 02:08:11'),
(47, 2, 'Chrome', '::1', '2020-01-12 03:09:13'),
(48, 2, 'Chrome', '::1', '2020-01-12 03:26:12'),
(49, 2, 'Chrome', '::1', '2020-01-12 03:58:14'),
(50, 2, 'Chrome', '::1', '2020-01-12 04:20:05'),
(51, 2, 'Chrome', '::1', '2020-01-12 06:05:01'),
(52, 1, 'Chrome', '::1', '2020-01-12 06:11:24'),
(53, 1, 'Chrome', '::1', '2020-01-13 03:32:42'),
(54, 1, 'Chrome', '::1', '2020-01-13 04:48:05'),
(55, 2, 'Chrome', '::1', '2020-01-17 03:32:38'),
(56, 2, 'Chrome', '::1', '2020-01-17 04:49:01'),
(57, 2, 'Chrome', '::1', '2020-01-17 06:33:42'),
(58, 2, 'Chrome', '::1', '2020-01-17 06:44:06'),
(59, 2, 'Chrome', '::1', '2020-01-17 06:46:24'),
(60, 2, 'Chrome', '::1', '2020-01-17 07:09:39'),
(61, 2, 'Chrome', '::1', '2020-01-17 08:27:16'),
(62, 2, 'Chrome', '::1', '2020-01-17 08:38:38'),
(63, 2, 'Chrome', '::1', '2020-01-22 08:41:51'),
(64, 2, 'Chrome', '::1', '2020-01-23 03:05:31'),
(65, 2, 'Chrome', '::1', '2020-01-23 05:33:59'),
(66, 2, 'Chrome', '::1', '2020-01-23 06:02:49'),
(67, 2, 'Chrome', '::1', '2020-01-23 06:06:20'),
(68, 2, 'Chrome', '::1', '2020-01-24 02:40:17'),
(69, 2, 'Chrome', '::1', '2020-01-24 04:53:42'),
(70, 2, 'Chrome', '::1', '2020-01-24 09:01:24'),
(71, 2, 'Chrome', '::1', '2020-01-25 03:16:47'),
(72, 2, 'Chrome', '::1', '2020-01-25 11:10:43');

-- --------------------------------------------------------

--
-- Table structure for table `customer_master`
--

CREATE TABLE IF NOT EXISTS `customer_master` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `password` varchar(300) NOT NULL,
  `country` varchar(50) NOT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `register_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ethereum_address` varchar(255) DEFAULT NULL,
  `referralcode` varchar(255) DEFAULT NULL,
  `referralcodefrom` varchar(255) DEFAULT NULL,
  `affiliateusercommission` varchar(200) DEFAULT NULL,
  `address` text,
  `phone_bill` text,
  `passport` text,
  `national_id` text,
  `proof_of_address` text,
  `verified_status` varchar(100) NOT NULL DEFAULT '0',
  `two_auth` tinyint(4) NOT NULL DEFAULT '0',
  `google_auth_code` varchar(255) DEFAULT NULL,
  `oauth_provider` text,
  `oauth_uid` text,
  `profile_pic` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `customer_master`
--

INSERT INTO `customer_master` (`cust_id`, `fullname`, `email`, `phone`, `date_of_birth`, `password`, `country`, `postal_code`, `city`, `status`, `register_date`, `ethereum_address`, `referralcode`, `referralcodefrom`, `affiliateusercommission`, `address`, `phone_bill`, `passport`, `national_id`, `proof_of_address`, `verified_status`, `two_auth`, `google_auth_code`, `oauth_provider`, `oauth_uid`, `profile_pic`) VALUES
(1, 'Test T Testing', 'vishaltesting14@gmail.com', '123456789', '0000-00-00', '202cb962ac59075b964b07152d234b70', 'India', NULL, NULL, 1, '2019-12-11 11:46:21', '', 'TestTTestingNywkhy', NULL, NULL, '403 anupam square\r\nmota varachha', '5VsjPnZBb8RCi9EA.jpg', '5VsjPnZBb8RCi9EA2.jpg', '5VsjPnZBb8RCi9EA3.jpg', '5VsjPnZBb8RCi9EA1.jpg', '0', 0, 'BJXQ3JWYILZWW5CV', NULL, NULL, NULL),
(2, 'Test Test', 'vishaltesting10@gmail.com', '0123456789', '2020-08-12', '202cb962ac59075b964b07152d234b70', 'India', '132465', 'Test', 1, '2019-12-28 08:46:07', '', 'VishalVGQA7J', NULL, NULL, '123, Test\r\nTest', 'fRKGtcN7JusPzQhp.jpg', 'BqvfHhaxkCg3AEWQ.jpg', 'BqvfHhaxkCg3AEWQ1.jpg', 'fRKGtcN7JusPzQhp1.jpg', '1', 0, '6JD6OTWYQ5EZC67K', NULL, NULL, 'LAxQqZhV0TMcug6X.jpg'),
(3, 'Vishatest', 'vishaltesting16@gmail.com', NULL, '0000-00-00', '202cb962ac59075b964b07152d234b70', 'India', NULL, NULL, 1, '2019-12-28 08:53:20', '', 'Vishatest3K3hwc', 'VishalVGQA7J', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL),
(4, '', 'vishaltesticvvng10@gmail.com', NULL, '0000-00-00', '202cb962ac59075b964b07152d234b70', '', NULL, NULL, 1, '2020-01-10 03:49:25', '', 'Uw34V1', '', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL),
(5, 'gffgh', 'vishaltesting14+12@gmail.com', NULL, '0000-00-00', '202cb962ac59075b964b07152d234b70', 'Albania', NULL, NULL, 1, '2020-01-10 03:51:24', '', 'xthxKs', 'resssss', NULL, '', NULL, NULL, NULL, NULL, '0', 0, 'DWZNE5URDGODDPSL', NULL, NULL, NULL),
(6, '', 'vishaltesting10+111@gmail.com', NULL, '0000-00-00', '202cb962ac59075b964b07152d234b70', '', NULL, NULL, 1, '2020-01-17 06:46:18', '', '4m3Pwv', 'VishalVGQA7J', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL),
(7, '', 'vishaltesting14+111@gmail.com', NULL, '0000-00-00', '202cb962ac59075b964b07152d234b70', '', NULL, NULL, 1, '2020-01-17 06:48:33', '', 'yeMoWDAh', '', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_wallet`
--

CREATE TABLE IF NOT EXISTS `customer_wallet` (
  `customer_wallet_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_wallet_id` int(11) NOT NULL,
  `total_balance` double NOT NULL,
  `address` varchar(555) NOT NULL,
  `method` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `reg_date` date NOT NULL,
  `assign_date` datetime DEFAULT NULL,
  PRIMARY KEY (`customer_wallet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `customer_wallet`
--

INSERT INTO `customer_wallet` (`customer_wallet_id`, `currency_wallet_id`, `total_balance`, `address`, `method`, `user_id`, `status`, `reg_date`, `assign_date`) VALUES
(1, 1, 10, '', 'Bitcoin', 1, 1, '2019-12-11', NULL),
(2, 2, 0, '', 'Ethereum', 1, 1, '2019-12-11', NULL),
(3, 3, 0, '', 'Ripple', 1, 1, '2019-12-11', NULL),
(4, 4, 0, '', 'Tether', 1, 1, '2019-12-11', NULL),
(5, 5, 0, '', 'Bitcoin Cash', 1, 1, '2019-12-11', NULL),
(6, 6, 0, '', 'Litecoin', 1, 1, '2019-12-11', NULL),
(7, 7, 0, '', 'EOS', 1, 1, '2019-12-11', NULL),
(8, 8, 0, '', 'Binancecoin', 1, 1, '2019-12-11', NULL),
(9, 9, 0, '', 'Bitcoin Cash SV', 1, 1, '2019-12-11', NULL),
(10, 10, 0, '', 'Cardano', 1, 1, '2019-12-11', NULL),
(11, 1, 0, '', 'Bitcoin', 2, 1, '2019-12-28', NULL),
(12, 2, 0, '', 'Ethereum', 2, 1, '2019-12-28', NULL),
(13, 3, 0, '', 'Ripple', 2, 1, '2019-12-28', NULL),
(14, 4, 0, '', 'Tether', 2, 1, '2019-12-28', NULL),
(15, 5, 0, '', 'Bitcoin Cash', 2, 1, '2019-12-28', NULL),
(16, 6, 0, '', 'Litecoin', 2, 1, '2019-12-28', NULL),
(17, 7, 0, '', 'EOS', 2, 1, '2019-12-28', NULL),
(18, 8, 0, '', 'Binancecoin', 2, 1, '2019-12-28', NULL),
(19, 9, 0, '', 'Bitcoin Cash SV', 2, 1, '2019-12-28', NULL),
(20, 10, 0, '', 'Cardano', 2, 1, '2019-12-28', NULL),
(21, 1, 0, '', 'Bitcoin', 3, 1, '2019-12-28', NULL),
(22, 2, 0, '', 'Ethereum', 3, 1, '2019-12-28', NULL),
(23, 3, 0, '', 'Ripple', 3, 1, '2019-12-28', NULL),
(24, 4, 0, '', 'Tether', 3, 1, '2019-12-28', NULL),
(25, 5, 0, '', 'Bitcoin Cash', 3, 1, '2019-12-28', NULL),
(26, 6, 0, '', 'Litecoin', 3, 1, '2019-12-28', NULL),
(27, 7, 0, '', 'EOS', 3, 1, '2019-12-28', NULL),
(28, 8, 0, '', 'Binancecoin', 3, 1, '2019-12-28', NULL),
(29, 9, 0, '', 'Bitcoin Cash SV', 3, 1, '2019-12-28', NULL),
(30, 10, 0, '', 'Cardano', 3, 1, '2019-12-28', NULL),
(31, 1, 0, '', 'Bitcoin', 4, 1, '2020-01-10', NULL),
(32, 2, 0, '', 'Ethereum', 4, 1, '2020-01-10', NULL),
(33, 3, 0, '', 'Ripple', 4, 1, '2020-01-10', NULL),
(34, 4, 0, '', 'Tether', 4, 1, '2020-01-10', NULL),
(35, 5, 0, '', 'Bitcoin Cash', 4, 1, '2020-01-10', NULL),
(36, 6, 0, '', 'Litecoin', 4, 1, '2020-01-10', NULL),
(37, 7, 0, '', 'EOS', 4, 1, '2020-01-10', NULL),
(38, 8, 0, '', 'Binancecoin', 4, 1, '2020-01-10', NULL),
(39, 9, 0, '', 'Bitcoin Cash SV', 4, 1, '2020-01-10', NULL),
(40, 10, 0, '', 'Cardano', 4, 1, '2020-01-10', NULL),
(41, 1, 0, '', 'Bitcoin', 5, 1, '2020-01-10', NULL),
(42, 2, 0, '', 'Ethereum', 5, 1, '2020-01-10', NULL),
(43, 3, 0, '', 'Ripple', 5, 1, '2020-01-10', NULL),
(44, 4, 0, '', 'Tether', 5, 1, '2020-01-10', NULL),
(45, 5, 0, '', 'Bitcoin Cash', 5, 1, '2020-01-10', NULL),
(46, 6, 0, '', 'Litecoin', 5, 1, '2020-01-10', NULL),
(47, 7, 0, '', 'EOS', 5, 1, '2020-01-10', NULL),
(48, 8, 0, '', 'Binancecoin', 5, 1, '2020-01-10', NULL),
(49, 9, 0, '', 'Bitcoin Cash SV', 5, 1, '2020-01-10', NULL),
(50, 10, 0, '', 'Cardano', 5, 1, '2020-01-10', NULL),
(51, 1, 0, '', 'Bitcoin', 6, 1, '2020-01-17', NULL),
(52, 2, 0, '', 'Ethereum', 6, 1, '2020-01-17', NULL),
(53, 3, 0, '', 'Ripple', 6, 1, '2020-01-17', NULL),
(54, 4, 0, '', 'Tether', 6, 1, '2020-01-17', NULL),
(55, 5, 0, '', 'Bitcoin Cash', 6, 1, '2020-01-17', NULL),
(56, 6, 0, '', 'Litecoin', 6, 1, '2020-01-17', NULL),
(57, 7, 0, '', 'EOS', 6, 1, '2020-01-17', NULL),
(58, 8, 0, '', 'Binancecoin', 6, 1, '2020-01-17', NULL),
(59, 9, 0, '', 'Bitcoin Cash SV', 6, 1, '2020-01-17', NULL),
(60, 10, 0, '', 'Cardano', 6, 1, '2020-01-17', NULL),
(61, 1, 0, '', 'Bitcoin', 7, 1, '2020-01-17', NULL),
(62, 2, 0, '', 'Ethereum', 7, 1, '2020-01-17', NULL),
(63, 3, 0, '', 'Ripple', 7, 1, '2020-01-17', NULL),
(64, 4, 0, '', 'Tether', 7, 1, '2020-01-17', NULL),
(65, 5, 0, '', 'Bitcoin Cash', 7, 1, '2020-01-17', NULL),
(66, 6, 0, '', 'Litecoin', 7, 1, '2020-01-17', NULL),
(67, 7, 0, '', 'EOS', 7, 1, '2020-01-17', NULL),
(68, 8, 0, '', 'Binancecoin', 7, 1, '2020-01-17', NULL),
(69, 9, 0, '', 'Bitcoin Cash SV', 7, 1, '2020-01-17', NULL),
(70, 10, 0, '', 'Cardano', 7, 1, '2020-01-17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE IF NOT EXISTS `email_template` (
  `temp_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(300) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`temp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`temp_id`, `subject`, `content`) VALUES
(1, 'test e mail', '<p>Dear [NAME]</p>\n\n<p>This is a test email</p>'),
(2, 'test new email', '<p>test new email</p>');

-- --------------------------------------------------------

--
-- Table structure for table `history_issuetoken`
--

CREATE TABLE IF NOT EXISTS `history_issuetoken` (
  `history_issuetoken_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wallet_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issue_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`history_issuetoken_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `history_issuetoken`
--

INSERT INTO `history_issuetoken` (`history_issuetoken_id`, `email`, `token`, `wallet_type`, `issue_by`, `issue_id`, `reg_date`) VALUES
(1, 'vishaltesting14@gmail.com', '200', 'Debite by vishaltesting10@gmail.com', 'user', 1, '2019-12-28 10:13:15'),
(2, 'vishaltesting10@gmail.com', '200', 'Credit By : vishaltesting14@gmail.com', 'user', 2, '2019-12-28 10:13:15'),
(3, 'vishaltesting14@gmail.com', '10', 'Debite by vishaltesting14@gmail.com', 'user', 2, '2019-12-28 10:13:56'),
(4, 'vishaltesting14@gmail.com', '10', 'Credit By : vishaltesting14@gmail.com', 'user', 1, '2019-12-28 10:13:56');

-- --------------------------------------------------------

--
-- Table structure for table `ico_display_setting`
--

CREATE TABLE IF NOT EXISTS `ico_display_setting` (
  `ico_display_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `bitcoin` int(11) NOT NULL DEFAULT '1',
  `ethereum` int(11) NOT NULL DEFAULT '1',
  `litecoin` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ico_display_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ico_display_setting`
--

INSERT INTO `ico_display_setting` (`ico_display_setting_id`, `bitcoin`, `ethereum`, `litecoin`) VALUES
(1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ico_round`
--

CREATE TABLE IF NOT EXISTS `ico_round` (
  `ico_round_id` int(11) NOT NULL AUTO_INCREMENT,
  `ico_round_name` varchar(255) DEFAULT NULL,
  `total_token` varchar(255) DEFAULT NULL,
  `bonus` varchar(255) DEFAULT NULL,
  `round_rate` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `total_sold` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ico_round_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ico_round`
--

INSERT INTO `ico_round` (`ico_round_id`, `ico_round_name`, `total_token`, `bonus`, `round_rate`, `start_date`, `end_date`, `total_sold`) VALUES
(4, 'ICO 1', '993666.4', '20', '1', '2018-10-01', '2020-01-31', '6333.6'),
(5, 'ICO 2', '10000000', '20', '1', '2020-01-01', '2020-12-31', '');

-- --------------------------------------------------------

--
-- Table structure for table `level_commission`
--

CREATE TABLE IF NOT EXISTS `level_commission` (
  `commission_id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(255) DEFAULT NULL,
  `commission` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`commission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `level_commission`
--

INSERT INTO `level_commission` (`commission_id`, `level`, `commission`) VALUES
(1, 'Level 1', '5'),
(2, 'Level 2', '3'),
(3, 'Level 3', '1');

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE IF NOT EXISTS `payment_method` (
  `p_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `method_id` int(11) DEFAULT NULL,
  `method_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`p_method_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`p_method_id`, `method_id`, `method_name`) VALUES
(1, 1, 'bitcoin'),
(2, 2, 'ethereum'),
(3, 3, 'lightcoin'),
(4, 4, 'stripe'),
(5, 5, 'paypal');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE IF NOT EXISTS `portfolio` (
  `portfolio_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `manager` varchar(255) NOT NULL,
  `amount_invest` double NOT NULL,
  `profit_target` double NOT NULL,
  `loss` double NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `assets_base` varchar(255) NOT NULL,
  `risk_level` varchar(255) NOT NULL,
  `portfolio_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`portfolio_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`portfolio_id`, `name`, `manager`, `amount_invest`, `profit_target`, `loss`, `start_date`, `end_date`, `assets_base`, `risk_level`, `portfolio_image`, `status`, `reg_date`) VALUES
(1, 'PRISM', 'Richard Wong', 12000, 0, 0, '1970-01-01 01:00:00', '1970-01-01 01:00:00', '1', 'high', 'bKNR0Vm6yYzWGLFd.png', 0, '2019-12-11 05:14:10'),
(2, 'SUNRISE', 'Kent Rood', 752000, 0, 0, '1970-01-01 01:00:00', '2020-03-01 00:00:00', '1', 'low', 'uOvq2LcZ3Xe1wj5m.png', 0, '2019-12-11 05:15:59'),
(3, 'HARMONY', 'Jack Dow', 17900, 0, 0, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2', 'low', 'nMJOoN6XgwiHdY89.png', 0, '2019-12-11 05:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `referearn`
--

CREATE TABLE IF NOT EXISTS `referearn` (
  `referearn_id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_date` datetime DEFAULT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_coins` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commission` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`referearn_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `referral_master`
--

CREATE TABLE IF NOT EXISTS `referral_master` (
  `referral_id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_date` datetime DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `purchase_coins` varchar(200) DEFAULT NULL,
  `commission` varchar(200) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `txn_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`referral_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stripe_key_setting`
--

CREATE TABLE IF NOT EXISTS `stripe_key_setting` (
  `stripe_key_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_key` varchar(255) DEFAULT NULL,
  `secret_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`stripe_key_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stripe_key_setting`
--

INSERT INTO `stripe_key_setting` (`stripe_key_setting_id`, `account_key`, `secret_key`) VALUES
(1, 'pk_test_KtIci4cYLuwXqDdDbUaXuU33', 'sk_test_1Z67qGxaLoVFRJUi15Qiu6EP');

-- --------------------------------------------------------

--
-- Table structure for table `support_ticket`
--

CREATE TABLE IF NOT EXISTS `support_ticket` (
  `support_ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_customer_id` int(11) DEFAULT NULL,
  `receiver_admin_id` int(11) DEFAULT NULL,
  `ticket_number` varchar(250) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `attachment` varchar(255) DEFAULT NULL,
  `ticket_date` datetime DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`support_ticket_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `support_ticket`
--

INSERT INTO `support_ticket` (`support_ticket_id`, `sender_customer_id`, `receiver_admin_id`, `ticket_number`, `subject`, `message`, `attachment`, `ticket_date`, `status`) VALUES
(1, 1, 1, 'pq01hMia', '2FA Support', '<p>Hello i have an issue with 2FA.</p>\n', '', '2019-03-16 03:17:24', '1'),
(8, 3, 1, '15FRiSFG', 'Test', 'test tsete stest stsett', 'XCrEMG5YxDDKETWm.jpg', '2019-06-27 10:00:25', '1'),
(9, 9, 1, 'XEu4vc54', 'product pages', 'test ', 't86GsshJjRxDtuTZ.jpg', '2019-07-09 12:09:20', '1'),
(10, 16, 1, '9WBDF86T', 'TEST', 'TEST ticket', '', '2019-08-14 07:53:44', '1'),
(11, 16, 1, 'XNzoYnNP', 'TEST', 'TESTAREA SUPORTULUI', 'q6W0Nj6COy2Fo1ZE.PNG', '2019-08-14 07:54:40', '2'),
(12, 14, 1, 'PXRVauz9', 'test ticket', 'test test test', 'HxWNGvDNwq2RLE9D.png', '2019-08-14 08:06:34', '1'),
(13, 14, 1, 'RyFdRfTh', 'test', 'test ', 'w3tjOI7hY8v8Ixjx.png', '2019-08-14 08:58:25', '1'),
(14, 23, 1, 'Cb5D22GS', 'Test', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'F7dP7RgNpBBB55wo.jpg', '2019-08-21 06:55:41', '1'),
(15, 9, 1, 'JaBfpHm1', 'test new ticket', 'test', 'Capitera Dashboard Rev 01 - Black - Muli.pdf', '2019-08-27 08:37:04', '2'),
(16, 27, 1, 'IwSf4Tmn', 'test ticket', 'test test test test test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test test', '2dPtetNoYs0lnPeF.jpg', '2019-10-14 07:18:57', '0');

-- --------------------------------------------------------

--
-- Table structure for table `support_ticket_chat`
--

CREATE TABLE IF NOT EXISTS `support_ticket_chat` (
  `support_ticket_chat_id` int(11) NOT NULL AUTO_INCREMENT,
  `support_ticket_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `message` text,
  `message_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message_status` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`support_ticket_chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `theme_setting`
--

CREATE TABLE IF NOT EXISTS `theme_setting` (
  `theme_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_color` int(11) NOT NULL,
  PRIMARY KEY (`theme_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `theme_setting`
--

INSERT INTO `theme_setting` (`theme_setting_id`, `theme_color`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `token_package`
--

CREATE TABLE IF NOT EXISTS `token_package` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `no_of_token` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `token_package`
--

INSERT INTO `token_package` (`package_id`, `package_name`, `price`, `no_of_token`, `image`) VALUES
(1, 'Capitera Bronze', '100', '2250', 'bronze.png'),
(2, 'Capitera Silver', '1000', '28600', 'silver.png'),
(3, 'Capitera Gold', '5000', '167000', 'gold.png'),
(4, 'Capitera Platinum', '10000', '400000', 'platinum.png');

-- --------------------------------------------------------

--
-- Table structure for table `total_sold_tokens_chart`
--

CREATE TABLE IF NOT EXISTS `total_sold_tokens_chart` (
  `chart_id` int(11) NOT NULL AUTO_INCREMENT,
  `week_1` int(20) DEFAULT NULL,
  `week_2` int(20) DEFAULT NULL,
  `week_3` int(20) DEFAULT NULL,
  `week_4` int(20) DEFAULT NULL,
  `week_5` int(20) DEFAULT NULL,
  `week_6` int(20) DEFAULT NULL,
  `week_7` int(20) DEFAULT NULL,
  `week_8` int(11) DEFAULT NULL,
  `week_9` int(20) DEFAULT NULL,
  `week_10` int(20) DEFAULT NULL,
  `week_11` int(20) DEFAULT NULL,
  `week_12` int(20) DEFAULT NULL,
  PRIMARY KEY (`chart_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `total_sold_tokens_chart`
--

INSERT INTO `total_sold_tokens_chart` (`chart_id`, `week_1`, `week_2`, `week_3`, `week_4`, `week_5`, `week_6`, `week_7`, `week_8`, `week_9`, `week_10`, `week_11`, `week_12`) VALUES
(1, 20000, 40000, 60000, 80000, 100000, 140000, 160000, 180000, 220000, 250000, 270000, 300000);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_mast`
--

CREATE TABLE IF NOT EXISTS `transaction_mast` (
  `txn_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(100) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `crypto` double DEFAULT NULL,
  `coins` float DEFAULT NULL,
  `bonus` float DEFAULT NULL,
  `total` double DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `wallet_status` int(11) DEFAULT NULL,
  `action` int(11) DEFAULT NULL,
  `frozen_devide_coin` double DEFAULT NULL,
  `remaining_coin` varchar(255) DEFAULT NULL,
  `frozen_days` int(11) NOT NULL DEFAULT '425',
  `frozen_status` int(11) NOT NULL DEFAULT '0',
  `transaction_date` datetime DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `round_id` varchar(255) DEFAULT NULL,
  `recycle_bin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`txn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `transaction_mast`
--

INSERT INTO `transaction_mast` (`txn_id`, `address`, `method`, `transaction_id`, `package_id`, `user_id`, `user_name`, `quantity`, `amount`, `crypto`, `coins`, `bonus`, `total`, `status`, `wallet_status`, `action`, `frozen_devide_coin`, `remaining_coin`, `frozen_days`, `frozen_status`, `transaction_date`, `browser`, `ip_address`, `round_id`, `recycle_bin`) VALUES
(1, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 1, 'Test T Testing', 1, 100, 0.01365689, 2250, 2250, 2250, 1, NULL, 1, NULL, '2250', 425, 0, '2019-12-28 07:08:00', NULL, NULL, '', 0),
(2, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 1, 'Test T Testing', 1, 100, 0.01365415, 2250, 2250, 2250, 1, 1, 1, NULL, '2250', 425, 0, '2019-12-28 07:34:00', NULL, NULL, '', 0),
(3, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 3, 1, 'Test T Testing', 2, 10000, 1.36403503, 334000, 334000, 334000, 1, NULL, 1, NULL, '334000', 425, 0, '2019-12-28 07:34:00', NULL, NULL, '', 0),
(4, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 2, 1, 'Test T Testing', 1, 1000, 0, 28600, 28600, 28600, 1, NULL, 1, NULL, '28600', 425, 0, '2019-12-28 07:35:00', NULL, NULL, '', 0),
(5, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 2, 1, 'Test T Testing', 1, 1000, 0.13641299, 28600, 28600, 28600, 1, NULL, 1, NULL, '28600', 425, 0, '2019-12-28 07:36:00', NULL, NULL, '', 0),
(6, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 2, 1, 'Test T Testing', 2, 2000, 0.27293321, 57200, 57200, 57200, 1, NULL, 1, NULL, '57200', 425, 0, '2019-12-28 07:39:00', NULL, NULL, '', 0),
(7, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 3, 1, 'Test T Testing', 1, 5000, 0.68417891, 167000, 167000, 167000, 2, NULL, 1, NULL, '167000', 425, 0, '2019-12-28 07:52:00', NULL, NULL, '', 0),
(8, '0x6b8550B7401dc5C745a7316Ab6F79efbb3b99C15', 'ethereum', NULL, 3, 1, 'Test T Testing', 1, 5000, 39.28964325, 167000, 167000, 167000, 1, NULL, 1, NULL, '167000', 425, 0, '2019-12-28 07:53:00', NULL, NULL, '', 0),
(9, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 2, 1, 'Test T Testing', 1, 1000, 0.1367961, 28600, 28600, 28600, 1, NULL, 1, NULL, '28600', 425, 0, '2019-12-28 07:55:00', NULL, NULL, '', 0),
(10, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 2, 'Vishal', 2, 200, 0.02743507, 4500, 4500, 4500, 1, 1, 1, NULL, '4500', 425, 0, '2019-12-28 10:28:00', NULL, NULL, '', 0),
(11, '0x6b8550B7401dc5C745a7316Ab6F79efbb3b99C15', 'ethereum', NULL, 1, 1, 'Vishal Test', 2, 200, 1.53491942, 4500, 4500, 4500, 1, 1, 1, NULL, '4500', 425, 0, '2020-01-01 05:38:00', NULL, NULL, '', 0),
(12, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 2, 'Vishal', 1, 100, 0.01236261, 2250, 2250, 2250, 0, NULL, 1, NULL, '2250', 425, 0, '2020-01-12 07:03:00', NULL, NULL, '', 0),
(13, '0x6b8550B7401dc5C745a7316Ab6F79efbb3b99C15', 'ethereum', NULL, 2, 1, 'Test T Testing', 1, 1000, 6.90131125, 28600, 28600, 28600, 0, NULL, 1, NULL, '28600', 425, 0, '2020-01-12 07:21:00', NULL, NULL, '', 0),
(15, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 1, 'Test T Testing', 1, 100, 0.01235398, 2250, 2250, 2250, 0, NULL, 1, NULL, '2250', 425, 0, '2020-01-13 05:48:00', NULL, NULL, '', 0),
(16, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 1, 'Test T Testing', 1, 100, 0.0123519, 2250, 2250, 2250, 0, NULL, 1, NULL, '2250', 425, 0, '2020-01-13 06:10:00', NULL, NULL, '', 0),
(17, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 2, 'Vishal', 2, 200, 0.02242062, 4500, 4500, 4500, 0, NULL, 1, NULL, '4500', 425, 0, '2020-01-17 10:24:00', 'Chrome', '::1', '', 0),
(18, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 2, 'Test Test', 10, 1000, 0.12077207, 22500, 22500, 22500, 0, NULL, 1, NULL, '22500', 425, 0, '2020-01-24 11:29:00', 'Chrome', '::1', '', 0),
(19, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 2, 'Test Test', 1, 100, 0.01199923, 2250, 2250, 2250, 0, NULL, 1, NULL, '2250', 425, 0, '2020-01-24 12:36:00', 'Chrome', '::1', '', 0),
(20, '3Qyw1gfRz6EJHkbRnNh7nnNK56EbxX8T6z', 'bitcoin', NULL, 1, 2, 'Test Test', 1, 100, 0.01198921, 2250, 2250, 2250, 0, NULL, 1, NULL, '2250', 425, 0, '2020-01-25 10:53:00', 'Chrome', '::1', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE IF NOT EXISTS `wallet` (
  `wallet_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) DEFAULT NULL,
  `total_coins` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`wallet_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`wallet_id`, `cust_id`, `total_coins`, `amount`) VALUES
(1, 1, '6360', '300'),
(2, 2, '230', '200');

-- --------------------------------------------------------

--
-- Table structure for table `withdraw`
--

CREATE TABLE IF NOT EXISTS `withdraw` (
  `w_id` int(11) NOT NULL AUTO_INCREMENT,
  `withdraw_date` datetime DEFAULT NULL,
  `eth_address` varchar(100) DEFAULT NULL,
  `tokens` float DEFAULT NULL,
  `withdraw_status` varchar(100) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `rcoin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`w_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
